import parmed
import numpy as np

import argparse
import json

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('single')
    parser.add_argument('output')
    parser.add_argument('--sxy', default=None)
    args = parser.parse_args()

    single = parmed.load_file(args.single)

    m = np.array([a.mass for a in single.atoms])
    com = np.sum(single.coordinates * m[:,None], axis=0) / np.sum(m)

    Nsingle = len(single.atoms)

    box = single.box

    if args.sxy is not None:
        sxy = float(args.sxy) * 10
        box[0] = sxy
        box[1] = sxy

    new = 2 * single

    coords = new.coordinates

    coords[:,:2] -= com[:2]

    coords[:,2] += 0.5 * box[2] - com[2]

    trans = 2 / 3 * box[0] * np.array([1, 0]) + 1 / 3 * box[1] * np.array([np.cos(np.deg2rad(box[-1])), np.sin(np.deg2rad(box[-1]))])
    coords[Nsingle:,:2] += trans

    new.coordinates = coords

    new.box = box
    new.velocities = None

    new.save(args.output)


if __name__ == '__main__':
    main()
