import numpy as np
import pandas as pd

import argparse
import json


parser = argparse.ArgumentParser()
parser.add_argument('resolved', nargs=4)
parser.add_argument('other')
parser.add_argument('other_perms')
parser.add_argument('output')
args = parser.parse_args()

columns = [
    'Permutation',
    'Kind',
    'Weighted',
    'RMSD',
    'q'
]

res = {c: [] for c in columns}

def format_perms(perms):
    return '_'.join(','.join(str(i) for i in perm) for perm in perms)

for i, kind in enumerate(['full', 'step', 'other']):
    for j, weighted in enumerate(['0', '1']):
        if kind == 'other':
            fn = args.other
        else:
            fn = args.resolved[2*i + j]
        with open(fn) as file:
            result = json.load(file)

        if kind == 'other':
            with open(args.other_perms) as file:
                perms = json.load(file)['perms']
        else:
            perms = result['perms']

        res['Permutation'].append(format_perms(perms))

        if kind == 'other':
            res['Kind'].append('Burkhard')

            res['RMSD'].append(result[f'rmsd_sw{weighted}_qw{weighted}'])
            res['q'].append(result[f'cornilescu_sw{weighted}_qw{weighted}'])
        else:
            if kind == 'step':
                res['Kind'].append('stepwise')
            else:
                res['Kind'].append('full')

            res['RMSD'].append(result['rmsd'])
            res['q'].append(result['cornilescu'])

        res['Weighted'].append(weighted == '1')

df = pd.DataFrame(res, columns=columns)
df.to_csv(args.output)
