;; This is only for converting the atom orders for the RDC assignment.
;; sgsrmsd is not published yet, so there is no corresponding Guix channel.
(specifications->manifest
 '("python"
   "python-numpy"
   "python-sgsrmsd"))
