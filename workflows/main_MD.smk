config = {
    'nsteps_single': 5000000,
    'seed': 4225807005
}

module initial_structure:
    snakefile:
        "rules/initial_structure.smk"
    config: config

use rule * from initial_structure

module charges:
    snakefile:
        "rules/charges.smk"
    config: config

use rule * from charges

module FF:
    snakefile:
        "rules/FF.smk"
    config: config

use rule * from FF

module MD_setup:
    snakefile:
        "rules/MD_setup.smk"
    config: config

use rule * from MD_setup

module MD:
    snakefile:
        "rules/MD.smk"
    config: config

use rule * from MD
