import h5py
import numpy as np

import argparse
from pathlib import Path
import json

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('path')
    parser.add_argument('mode')
    parser.add_argument('first_index')
    parser.add_argument('last_index')
    parser.add_argument('num_stages')
    parser.add_argument('output')
    args = parser.parse_args()

    path = Path(args.path)

    mode = args.mode

    first_index = int(args.first_index)
    last_index = int(args.last_index)
    num_stages = int(args.num_stages)

    r = np.linspace(0, 20, 100)

    dist_sums = 0
    dist_Nframes = 0

    Nframes = 0
    Nmol = None
    Nams = None

    for index in range(first_index, last_index+1):
        filename = path / f"{index}/{num_stages}/RDCs_{mode}.hdf5"
        with h5py.File(filename, 'r') as file:
            if file.attrs['mode'] != mode:
                raise RuntimeError('Different modes!')

            if mode != 'A':
                X = np.array(file['D'])
            else:
                X = np.array(file['A'])

        Nframes += len(X)

        filename = path / f"{index}/{num_stages}/com_distances.hdf5"
        with h5py.File(filename) as file:
            distance = np.array(file['distance'])

            if Nmol is None:
                Nmol = distance.shape[2]
            else:
                assert Nmol == distance.shape[2]

            if Nams is None:
                Nams = distance.shape[1]
            else:
                assert Nams == distance.shape[1]

            X2 = np.tile(X.transpose((1, 0, 2))[:, None, :, None], (1, distance.shape[1], 1, distance.shape[-1], 1))

            assert len(distance) == len(X2)

            new_sums = np.zeros((len(r)-1, X.shape[-1]), dtype='float64')
            new_Nframes = np.zeros((len(r)-1, X.shape[-1]), dtype='uint64')
            for k in range(X2.shape[-1]):
                new_sums[:, k], _ = np.histogram(distance.ravel(), bins=r, weights=X2[..., k].ravel(), density=False)
                new_Nframes, _ = np.histogram(distance.ravel(), bins=r, density=False)

            dist_sums += new_sums
            dist_Nframes += new_Nframes

    with h5py.File(args.output, 'w') as file:
        file.attrs['Nframes'] = Nframes
        file.attrs['Nams'] = Nams
        file.attrs['Nmol'] = Nmol
        file.attrs['mode'] = mode
        file.create_dataset('r', data=r)
        file.create_dataset('dist_sums', data=dist_sums)
        file.create_dataset('dist_Nframes', data=dist_Nframes)


if __name__ == '__main__':
    main()
