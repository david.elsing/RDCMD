import RDC_tools
from RDC_tools import file_io, alignment_tensor, RDCs
import numpy as np

import argparse
import json
import itertools

parser = argparse.ArgumentParser()
parser.add_argument('couplings')
parser.add_argument('xyz')
parser.add_argument('RDCs')
parser.add_argument('output')
args = parser.parse_args()

with open(args.xyz) as file:
    mols = file_io.read_xyz_file(file)

with open(args.RDCs) as file:
    D, Derr, _, _ = file_io.read_RDC_file(file)

with open(args.couplings) as file:
    spec = json.load(file)

couplings = file_io.parse_coupling_spec(spec)


def fit(couplings, D, Derr, weight_svd=False, weight_res=False):
    M = 0
    for atoms in mols:
        prefactors = RDCs.calc_prefactors(couplings)
        directions = RDCs.calc_directions(couplings, [a['pos'] for a in atoms])
        distances = RDCs.calc_distances(couplings, [a['pos'] for a in atoms])
        M += alignment_tensor.angle_parameter_matrix(prefactors, directions, distances)
    M /= len(mols)

    mean = np.array([np.mean(v) for v in D])
    meanerr = np.array([np.mean(v) for v in Derr])
    if not weight_svd:
        A, _ = alignment_tensor.fit_alignment_tensor_averages(M, mean)
    else:
        A, _, _ = alignment_tensor.fit_alignment_tensor_averages(M, mean, meanerr)
    Dcalc = alignment_tensor.calc_RDCs(M, A)

    rmsd_D = np.sqrt(np.mean(mean**2))
    if not weight_res:
        rmsd = np.sqrt(np.mean((mean - Dcalc)**2))
        cq = rmsd / rmsd_D
    else:
        w = 1 / meanerr
        rmsd = np.sqrt(np.sum(w * (mean - Dcalc)**2) / np.sum(w))
        cq = rmsd / rmsd_D
    return A, rmsd, cq

result = {}

for sw, qw in itertools.product([0, 1], repeat=2):
    _, rmsd, cq = fit(couplings, D, Derr, sw == 1, qw == 1)
    result[f'rmsd_sw{sw}_qw{qw}'] = rmsd
    result[f'cornilescu_sw{sw}_qw{qw}'] = cq

with open(args.output, 'w') as file:
    json.dump(result, file, indent=2)
    file.write('\n')
