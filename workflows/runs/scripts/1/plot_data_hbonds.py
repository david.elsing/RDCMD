import h5py
import numpy as np

import argparse
from pathlib import Path
import json

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('path')
    parser.add_argument('hbonds_receptor')
    parser.add_argument('max_HA_distance')
    parser.add_argument('min_angle')
    parser.add_argument('first_index')
    parser.add_argument('last_index')
    parser.add_argument('num_stages')
    parser.add_argument('output')
    parser.add_argument('--Nmol')
    args = parser.parse_args()

    path = Path(args.path)

    with open(args.hbonds_receptor) as file:
        hbonds_receptor = json.load(file)

    max_HA_distance = float(args.max_HA_distance)
    min_angle = float(args.min_angle)

    first_index = int(args.first_index)
    last_index = int(args.last_index)
    num_stages = int(args.num_stages)

    results = {f'acc_index{i}': {
        'Nframes': 0,
        'Nbonds': 0,
        'lifetime_sum': 0
        } for i in hbonds_receptor['acceptors']}

    Nmol = int(args.Nmol)
    Nframes = 0
    Nchains = None

    for index in range(first_index, last_index+1):
        filename = path / f"{index}/{num_stages}/hbonds.hdf5"

        with h5py.File(filename) as file:
            new_Nframes = file.attrs['Nframes']
            Nframes += new_Nframes

            if Nchains is None:
                Nchains = file.attrs['Nchains']
            else:
                assert Nchains == file.attrs['Nchains']

            for k in range(Nchains):
                c_group = file[f'chain{k}']
                for groupname, group_results in results.items():
                    group = c_group[groupname]
                    HA_dist = np.array(group['HA_distance'])
                    hbond_angle = np.array(group['angle']) * 180 / np.pi
                    indices = np.array(group['frame_index'])
                    monomer_indices = np.array(group['monomer_index'])

                    monomer = -1 * np.ones(new_Nframes, dtype='int64')
                    monomer[indices] = monomer_indices

                    outer = np.zeros(new_Nframes, dtype='bool')
                    outer[indices] = (HA_dist <= max_HA_distance) & (hbond_angle >= min_angle)

                    curr_monomer = -1
                    lt = 0
                    for i in range(new_Nframes):
                        if lt == 0:
                            if outer[i]:
                                lt += 1
                                curr_monomer = monomer[i]
                        else:
                            if outer[i] and curr_monomer == monomer[i]:
                                lt += 1
                            else:
                                group_results['lifetime_sum'] += lt
                                group_results['Nbonds'] += 1
                                lt = 0

                    group_results['Nframes'] += int(np.sum(outer))


    results['Nframes'] = int(Nframes)
    results['Nmol'] = Nmol

    with open(args.output, 'w') as file:
        json.dump(results, file, indent=4)
        file.write('\n')

if __name__ == '__main__':
    main()
