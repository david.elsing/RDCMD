import numpy as np
import parmed
from scipy.spatial.transform import Rotation

import argparse

A_PAS = np.diag([-1.0, -1.0, 2.0])

parser = argparse.ArgumentParser()
parser.add_argument('gro', help='GROMACS .gro file')
parser.add_argument('top', help='GROMACS .top file')
parser.add_argument('out', help='Output file name')
parser.add_argument('--radius', default=10.0, help='Radius of cylinder')
parser.add_argument('--rel_err', default=0.01, help='Relative error')
parser.add_argument('--batch_size', default=1000, help='Batch size')
parser.add_argument('--seed', default=0, help='Random seed')
args = parser.parse_args()

radius = float(args.radius)
rel_err = float(args.rel_err)
batch_size = int(args.batch_size)
seed = int(args.seed)

# Load the system
mol = parmed.load_file(args.gro, args.top)
masses = np.array([a.mass for a in mol.atoms])
coords = mol.coordinates
com = np.average(coords, axis=0, weights=masses)
coords -= com
radii = np.array([a.sigma / 2 for a in mol.atoms])

atom_distances = np.linalg.norm(coords, axis=1)
idx = np.argmax(atom_distances)
max_radius = atom_distances[idx] + radii[idx]

g = np.random.default_rng(seed)

# Compute the average and variance of the RDCs with a batched online algorithm
n = 0
mean = 0.0
M2 = 0.0
while True:
    dist = g.uniform(radius, radius + max_radius, size=batch_size)
    rot = Rotation.random(batch_size, random_state=g).as_matrix()

    pos = np.einsum('nij,kj->nki', rot, coords)
    pos[:, :, 0] += dist[:, None]

    # Check collisions with cylinder along z-axis
    atom_distances = np.linalg.norm(pos[:,:,:2], axis=2)
    collision = np.any(atom_distances < radius + radii[None, :], axis=1)

    # Alignment tensor
    A = np.einsum('nji,jk,nkl->nil', rot, A_PAS, rot)

    # Set A to zero for collisions
    A[collision] = 0.0

    # Update mean and variance
    n += batch_size
    delta = A - mean
    mean += np.sum(delta, axis=0) / n
    delta2 = A - mean
    M2 += np.sum(delta * delta2, axis=0)

    var = M2 / (n - 1) / n

    max_component = np.max(np.abs(mean))
    max_err = np.sqrt(np.max(var))

    if max_err < rel_err * max_component:
        break

np.savetxt(args.out, mean)

print('Number of samples:', n)
print('Mean:', mean)
print('Error:', np.sqrt(var))
