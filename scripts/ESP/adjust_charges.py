import numpy as np

import argparse
import json

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('input_file')
    parser.add_argument('output_file')
    parser.add_argument('--buildspec', default=None)
    args = parser.parse_args()

    charges = np.loadtxt(args.input_file)

    if args.buildspec is None:
        charges -= np.sum(charges) / len(charges)
    else:
        with open(args.buildspec) as file:
            buildspec = json.load(file)

        for indices in [buildspec['monomer'],buildspec['fullcaps'][0], buildspec['fullcaps'][1]]:
            charges[indices] -= np.sum(charges[indices]) / len(indices)

    np.savetxt(args.output_file, charges)

if __name__ == '__main__':
    main()
