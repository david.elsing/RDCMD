import numpy as np

import argparse


bohr_radius = 0.529177210903

def read_xyz_file(filename):
    positions = []
    with open(filename) as file:
        for i, line in enumerate(file):
            parts = line.split()
            if i == 0:
                N = int(line)
            elif i == 1:
                pass
            elif i - 2 < N:
                positions.append([float(v) for v in parts[1:4]])
            else:
                break

    return positions

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('xyz_file')
    parser.add_argument('grid')
    parser.add_argument('grid_esp')
    parser.add_argument('output_file')
    args = parser.parse_args()

    pos = read_xyz_file(args.xyz_file)

    grid_pos = np.loadtxt(args.grid)
    esp = np.loadtxt(args.grid_esp)

    def format_float(val):
        negative = val < 0
        if negative:
            val = -val
        s = f'{val:.6E}'
        e = s.find('E')
        sign = '-' if negative else ''
        return (sign + f'0.{s[0]}{s[2:e]}{s[e:e+2]}{abs(int(s[e+1:])+1):02d}').rjust(16)

    with open(args.output_file, 'w') as file:
        file.write(f'{len(pos):>5}{len(grid_pos):>5}\n')

        for p in pos:
            file.write(17*' '+f'{format_float(p[0] / bohr_radius)}{format_float(p[1] / bohr_radius)}{format_float(p[2] / bohr_radius)}\n')

        for p, val in zip(grid_pos, esp):
            file.write(f' {format_float(val)}{format_float(p[0] / bohr_radius)}{format_float(p[1] / bohr_radius)}{format_float(p[2] / bohr_radius)}\n')

if __name__ == '__main__':
    main()
