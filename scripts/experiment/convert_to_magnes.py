import RDC_tools
from RDC_tools import file_io, alignment_tensor, RDCs
import numpy as np

import argparse
import json

parser = argparse.ArgumentParser()
parser.add_argument('couplings')
parser.add_argument('RDCs')
parser.add_argument('output')
args = parser.parse_args()

def indices(pair):
    return pair[0]['index'], pair[1]['index']

with open(args.RDCs) as file:
    _, _, Ds, Derrs = file_io.read_RDC_file(file)

with open(args.couplings) as file:
    spec = json.load(file)

couplings = spec['data']

s = 'rdc_data {\n'
for i, (c, D, Derr) in enumerate(zip(couplings, Ds, Derrs)):
    if c['type'] == 'normal':
        s += '# ' + c['name'] + '\n'
        pair = c['pair']
        i = indices(pair)
        s += ' ' + str(i[0]).rjust(3) + ' ' + str(i[1]).rjust(3)
        s += ' ' + f'{D[0]:.2f}'.rjust(6) + ' ' + f'{Derr[0]:.2f}' + '\n'
    else:
        for pair in c['pairs']:
            s += '# ' + c['name'] + '\n'
            i = indices(pair)
            s += ' ' + str(i[0]).rjust(3) + ' ' + str(i[1]).rjust(3)
            s += ' ' + f'{D[0]:.2f}'.rjust(6) + ' ' + f'{Derr[0]:.2f}' + '\n'

s += '}\n'

with open(args.output, 'w') as file:
    file.write(s)
