mkdir -p profiles
for name in ambertools analysis gromacs openbabel pyscf python rdkit snakemake squashfs; do
	echo "$name"
	guix time-machine -C manifests/"$name"/channels.scm -- package -m manifests/"$name"/manifest.scm -p profiles/"$name"
done
