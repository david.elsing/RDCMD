import numpy as np
import sgsrmsd
import h5py

import argparse

parser = argparse.ArgumentParser()
parser.add_argument('xyz')
parser.add_argument('perm')
parser.add_argument('output')
parser.add_argument('--inverse', action='store_true')
args = parser.parse_args()

with h5py.File(args.perm, 'r') as file:
    iso_perm = np.array(file['iso_perms']['iso_0_0'])
    perm = np.array(file['perms']['0_0'])

perm = sgsrmsd.compose(iso_perm, perm)
if args.inverse:
    perm = sgsrmsd.inverse(perm)

with open(args.xyz) as file:
    lines = file.readlines()

with open(args.output, 'w') as file:
    for line in lines[:2]:
        file.write(line)

    for i in perm:
        file.write(lines[2:][i])
