# MD simulations for the prediction of RDCs

## Steps for running the simulations
1. Install [GNU Guix](https://guix.gnu.org/).
2. Make the Guix profiles:

        cd workflows/guix && bash make_profiles.sh && cd -
	This will download/build all required software. All software is
    built from source, except if binary substitutes are available from
    (previously) authorized substitute servers.
3. Adjust the snakemake profiles for the cluster in
   workflows/smk-profiles. For slurm, there are existing profiles.
4. Put the molecule .mol files for one enantiomer in
   `data/molecules/small` following the naming scheme of the existing
   files. The enantiomer must match the regex `[mpRS]+`.
5. Activate the Guix environment for snakemake:

		guix shell -p workflows/guix/profiles/snakemake
6. For e.g. (-)-IPC with 1000 chains of 100 ns each, run the following inside the Guix environment:

		snakemake --profile workflows/smk-profiles/<your profile> -s workflows/main_MD.smk results/MD/free/PBLG/1/m-IPC/chains/{1..100}/md/{1..10}_whole/whole.xtc -n
	for a dry-run and

		snakemake --profile workflows/smk-profiles/<your profile> -s workflows/main_MD.smk results/MD/free/PBLG/1/m-IPC/chains/{1..100}/md/{1..10}_whole/whole.xtc
	for the actual run.

## Steps for analyzing the MD trajectories
1. Adjust `workflows/runs/1.smk` for the new molecules and types of simulations.
2. Run

		snakemake --profile workflows/smk-profiles/<your profile> -s workflows/runs/1.smk all
	For large MD simulations, this will take a while, so it might be
    preferable to run the rules `workflows/rules/MDanalysis.smk`
    beforehand and comment out the `use rule * from MDanalysis`
    statement.
3. The calculated properties are then in `results/runs/1`.

## Citation:
[J. Chem. Theory Comput. 2024, 20, 15, 6454–6469](https://doi.org/10.1021/acs.jctc.4c00441)
