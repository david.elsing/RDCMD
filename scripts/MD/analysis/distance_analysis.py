import parmed
import MDAnalysis
import h5py
import numpy as np

import argparse
import json

def _xdr_load_offsets(self):
    self._read_offsets()
def _xdr_read_offsets(self, store=False):
    self._xdr.offsets
MDAnalysis.coordinates.XDR.XDRBaseReader._load_offsets = _xdr_load_offsets
MDAnalysis.coordinates.XDR.XDRBaseReader._read_offsets = _xdr_read_offsets

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('structure_file')
    parser.add_argument('top_receptor')
    parser.add_argument('top_ligand')
    parser.add_argument('buildspec')
    parser.add_argument('numbers')
    parser.add_argument('groups')
    parser.add_argument('max_distance')
    parser.add_argument('output_file')
    parser.add_argument('trajectory_files', nargs='+')
    args = parser.parse_args()

    top_receptor = parmed.load_file(args.top_receptor)
    sigma_receptor = np.array([a.sigma for a in top_receptor.atoms])

    top_ligand = parmed.load_file(args.top_ligand)
    sigma_ligand = np.array([a.sigma for a in top_ligand.atoms])

    with open(args.buildspec) as file:
        buildspec = json.load(file)

    with open(args.numbers) as file:
        numbers = json.load(file)

    Nmonomer = numbers['Nmonomer']
    num_monomers = numbers['num_monomers']

    Nrecatoms = Nmonomer * num_monomers

    with open(args.groups) as file:
        groups_spec = json.load(file)

    max_distance = float(args.max_distance)

    group_lengths = [len(group['indices']) for group in groups_spec]
    group_selections = []
    group_receptor_sigmas = []
    for group in groups_spec:
        indices = [buildspec['new_indices'][i-1] - len(buildspec['fullcaps'][0]) for i in group['indices']]
        group_selections.append([' or ' .join(f'index {index + k*Nmonomer}' for index in indices) for k in range(num_monomers)])
        group_receptor_sigmas.append(sigma_receptor[indices])

    results = [{
        'frame_index': [],
        'monomer_indices': [],
        'distance': [],
        } for _ in groups_spec]

    for gs, group_results in zip(groups_spec, results):
        if gs['type'] == 'nearest':
            group_results['sigma'] = []


    frame_index = 0

    for trajectory_file in args.trajectory_files:
        u = MDAnalysis.Universe(args.structure_file, trajectory_file)

        groups = [sum(u.select_atoms(s, updating=True) for s in sel)
                  for sel in group_selections]

        ligand = u.select_atoms(f'resname LIG', updating=True)

        for frame in u.trajectory:
            rligand = ligand.positions

            for i, (gl, gs, group, group_results, group_rs) in enumerate(zip(group_lengths, groups_spec, groups, results, group_receptor_sigmas)):
                if gs['type'] == 'nearest':
                    rgroup = group.positions
                    all_dists = MDAnalysis.lib.distances.distance_array(rgroup, rligand, box=frame.dimensions)
                    distances = []
                    rel_distances = []
                    sigmas = []
                    for k in range(num_monomers):
                        all_rel_dists = all_dists[gl*k:gl*(k+1),:] / (0.5*(group_rs[:,None] + sigma_ligand[None,:]))
                        min_indices = np.unravel_index(np.argmin(all_rel_dists.ravel()), all_rel_dists.shape)
                        distances.append(all_dists[gl*k+min_indices[0],min_indices[1]])
                        rel_distances.append(all_rel_dists[tuple(min_indices)])
                        sigmas.append(0.5*(group_rs[min_indices[0]] + sigma_ligand[min_indices[1]]))
                else:
                    rgroup = np.array([np.mean(group.positions[gl*k:gl*(k+1)], axis=0) for k in range(num_monomers)])
                    all_dists = MDAnalysis.lib.distances.distance_array(rgroup, rligand, box=frame.dimensions)
                    distances = []
                    for k in range(num_monomers):
                        distances.append(np.min(all_dists[k,:]))

                best_distances = 4*[-1.0]
                best_sigmas = 4*[-1.0]

                if gs['type'] == 'nearest':
                    best_indices = np.argsort(rel_distances)[:4]
                else:
                    best_indices = np.argsort(distances)[:4]

                for n in range(4):
                    index = best_indices[n]
                    if distances[index] <= max_distance:
                        best_distances[n] = distances[index]
                        if gs['type'] == 'nearest':
                            best_sigmas[n] = sigmas[index]
                    else:
                        best_indices[n] = -1

                if any(index != -1 for index in best_indices):
                    group_results['frame_index'].append(frame_index)
                    group_results['monomer_indices'].append(best_indices)
                    group_results['distance'].append(best_distances)
                    if gs['type'] == 'nearest':
                        group_results['sigma'].append(best_sigmas)

            frame_index += 1


    with h5py.File(args.output_file, 'w') as file:
        file.attrs['Nframes'] = frame_index
        for i, (group, group_results) in enumerate(zip(groups_spec, results)):
            g = file.create_group(f'group{i}')
            g.attrs['name'] = group['name']
            g.create_dataset('frame_index', data=np.atleast_1d(np.array(group_results['frame_index'])))
            g.create_dataset('monomer_indices', data=np.atleast_1d(np.array(group_results['monomer_indices'])))
            g.create_dataset('distance', data=np.atleast_1d(np.array(group_results['distance'])))
            if group['type'] == 'nearest':
                g.create_dataset('sigma', data=np.atleast_1d(np.array(group_results['sigma'])))

if __name__ == '__main__':
    main()
