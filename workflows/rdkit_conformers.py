import numpy as np

from rdkit import Chem
from rdkit.Chem import AllChem

import argparse

def bool_str(arg):
    if type(arg) is bool:
        return arg
    if arg == 'False':
        return False
    elif arg == 'True':
        return True
    else:
        raise ValueError(f'arg must be \'True\' or \'False\', not \'{arg}\'')

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('smiles_file')
    parser.add_argument('output_file')
    parser.add_argument('numConfs')
    parser.add_argument('--maxAttempts', default = 0),
    parser.add_argument('--randomSeed', default = 98243394),
    parser.add_argument('--clearConfs', default = True),
    parser.add_argument('--useRandomCoords', default = False),
    parser.add_argument('--boxSizeMult', default = 2.0),
    parser.add_argument('--randNegEig', default = True),
    parser.add_argument('--numZeroFail', default = 1),
    parser.add_argument('--pruneRmsThresh', default = -1.0),
    parser.add_argument('--forceTol', default = 0.001),
    parser.add_argument('--ignoreSmoothingFailures', default = False),
    parser.add_argument('--enforceChirality', default = True),
    parser.add_argument('--numThreads', default = 1),
    parser.add_argument('--useExpTorsionAnglePrefs', default = False),
    parser.add_argument('--useBasicKnowledge', default = True),
    parser.add_argument('--useSmallRingTorsions', default = False),
    parser.add_argument('--useMacrocycleTorsions', default = False)
    parser.add_argument('--max_MMFF_iters', default = False)
    args = parser.parse_args()

    numThreads = int(args.numThreads)

    max_MMFF_iters = int(args.max_MMFF_iters)

    with open(args.smiles_file) as file:
        smi = file.read().strip()

    mol = Chem.MolFromSmiles(smi)
    mol = Chem.AddHs(mol)

    print('Generating conformers', flush=True)
    cids = AllChem.EmbedMultipleConfs(mol,
            numConfs = int(args.numConfs),
            maxAttempts = int(args.maxAttempts),
            randomSeed = int(args.randomSeed),
            clearConfs = bool_str(args.clearConfs),
            useRandomCoords = bool_str(args.useRandomCoords),
            boxSizeMult = float(args.boxSizeMult),
            randNegEig = bool_str(args.randNegEig),
            numZeroFail = int(args.numZeroFail),
            pruneRmsThresh = float(args.pruneRmsThresh),
            coordMap = {},
            forceTol = float(args.forceTol),
            ignoreSmoothingFailures = bool_str(args.ignoreSmoothingFailures),
            enforceChirality = bool_str(args.enforceChirality),
            numThreads = numThreads,
            useExpTorsionAnglePrefs = bool_str(args.useExpTorsionAnglePrefs),
            useBasicKnowledge = bool_str(args.useBasicKnowledge),
            printExpTorsionAngles = False,
            useSmallRingTorsions = bool_str(args.useSmallRingTorsions),
            useMacrocycleTorsions = bool_str(args.useMacrocycleTorsions))

    if len(cids) == 0:
        raise RuntimeError('No conformer was generated')

    print(f'{len(cids)} conformers were generated', flush=True)

    print('Optimizing conformers with MMFF', flush=True)
    AllChem.MMFFOptimizeMoleculeConfs(mol, numThreads=numThreads, maxIters = max_MMFF_iters)

    print('Writing output', flush=True)

    with Chem.SDWriter(args.output_file) as writer:
        for cid in cids:
            writer.write(mol, confId=cid)

if __name__ == '__main__':
    main()
