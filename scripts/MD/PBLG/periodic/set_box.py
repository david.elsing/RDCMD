import numpy as np

import argparse

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('struct')
    parser.add_argument('dxy')
    parser.add_argument('dz')
    parser.add_argument('output')
    args = parser.parse_args()

    atoms = []

    with open(args.struct) as file:
        for i, line in enumerate(file):
            if i == 0:
                header = line
            elif i == 1:
                header += line
                num = int(line)
                pos = np.zeros((num, 3), dtype='float64')
            elif i < 2 + num:
                j = i - 2
                pos[j] = np.array([float(v) for v in [line[20:28], line[28:36], line[36:44]]])
                atoms.append(line[:20])
            else:
                break

    pmin = np.min(pos, axis=0)
    pmax = np.max(pos, axis=0)
    pdiff = pmax - pmin

    dxy = float(args.dxy)
    dz = float(args.dz)

    sxy = np.max(pdiff[:2]) + dxy
    sz = pdiff[2] + dz

    pos -= (pmin + pmax) / 2
    pos[:,2] += sz / 2

    box = [
            sxy,
            np.sqrt(3) / 2 * sxy,
            sz,
            0.,
            0.,
            -0.5 * sxy,
            0.,
            0.,
            0.
    ]

    with open(args.output, 'w') as outfile:
        outfile.write(header)
        for a, p in zip(atoms, pos):
            outfile.write(a + ''.join(f'{v:.3f}'.rjust(8) for v in p) + '\n')
        outfile.write(' '.join(str(v) for v in box) + '\n')

if __name__ == '__main__':
    main()
