import h5py
import numpy as np
from correrr import inseq

import argparse
from pathlib import Path
import json

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('path')
    parser.add_argument('mode')
    parser.add_argument('groups')
    parser.add_argument('max_LJ_distance')
    parser.add_argument('first_index')
    parser.add_argument('last_index')
    parser.add_argument('num_stages')
    parser.add_argument('output')
    args = parser.parse_args()

    path = Path(args.path)

    mode = args.mode

    with open(args.groups) as file:
        groups_spec = json.load(file)

    max_LJ_distance = float(args.max_LJ_distance)

    first_index = int(args.first_index)
    last_index = int(args.last_index)
    num_stages = int(args.num_stages)

    results = [{'Xs': [], 'Nframes': 0} for _ in groups_spec]

    Nframes = 0

    for index in range(first_index, last_index+1):
        filename = path / f"{index}/{num_stages}/RDCs_{mode}.hdf5"
        with h5py.File(filename, 'r') as file:
            if file.attrs['mode'] != mode:
                raise RuntimeError('Different modes!')

            if mode != 'A':
                X = np.array(file['D'])
            else:
                X = np.array(file['A'])

            assert X.shape[0] == 1
            X = X[0]

        Nframes += len(X)

        filename = path / f"{index}/{num_stages}/distances.hdf5"
        with h5py.File(filename) as file:
            for i, (gs, group_results) in enumerate(zip(groups_spec, results)):
                group = file[f'group{i}']
                all_indices = np.array(group['monomer_indices'][:,0])
                frame_indices = np.array(group['frame_index'])
                distance = group['distance'][:,0]
                if gs['type'] == 'nearest':
                    sigma = group['sigma'][:,0]
                    mask = (all_indices != -1) & (distance <= max_LJ_distance * sigma)
                    indices = frame_indices[mask]
                else:
                    max_distance = gs['max_distance']
                    mask = (all_indices != -1) & (distance <= max_distance)
                    indices = frame_indices[mask]

                mask = np.ones(len(X), dtype='bool')
                mask[indices] = 0
                Xmasked = X.copy()
                Xmasked[mask, :] = 0

                group_results['Xs'].append(Xmasked)
                group_results['Nframes'] += len(indices)

    with h5py.File(args.output, 'w') as file:
        file.attrs['Nframes'] = Nframes
        file.attrs['mode'] = mode
        for i, group_results in enumerate(results):
            group = file.create_group(f'group{i}')

            print('Calculating group', i, flush=True)
            res = inseq.inseq(group_results['Xs'], save_autocov=False, verbose=True)
            mean = res['mu']
            Cov = res['Sigma_adj'] / res['n']

            group.create_dataset('mean', data=mean)
            group.create_dataset('Cov', data=Cov)
            group.attrs['Nframes'] = group_results['Nframes']
            group.attrs['Ntot'] = res['n']


if __name__ == '__main__':
    main()
