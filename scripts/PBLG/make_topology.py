import parmed
import numpy as np

import argparse
import json

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('polymer')
    parser.add_argument('prmtop')
    parser.add_argument('charges')
    parser.add_argument('three_capped_numbers')
    parser.add_argument('polymer_numbers')
    parser.add_argument('output')
    parser.add_argument('--periodic', action='store_true')
    args = parser.parse_args()

    mol = parmed.load_file(args.prmtop)

    s_polymer = parmed.load_file(args.polymer)

    charges = np.loadtxt(args.charges)

    with open(args.three_capped_numbers) as file:
        tc_numbers = json.load(file)

    Ncap1 = tc_numbers['Ncap1']
    Nmonomer = tc_numbers['Nmonomer']
    Ncap2 = tc_numbers['Ncap2']

    with open(args.polymer_numbers) as file:
        p_numbers = json.load(file)

    num_monomers = p_numbers['num_monomers']

    s_first = mol[:Ncap1 + Nmonomer]
    s_monomer = mol[Ncap1 + Nmonomer:Ncap1 + 2*Nmonomer]
    s_last = mol[Ncap1 + 2*Nmonomer:]

    if not args.periodic:
        polymer = s_first + (num_monomers - 2) * s_monomer + s_last

        polymer.residues = parmed.ResidueList()
        for i, a in enumerate(polymer.atoms):
            a.name = s_polymer.atoms[i].name
            if i < Ncap1:
                resnum, resname = 1, 'PC1'
            elif i < Ncap1 + num_monomers * Nmonomer:
                resnum, resname = 2 + (i-Ncap1) // Nmonomer, 'PMO'
            else:
                resnum, resname = 2 + num_monomers, 'PC2'
            polymer.residues.add_atom(a, resname, resnum)

        s_monomer.residues = parmed.ResidueList()
        for a in s_monomer.atoms:
            s_monomer.residues.add_atom(a, 'PMO', 3)

        s_last.residues = parmed.ResidueList()
        for i, a in enumerate(s_last.atoms):
            if i < Nmonomer:
                resnum, resname = 4, 'PMO'
            else:
                resnum, resname = 5, 'PC2'
            s_last.residues.add_atom(a, resname, resnum)

        for i in range(Ncap1):
            polymer.atoms[i].charge = charges[i]

        for i in range(Ncap2):
            polymer.atoms[Ncap1+num_monomers*Nmonomer+i].charge
            charges[Ncap1+Nmonomer+i]
            polymer.atoms[Ncap1+num_monomers*Nmonomer+i].charge = charges[Ncap1+Nmonomer+i]

        for n in range(num_monomers):
            for i in range(Nmonomer):
                polymer.atoms[Ncap1+n*Nmonomer+i].charge = charges[Ncap1+i]
    else:
        polymer = num_monomers * s_monomer
        polymer.residues = parmed.ResidueList()
        for i, a in enumerate(polymer.atoms):
            a.name = s_polymer.atoms[i].name
            resnum, resname = 1 + i // Nmonomer, 'PMO'
            polymer.residues.add_atom(a, resname, resnum)

        for n in range(num_monomers):
            for i in range(Nmonomer):
                polymer.atoms[n*Nmonomer+i].charge = charges[Ncap1+i]

    def prev_and_monomer(seq):
        prev = False
        monomer = False
        for i in seq:
            if i < Ncap1 + Nmonomer:
                prev = True
            elif i < Ncap1 + 2*Nmonomer:
                monomer = True

        return prev and monomer

    def new_index(i, n):
        if not args.periodic:
            return i + (n-1) * Nmonomer
        else:
            index = i - Ncap1 + (n-1) * Nmonomer
            if index >= 0:
                return index
            else:
                return index + num_monomers * Nmonomer

    if not args.periodic:
        nstart = 1
    else:
        nstart = 0

    for b in mol.bonds:
        if prev_and_monomer((b.atom1.idx, b.atom2.idx)):
            if b.type and b.type not in polymer.bond_types:
                polymer.bond_types.append(b.type)
            btype = b.type and polymer.bond_types[polymer.bond_types.index(b.type)]
            for n in range(nstart, num_monomers):
                polymer.bonds.append(parmed.Bond(
                    polymer.atoms[new_index(b.atom1.idx, n)],
                    polymer.atoms[new_index(b.atom2.idx, n)],
                    type=btype, order=b.order))
                polymer.bonds[-1].funct = b.funct

    for a in mol.angles:
        if prev_and_monomer((a.atom1.idx, a.atom2.idx, a.atom3.idx)):
            if a.type and a.type not in polymer.angle_types:
                polymer.angle_types.append(a.type)
            atype = a.type and polymer.angle_types[polymer.angle_types.index(a.type)]
            for n in range(nstart, num_monomers):
                polymer.angles.append(parmed.Angle(
                    polymer.atoms[new_index(a.atom1.idx, n)],
                    polymer.atoms[new_index(a.atom2.idx, n)],
                    polymer.atoms[new_index(a.atom3.idx, n)],
                    type=atype))
                polymer.angles[-1].funct = a.funct

    for d in mol.dihedrals:
        if prev_and_monomer((d.atom1.idx, d.atom2.idx, d.atom3.idx, d.atom4.idx)):
            if d.type and d.type not in polymer.dihedral_types:
                polymer.dihedral_types.append(d.type)
            dtype = d.type and polymer.dihedral_types[polymer.dihedral_types.index(d.type)]
            for n in range(nstart, num_monomers):
                polymer.dihedrals.append(parmed.Dihedral(
                    polymer.atoms[new_index(d.atom1.idx, n)],
                    polymer.atoms[new_index(d.atom2.idx, n)],
                    polymer.atoms[new_index(d.atom3.idx, n)],
                    polymer.atoms[new_index(d.atom4.idx, n)],
                    improper=d.improper, ignore_end=d.ignore_end, type=dtype))
                polymer.dihedrals[-1].funct = d.funct


    for i in mol.impropers:
        if prev_and_monomer((i.atom1.idx, i.atom2.idx, i.atom3.idx, i.atom4.idx)):
            if i.type and i.type not in polymer.improper_types:
                polymer.improper_types.append(i.type)
            itype = i.type and polymer.improper_types[polymer.improper_types.index(i.type)]
            for n in range(nstart, num_monomers):
                polymer.impropers.append(parmed.Improper(
                    polymer.atoms[new_index(i.atom1.idx, n)],
                    polymer.atoms[new_index(i.atom2.idx, n)],
                    polymer.atoms[new_index(i.atom3.idx, n)],
                    polymer.atoms[new_index(i.atom4.idx, n)],
                    type=itype))

    polymer.save(args.output)

if __name__ == '__main__':
    main()
