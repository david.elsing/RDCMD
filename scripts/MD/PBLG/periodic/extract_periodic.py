import numpy as np

import argparse
import json

def get_element(name):
    el = name.strip().rstrip('0123456789').upper()
    if len(el) == 2:
        el = el[0] + el[1].lower()
    return el


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('struct') # need to be whole
    parser.add_argument('numbers')
    parser.add_argument('num_monomers')
    parser.add_argument('bondi_radii')
    parser.add_argument('bondi_radius_scale')
    parser.add_argument('Nmatch')
    parser.add_argument('Ntest_collision')
    parser.add_argument('dimage')
    parser.add_argument('output')
    parser.add_argument('out_numbers')
    parser.add_argument('--sz', default=None)
    args = parser.parse_args()

    with open(args.bondi_radii) as file:
        bondi_radii = json.load(file)

    Nmatch = int(args.Nmatch)
    Ntest_collision = int(args.Ntest_collision)

    atoms = []

    with open(args.struct) as file:
        for i, line in enumerate(file):
            if i == 0:
                header = line
            elif i == 1:
                num = int(line)
                pos = np.zeros((num, 3), dtype='float64')
            elif i < 2 + num:
                j = i - 2
                pos[j] = np.array([float(v) for v in [line[20:28], line[28:36], line[36:44]]])
                atoms.append((line[5:10], line[10:15]))
            else:
                break

    with open(args.numbers) as file:
        numbers = json.load(file)

    Ncap1 = numbers['Ncap1']
    num_long = numbers['num_monomers']
    mainchain = numbers['first_mainchain']
    Nmonomer = numbers['Nmonomer']

    num_monomers = int(args.num_monomers)
    bondi_radius_scale = float(args.bondi_radius_scale)

    radii = bondi_radius_scale * np.array([bondi_radii[get_element(a[1])] for a in atoms]) / 10

    sidechains = np.array([i for i in range(Ncap1, Ncap1+Nmonomer) if i not in mainchain], dtype='int64')

    def collision(n, d):
        d = np.array([0,0,d])
        for i in range(n, n + Ntest_collision):
            indices1 = sidechains + i * Nmonomer
            p1 = pos[indices1]
            r1 = radii[indices1]
            for j in range(n + num_monomers - Ntest_collision, n + num_monomers):
                indices2 = sidechains + j * Nmonomer
                p2 = pos[indices2]
                r2 = radii[indices2]
                if np.any(np.sum(((p1[:,None,:]+d)-p2[None,:,:])**2, axis=-1) < (r1[:,None]+r2[None,:])**2):
                    return True
        return False

    def min_msd(n):
        p1 = pos[[(n+i)*Nmonomer + j for i in range(Nmatch) for j in mainchain],:]
        p2 = pos[[(n+i+num_monomers)*Nmonomer + j for i in range(Nmatch) for j in mainchain],:]
        p3 = pos[[(n+i+num_monomers)*Nmonomer + j for i in range(-Nmatch,0) for j in mainchain],:]
        c1 = np.mean(p1[:,2])
        c2 = np.mean(p2[:,2])
        c3 = np.mean(p3[:,2])
        d = (c3 - c1) * num_monomers / (num_monomers - Nmatch)
        msd = np.sum((p2 - (p1 + np.array([0, 0, d])))**2) / len(mainchain)
        return msd, d

    ns = []
    mc_msd = []
    for n in range(Nmatch, num_long - num_monomers - (Nmatch-1)):
        msd, d = min_msd(n)
        if not collision(n, d):
            ns.append(n)
            mc_msd.append(msd)

    n = ns[np.argmin(mc_msd)]
    if args.sz is None:
        sz = min_msd(n)[1]
    else:
        sz = float(args.sz)
    start = Ncap1 + n * Nmonomer
    end = start + num_monomers * Nmonomer
    atoms = atoms[start:end]

    dimage = float(args.dimage)

    pos[:,2] -= pos[mainchain[0] + n * Nmonomer,2]

    full_mainchain = [n2 * Nmonomer + i for n2 in range(num_monomers) for i in mainchain]
    pos[:,:2] -= np.mean(pos[full_mainchain,:2], axis=0)

    pos = pos[start:end]
    sxy = 2*np.max(np.linalg.norm(pos[:,:2], axis=1)) + dimage

    box = [
            sxy,
            np.sqrt(3) / 2 * sxy,
            sz,
            0.,
            0.,
            -0.5 * sxy,
            0.,
            0.,
            0.
    ]

    with open(args.output, 'w') as outfile:
        outfile.write(header)
        outfile.write(f'{num_monomers * Nmonomer}\n')
        for i, (a, p) in enumerate(zip(atoms, pos)):
            resi = i // Nmonomer + 1
            outfile.write(f'{resi}'.rjust(5) + a[0].ljust(5) + a[1].rjust(5) + f'{i+1}'.rjust(5) + ''.join(f'{v:.3f}'.rjust(8) for v in p) + '\n')
        outfile.write(' '.join(str(v) for v in box) + '\n')

    out_numbers = {
            'Nmonomer': Nmonomer,
            'num_monomers': num_monomers,
            'first_mainchain': [i - Ncap1 for i in mainchain]
    }

    with open(args.out_numbers, 'w') as file:
        json.dump(out_numbers, file, indent=4)
        file.write('\n')

if __name__ == '__main__':
    main()
