import h5py
import numpy as np

import argparse
import json

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('ref_values')
    parser.add_argument('output')
    parser.add_argument('--values', nargs='+')
    parser.add_argument('--l_num_indices', nargs='+')
    args = parser.parse_args()

    results = {
        'num_indices': [int(n) for n in args.l_num_indices],
        'relative_errors': []
    }


    with h5py.File(args.ref_values) as file:
        mu = np.array(file['mu'])

    for filename in args.values:
        with h5py.File(filename) as file:
            std = np.array(file['std'])
            results['relative_errors'].append(np.max(std) / np.max(np.abs(mu)))

    with open(args.output, 'w') as file:
        json.dump(results, file, indent=4)
        file.write('\n')

if __name__ == '__main__':
    main()
