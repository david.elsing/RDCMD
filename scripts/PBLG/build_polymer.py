import parmed
import networkx as nx
import numpy as np
from scipy.optimize import shgo
from scipy.spatial.transform import Rotation

import argparse
import json
import sys


def calc_point(r, phi, tau):
    p = np.array([r, 0, 0], dtype='float64')
    R = Rotation.from_rotvec([tau, 0, 0]) \
        * Rotation.from_rotvec([0, 0, np.pi-phi])
    return R.apply(p)

def normalize(v):
    return v / np.linalg.norm(v)

def rot_from_xy(xref, xb):
    n1 = normalize(xb)
    n3 = normalize(np.cross(xb, xref))
    n2 = normalize(np.cross(n3, xb))
    return Rotation.from_matrix(np.stack((n1, n2, n3)).T)

def rot_to_ref(x1, x2, x3):
    return rot_from_xy(x1 - x2, x3 - x2)

def adjust_resnums(seq):
    minresnum = min(v[1] for v in seq)
    return tuple((i, rn-minresnum) for i, rn in seq)

def parse_seq(seq_str):
    parts = seq_str.split('-')
    seq = []
    for index in parts:
        if 'n' in index:
            if 'p' not in index:
                resnum = index.count('n')
                index = index.split('n')[0]
            else:
                raise ValueError(f'Invalid sequence: {seq_str}')
        else:
            if 'p' in index:
                resnum = -index.count('p')
                index = index.split('p')[0]
            else:
                resnum = 0

        seq.append((int(index)-1, resnum))

    return seq

def spec_values(spec, mainchain, sidechains, name):
    values = {}
    for vstr, val in spec.items():
        seq = parse_seq(vstr)
        for i, _ in seq:
            if i not in mainchain and i not in sidechains:
                raise ValueError(f'Only mainchain and sidechain {name}s'
                                 + 'may be specified')

        seq = adjust_resnums(seq)
        values[tuple(seq)] = val
        values[tuple(seq[::-1])] = val

    return values


def d2r(angle):
    return angle*np.pi/180


def r2d(angle):
    return angle*180/np.pi


def normalize(v, eps=0):
    return v / (np.linalg.norm(v)+eps)


def calc_angle(a, b):
    return np.arccos(a.dot(b))


def calc_dihedral(x1, x2, axis, eps=1e-12):
    n1 = normalize(np.cross(x1, axis), eps)
    n2 = normalize(np.cross(n1, axis), eps)
    x2n = normalize(x2, eps)
    angle = -np.arctan2(n1.dot(x2n), -n2.dot(x2n))
    return np.fmod(angle+2*np.pi, 2*np.pi)


def prepare(argv):
    parser = argparse.ArgumentParser(prog='PolymerBuilder prepare')
    parser.add_argument('monomer_capped')
    parser.add_argument('capped_spec')
    parser.add_argument('adjusted')
    parser.add_argument('buildspec')
    args = parser.parse_args(argv)

    mol = parmed.load_file(args.monomer_capped, structure=True)

    g = nx.Graph()
    for b in mol.bonds:
        g.add_edge(b.atom1.idx, b.atom2.idx)

    with open(args.capped_spec) as file:
        spec = json.load(file)

    mainchain = []
    for specidx in spec['mainchain']:
        mainchain.append(specidx - 1)

    if len(mainchain) < 2:
        raise RuntimeError('Length of main chain must be at least 2')

    def find_sidechain(idx):
        mc_found = False
        g2 = g.copy()
        g2.remove_node(idx)
        indices = set([idx])
        for comp in nx.connected_components(g2):
            if comp.isdisjoint(mainchain):
                for idx2 in comp:
                    indices.add(idx2)
            else:
                if mc_found:
                    raise RuntimeError('Side chain connected to main chain twice')
                mc_found = True

        return indices

    full_sidechains = {}

    caps = [None, None]
    for specidx in spec['caps']:
        idx = specidx - 1
        neighbors = set(g.neighbors(idx))
        if mainchain[0] in neighbors:
            caps[0] = idx
            full_sidechains[idx] = find_sidechain(idx)
        elif mainchain[-1] in neighbors:
            caps[1] = idx
            full_sidechains[idx] = find_sidechain(idx)
        else:
            raise RuntimeError(f'Cap {idx+1} not connected to start or end')

    if None in caps:
        raise RuntimeError('Caps not found')

    sidechains = set()
    for i, idx in enumerate(mainchain):
        for idx2 in g.neighbors(idx):
            if idx2 not in mainchain and idx2 not in caps:
                sidechains.add(idx2)
                full_sidechains[idx2] = find_sidechain(idx2)

    dihedrals = spec_values(spec['dihedrals'], mainchain, sidechains, 'dihedral')
    angles = spec_values(spec['angles'], mainchain, sidechains, 'angle')
    lengths = spec_values(spec['lengths'], mainchain, sidechains, 'length')

    chiral_orders = {(int(idx)-1): adjust_resnums(parse_seq(order)) for idx, order in spec['chiral_orders'].items()}

    for i, idx in enumerate(mainchain):
        # Find dihedral angle
        curr_sidechains = []
        for idx2 in g.neighbors(idx):
            if idx2 not in caps and idx2 not in mainchain:
                curr_sidechains.append(idx2)

        mc_prev2, mc_prev, mc_curr, mc_next = [(mainchain[j % len(mainchain)], j // len(mainchain)) for j in [i-2,i-1,i, i+1]]

        xb = np.array([1, 0, 0])
        xref = np.array([0, 1, 0])
        d = adjust_resnums((mc_prev2, mc_prev, mc_curr, mc_next))
        d_angle = d2r(dihedrals[d])
        a = adjust_resnums((mc_prev, mc_curr, mc_next))
        angle = d2r(angles[a])
        xdh = np.array([np.cos(angle),
                        np.sin(angle) * np.cos(d_angle),
                        np.sin(angle) * np.sin(d_angle)])

        num_adjusted = len(curr_sidechains)
        given_angles = []

        for j, idx2 in enumerate(curr_sidechains):
            for k in range(j+1, num_adjusted):
                idx3 = curr_sidechains[k]
                a = ((idx2, 0), (idx, 0), (idx3, 0))
                if a in angles:
                    given_angles.append((j, k, d2r(angles[a])))

            for k, atom3 in enumerate([mc_prev, mc_next]):
                a = adjust_resnums(((idx2, 0), (idx, 0), atom3))
                if a in angles:
                    given_angles.append((j, num_adjusted + k, d2r(angles[a])))

        if idx in chiral_orders:
            chiral_order = []
            for idx2, resnum in chiral_orders[idx]:
                if (idx2, resnum) == mc_next:
                    chiral_order.append(num_adjusted+1)
                elif idx2 in curr_sidechains:
                    if resnum != 0:
                        raise RuntimeError(f'Invalid chiral order: {chiral_orders[idx]}')
                    chiral_order.append(curr_sidechains.index(idx2))
                else:
                    raise RuntimeError(f'Invalid chiral order: {chiral_orders[idx]}')
        else:
            chiral_order = None

        # TODO: optimization on sphere
        def f(x):
            x = np.concatenate((x.reshape((-1, 3)), [xb, xdh]))
            res = 0
            for i in range(num_adjusted):
                res += (np.sum(x[i]**2) - 1)**2
            for i, j, bond_angle in given_angles:
                res += (x[i].dot(x[j]) - np.cos(bond_angle))**2
            if chiral_order is not None:
                for i, j in zip(chiral_order[:-1], chiral_order[1:]):
                    if np.fmod(calc_dihedral(xref, x[j], xb) - calc_dihedral(xref, x[i], xb), 2 * np.pi) >= np.pi:
                        return 1
            return res / (num_adjusted + len(given_angles))

        res = shgo(f, [(-1, 1)]*(3*num_adjusted),
                   n=100, sampling_method='sobol')
        print('err:', res.fun)
        if res.fun > 1e-2:
            raise RuntimeError('Fit not working')
        x = np.concatenate((res.x.reshape((-1, 3)), [xb, xdh]))

        for j, idx2 in enumerate(curr_sidechains):
            a = adjust_resnums(((idx2, 0), (idx, 0), mc_prev))
            if a not in angles:
                angles[a] = angles[a[::-1]] = \
                    r2d(calc_angle(x[j], x[num_adjusted + k]))

            d = adjust_resnums((mc_prev2, mc_prev, (idx, 0), (idx2, 0)))

            d_angle = calc_dihedral(xref, x[j], xb)

            if d not in dihedrals:
                dihedrals[d] = r2d(d_angle)

    old_coords = mol.coordinates

    coords = mol.coordinates.copy()

    idx1 = mainchain[0]
    idx2 = mainchain[1]

    r = coords[idx2] - coords[idx1]
    coords[idx2] = coords[idx1] + \
        r / np.linalg.norm(r) * lengths[adjust_resnums(((idx1,0),(idx2,0)))]

    prev_coords = [None, mol.coordinates[caps[0]]]
    R_ref = rot_to_ref(coords[idx2], coords[idx1], prev_coords[1])

    r = lengths[adjust_resnums(((mainchain[-2], -1), (mainchain[-1], -1)))]
    phi = d2r(angles[adjust_resnums(((mainchain[-2], -1), (mainchain[-1], -1), (mainchain[0], 0)))])
    tau = d2r(dihedrals[adjust_resnums(((mainchain[-2], -1), (mainchain[-1], -1), (mainchain[0], 0), (mainchain[1], 0)))])
    prev_coords[0] = prev_coords[1] + R_ref.apply(calc_point(r, phi, tau))

    for i, idx in enumerate(mainchain):
        mc_R_ref = rot_to_ref(*(coords[mainchain[j]]
                                if j >= 0 else prev_coords[j]
                                for j in [i-2, i-1, i]))

        if i == 0:
            R_ref_start = mc_R_ref

        for idx2 in g.neighbors(idx):
            if idx2 == caps[0] or idx2 in mainchain[:2]:
                continue

            R_ref_old = rot_to_ref(*(old_coords[mainchain[j]]
                                     if j >= 0 else prev_coords[j]
                                     for j in [i-1, i]),
                                   old_coords[idx2])

            atoms = [(mainchain[j], j // len(mainchain))
                     for j in [i-2, i-1, i]] + [(idx2, 0)]
            if idx2 == caps[1]:
                atoms[-1] = (mainchain[0], 1)
            r = lengths[adjust_resnums(atoms[-2:])]
            phi = d2r(angles[adjust_resnums(atoms[-3:])])
            tau = d2r(dihedrals[adjust_resnums(atoms)])

            new_pos = mc_R_ref.apply(calc_point(r, phi, tau)) + coords[idx]

            R_ref = rot_to_ref(*(coords[mainchain[j]]
                                 if j >= 0 else prev_coords[j]
                                 for j in [i-1, i]),
                               new_pos)

            R = R_ref * R_ref_old.inv()
            offset = new_pos - R.apply(old_coords[idx2])

            if idx == mainchain[-1] and idx2 == caps[1]:
                R_next = R_ref * R_ref_start.inv()
                offset_next = new_pos - R_next.apply(old_coords[mainchain[0]])

            if idx2 in mainchain:
                move_atoms = [idx2]
            else:
                move_atoms = full_sidechains[idx2]

            for idx3 in move_atoms:
                coords[idx3] = R.apply(old_coords[idx3]) + offset

    mol.coordinates = coords

    cap1 = list(full_sidechains[caps[0]])
    cap2 = list(full_sidechains[caps[1]])
    allcaps = cap1 + cap2

    new_mainchain_indices = {}
    monomer = []
    for i in range(len(mol.atoms)):
        if i not in allcaps:
            monomer.append(i)
            if i in mainchain:
                new_mainchain_indices[i] = len(cap1) + len(monomer) - 1

    new_mainchain = [new_mainchain_indices[i] for i in mainchain]

    new_monomer = (len(cap1) + np.arange(len(monomer))).tolist()
    new_caps = [cap1.index(caps[0]), len(cap1) + len(monomer) + cap2.index(caps[1])]
    new_fullcaps = [list(range(len(cap1))), (len(cap1) + len(monomer) + np.arange(len(cap2))).tolist()]

    for b in mol.bonds:
        idx1 = b.atom1.idx
        idx2 = b.atom2.idx
        if idx1 in caps and idx2 in monomer:
            b.delete()
        elif idx2 in caps and idx1 in monomer:
            b.delete()

    mol.prune_empty_terms()

    s_cap1 = mol[cap1]
    s_monomer = mol[monomer]
    s_cap2 = mol[cap2]

    s_cap1.residues = [parmed.Residue(name='PC1', number=1)]
    for a in s_cap1.atoms:
        s_cap1.residues[0].add_atom(a)

    s_monomer.residues = [parmed.Residue(name='PMO', number=1)]
    for a in s_monomer.atoms:
        s_monomer.residues[0].add_atom(a)

    s_cap2.residues = [parmed.Residue(name='PC2', number=1)]
    for a in s_cap2.atoms:
        s_cap2.residues[0].add_atom(a)

    mol = s_cap1 + s_monomer + s_cap2

    mol.bonds.append(parmed.Bond(mol.atoms[new_caps[0]], mol.atoms[new_mainchain[0]]))
    mol.bonds.append(parmed.Bond(mol.atoms[new_caps[1]], mol.atoms[new_mainchain[1]]))

    mol.save(args.adjusted)

    new_indices = len(mol.atoms) * [None]
    for i, oldi in enumerate(cap1):
        new_indices[oldi] = i
    for i, oldi in enumerate(monomer):
        new_indices[oldi] = len(cap1) + i
    for i, oldi in enumerate(cap2):
        new_indices[oldi] = len(cap2) + len(monomer) + i

    buildspec = {
        'mainchain': new_mainchain,
        'monomer': new_monomer,
        'caps': new_caps,
        'fullcaps': new_fullcaps,
        'R_next': R_next.as_matrix().tolist(),
        'offset_next': offset_next.tolist(),
        'new_indices': new_indices
    }

    with open(args.buildspec, 'w') as file:
        json.dump(buildspec, file, indent=4)
        file.write('\n')


def build(argv):
    parser = argparse.ArgumentParser(prog='PolymerBuilder build')
    parser.add_argument('monomer_capped')
    parser.add_argument('buildspec')
    parser.add_argument('num_monomers')
    parser.add_argument('polymer')
    parser.add_argument('numbers')
    args = parser.parse_args(argv)

    mol = parmed.load_file(args.monomer_capped, structure=True)

    with open(args.buildspec) as file:
        buildspec = json.load(file)

    num_monomers = int(args.num_monomers)

    monomer = buildspec['monomer']
    mainchain = buildspec['mainchain']
    caps = buildspec['caps']
    fullcaps = buildspec['fullcaps']
    R_next = Rotation.from_matrix(buildspec['R_next'])
    offset_next = np.array(buildspec['offset_next'], dtype='float64')

    allcaps = fullcaps[0] + fullcaps[1]

    Ncap1 = len(fullcaps[0])
    Nmonomer = len(monomer)
    Ncap2 = len(fullcaps[1])

    element_indices = {}
    for a in mol.atoms:
        n = a.atomic_number
        if n not in element_indices:
            element_indices[n] = 1
        a.name = f'{parmed.periodic_table.Element[n]}{element_indices[n]}'
        element_indices[n] += 1

    for b in mol.bonds:
        idx1 = b.atom1.idx
        idx2 = b.atom2.idx
        if idx1 in caps and idx2 in monomer:
            b.delete()
        elif idx2 in caps and idx1 in monomer:
            b.delete()

    mol.prune_empty_terms()

    s_cap1 = mol[fullcaps[0]]
    s_monomer = mol[monomer]
    s_cap2 = mol[fullcaps[1]]

    polymer = s_cap1 + num_monomers * s_monomer + s_cap2

    for i in range(num_monomers+1):
        if i == 0:
            idx1 = caps[0]
        else:
            idx1 = mainchain[-1] + (i-1) * Nmonomer

        if i == num_monomers:
            idx2 = (num_monomers - 1) * Nmonomer + caps[1]
        else:
            idx2 = mainchain[0] + i * Nmonomer

        polymer.bonds.append(parmed.Bond(polymer[idx1], polymer[idx2]))

    coords = np.zeros((Ncap1+num_monomers*Nmonomer + Ncap2, 3), dtype='float64')
    coords[:Ncap1] = s_cap1.coordinates

    R_tot = Rotation.identity()
    offset_tot = np.zeros(3, dtype='float64')
    for i in range(num_monomers):
        coords[Ncap1+Nmonomer*i:Ncap1+Nmonomer*(i+1)] = offset_tot + R_tot.apply(s_monomer.coordinates)
        if i == num_monomers - 1:
            coords[Ncap1+num_monomers*Nmonomer:] = offset_tot + R_tot.apply(s_cap2.coordinates)

        offset_tot += R_tot.apply(offset_next)
        R_tot *= R_next

    polymer.coordinates = coords

    polymer.save(args.polymer)

    numbers = {
            'Ncap1': Ncap1,
            'Ncap2': Ncap2,
            'Nmonomer': Nmonomer,
            'num_monomers': num_monomers,
            'first_mainchain': mainchain

    }
    with open(args.numbers, 'w') as file:
        json.dump(numbers, file, indent=4)
        file.write('\n')

def main():
    commands = {
        'prepare': prepare,
        'build': build
    }
    parser = argparse.ArgumentParser(prog='PolymerBuilder')
    parser.add_argument('command', metavar='command', choices=commands.keys())
    args = parser.parse_args(sys.argv[1:2])

    command = args.command
    commands[command](sys.argv[2:])

if __name__ == '__main__':
    main()
