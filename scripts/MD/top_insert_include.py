import argparse
import re

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('top')
    parser.add_argument('posre')
    parser.add_argument('moltypenum')
    parser.add_argument('output')
    args = parser.parse_args()

    moltypenum = int(args.moltypenum)

    r = re.compile(r'\[ (\w+) ]\s*$')

    with open(args.top) as file:
        lines = file.readlines()

    line_index = None

    imt = -1
    for i, line in enumerate(lines):
        match = r.match(line)
        if not match:
            continue
        s = match.group(1)
        if s == 'moleculetype' or s == 'system':
            if imt == moltypenum:
                line_index = i
                break
            elif s == 'moleculetype':
                imt += 1

    if line_index is None:
        raise RuntimeError(f'moleculetype {moltypenum} not found')

    includeline = f'#include "{args.posre}"\n'

    lines = lines[:line_index] + [includeline] + lines[line_index:]

    with open(args.output, 'w') as file:
        file.write(''.join(lines))

if __name__ == '__main__':
    main()
