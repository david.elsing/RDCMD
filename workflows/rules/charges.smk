import common

re_method = "(HF|DFT)"

def charge_xyz_input(w):
    if w.which == 'PBLG' and w.name == 'adjusted':
        return f"results/molecules/PBLG/adjusted_pdb_conv.xyz"
    else:
        return f"results/molecules/{w.which}/{w.name}/opt.xyz"

localrules: copy_charge_xyz_file
rule copy_charge_xyz_file:
    input:
        charge_xyz_input
    output:
        "results/FF/{which}/{name}/charges/input.xyz"
    shell:
        """
        cp {input[0]:q} {output[0]:q}
        """

localrules: ESP_grid
rule ESP_grid:
    input:
        xyz="results/FF/{which}/{name}/charges/input.xyz",
        bondi_radii="data/bondi_radii.json"
    output:
        "results/FF/{which}/{name}/charges/RESP/grid.dat"
    wildcard_constraints:
        which=common.re_which,
        name=common.re_name
    params:
        script=workflow.source_path("../../scripts/ESP/make_grid.py"),
        density=5,
        shell="""
        python3 {params.script:q} {input.xyz:q} {input.bondi_radii:q} {params.density:q} {output[0]:q}
        """,
        guix="python"
    script:
        "../run_smk_rule.py"

rule pyscf_ESP:
    input:
        xyz="results/FF/{which}/{name}/charges/input.xyz",
        grid="results/FF/{which}/{name}/charges/RESP/grid.dat"
    output:
        "results/FF/{which}/{name}/charges/RESP/{method}/grid_esp.dat"
    wildcard_constraints:
        which=common.re_which,
        name=common.re_name,
        method=re_method
    params:
        script=workflow.source_path("../../scripts/ESP/pyscf_esp.py"),
        max_memory=lambda _, resources: int(float(resources.mem_mb) * 0.9),
        shell="""
        export OMP_NUM_THREADS={resources.threads}
        export PYSCF_MAX_MEMORY={params.max_memory}
        python3 {params.script:q} {input.xyz:q} {input.grid:q} {wildcards.method} {output[0]:q}
        """,
        guix="pyscf"
    script:
        "../run_smk_rule.py"

localrules: PBLG_capped_respgen_a
rule PBLG_capped_respgen_a:
    input:
        pdb="results/FF/PBLG/adjusted/charges/input_xyz_conv.pdb",
        buildspec="results/molecules/PBLG/buildspec.json"
    output:
        respgen_a="results/FF/PBLG/adjusted/charges/RESP/respgen_a"
    params:
        script=workflow.source_path("../../scripts/ESP/make_respgen_a.py"),
        shell="""
        python3 {params.script:q} {input.pdb:q} {input.buildspec:q} {output[0]:q}
        """,
        guix="python"
    script:
        "../run_smk_rule.py"

localrules: PBLG_capped_RESP_charges
rule PBLG_capped_RESP_charges:
    input:
        pdb="results/FF/PBLG/adjusted/charges/input_xyz_conv.pdb",
        xyz="results/FF/PBLG/adjusted/charges/input.xyz",
        grid="results/FF/PBLG/adjusted/charges/RESP/grid.dat",
        esp="results/FF/PBLG/adjusted/charges/RESP/{method}/grid_esp.dat",
        respgen_a="results/FF/PBLG/adjusted/charges/RESP/respgen_a"
    output:
        dir=directory("results/FF/PBLG/adjusted/charges/RESP/{method}/rundir"),
        charges="results/FF/PBLG/adjusted/charges/RESP/{method}/charges"
    wildcard_constraints:
        method=re_method
    params:
        convert_script=workflow.source_path("../../scripts/ESP/convert_esp.py"),
        resp_script=workflow.source_path("../../scripts/ESP/calc_resp_charges.py"),
        shell="""
        mkdir {output.dir:q}
        python3 {params.convert_script:q} {input.xyz:q} {input.grid:q} {input.esp:q} {output.dir:q}/esp.dat

        pdb="$(readlink -f {input.pdb:q})"
        respgen_a="$(readlink -f {input.respgen_a:q})"
        cd {output.dir:q}

        antechamber -fi pdb -i "$pdb" -fo ac -o mol.ac

        respgen -i mol.ac -o stage1.respin -f resp1 -a "$respgen_a"
        resp -O -i stage1.respin -o stage1.respout -e esp.dat -t qout_stage1

        respgen -i mol.ac -o stage2.respin -f resp2 -a "$respgen_a"
        resp -O -i stage2.respin -o stage2.respout -e esp.dat -q qout_stage1 -t qout_stage2
        python3 -c 'with open("qout_stage2") as infile:\n\twith open("../charges", "w") as outfile:\n\t\toutfile.write("\\n".join(infile.read().split())+"\\n")'
        """,
        guix="ambertools"
    script:
        "../run_smk_rule.py"

localrules: RESP_charges
rule RESP_charges:
    input:
        pdb="results/FF/{which}/{name}/charges/input_xyz_conv.pdb",
        xyz="results/FF/{which}/{name}/charges/input.xyz",
        grid="results/FF/{which}/{name}/charges/RESP/grid.dat",
        esp="results/FF/{which}/{name}/charges/RESP/{method}/grid_esp.dat"
    output:
        dir=directory("results/FF/{which}/{name}/charges/RESP/{method}/rundir"),
        charges="results/FF/{which}/{name}/charges/RESP/{method}/charges"
    wildcard_constraints:
        method=re_method,
        which="(small|TCM)",
        name=common.re_name
    params:
        convert_script=workflow.source_path("../../scripts/ESP/convert_esp.py"),
        resp_script=workflow.source_path("../../scripts/ESP/calc_resp_charges.py"),
        shell="""
        mkdir {output.dir:q}
        python3 {params.convert_script:q} {input.xyz:q} {input.grid:q} {input.esp:q} {output.dir:q}/esp.dat

        pdb="$(readlink -f {input.pdb:q})"
        cd {output.dir:q}

        antechamber -fi pdb -i "$pdb" -fo ac -o mol.ac

        respgen -i mol.ac -o stage1.respin -f resp1
        resp -O -i stage1.respin -o stage1.respout -e esp.dat -t qout_stage1

        respgen -i mol.ac -o stage2.respin -f resp2
        resp -O -i stage2.respin -o stage2.respout -e esp.dat -q qout_stage1 -t qout_stage2
        python3 -c 'with open("qout_stage2") as infile:\n\twith open("../charges", "w") as outfile:\n\t\toutfile.write("\\n".join(infile.read().split())+"\\n")'
        """,
        guix="ambertools"
    script:
        "../run_smk_rule.py"

localrules: adjust_charges
rule adjust_charges:
    input:
        "results/FF/{which}/{name}/charges/RESP/{method}/charges"
    output:
        "results/FF/{which}/{name}/charges/RESP/{method}/charges_adjusted"
    wildcard_constraints:
        method=re_method,
        which=common.re_which,
        name=common.re_name
    params:
        script=workflow.source_path("../../scripts/ESP/adjust_charges.py"),
        shell="""
        python3 {params.script:q} {input[0]:q} {output[0]:q}
        """,
        guix="python"
    script:
        "../run_smk_rule.py"

localrules: adjust_PBLG_capped_charges
rule adjust_PBLG_capped_charges:
    input:
        "results/FF/PBLG/adjusted/charges/{method}/charges",
        "results/molecules/PBLG/buildspec.json",
    output:
        "results/FF/PBLG/adjusted/charges/RESP/{method}/charges_adjusted"
    wildcard_constraints:
        method=re_method
    params:
        script=workflow.source_path("../../scripts/ESP/adjust_charges.py"),
        shell="""
        python3 {params.script:q} {input[0]:q} {output[0]:q} --buildspec {input[1]:q}
        """,
        guix="python"
    script:
        "../run_smk_rule.py"

#localrules: PBLG_long_charges
#rule PBLG_long_charges:
#    input:
#        charges="results/FF/charges/PBLG/adjusted/charges/{method}/charges_adjusted",
#        numbers="results/MD/PBLG/long/PBLG_long_numbers.json"
#    output:
#        "results/FF/charges/{chargemethod}/PBLG/long/charges"
#    wildcard_constraints:
#        chargemethod=re_chargemethod
#    params:
#        script=workflow.source_path("../../scripts/PBLG/long/collect_charges.py"),
#        shell="""
#        python3 {params.script:q} {input.charges:q} {input.numbers:q} {output[0]:q}
#        """,
#        guix="python"
#    script:
#        "../run_smk_rule.py"
#
#localrules: adjust_PBLG_long_charges
#rule adjust_PBLG_long_charges:
#    input:
#        "results/FF/charges/{chargemethod}/PBLG/long/charges"
#    output:
#        "results/FF/charges/{chargemethod}/PBLG/long/charges_adjusted"
#    wildcard_constraints:
#        chargemethod=re_chargemethod
#    params:
#        script=workflow.source_path("../../scripts/ESP/adjust_charges.py"),
#        shell="""
#        python3 {params.script:q} {input[0]:q} {output[0]:q}
#        """,
#        guix="python"
#    script:
#        "../run_smk_rule.py"
