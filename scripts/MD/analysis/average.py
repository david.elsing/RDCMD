from correrr import inseq

import h5py
import numpy as np

import argparse
from pathlib import Path

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('output_file')
    parser.add_argument('analysis_files', nargs='+')
    args = parser.parse_args()

    Xs = []

    X = None

    mode = None
    for i, filename in enumerate(args.analysis_files):
        with h5py.File(filename, 'r') as file:
            if mode is None:
                mode = file.attrs['mode']
            else:
                newmode = file.attrs['mode']
                if newmode != mode:
                    raise RuntimeError('Different modes!')

            if mode != 'A':
                new = np.mean(np.array(file['D']), axis=0)
            else:
                new = np.mean(np.array(file['A']), axis=0)

            Xs.append(new)

    res = inseq.inseq(Xs, save_autocov=False, verbose=True)

    n = res['n']

    mu = res['mu']
    Cov = res['Sigma_adj'] / n

    with h5py.File(args.output_file, 'w') as output_file:
        output_file.attrs['mode'] = mode
        output_file.attrs['n'] = n
        output_file['mu'] = mu
        output_file['Cov'] = Cov
        output_file['std'] = np.sqrt(np.diag(Cov))


if __name__ == '__main__':
    main()
