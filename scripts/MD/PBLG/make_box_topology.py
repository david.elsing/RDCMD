import parmed

import argparse
import itertools
import functools
import operator

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('struct')
    parser.add_argument('top_polymer')
    parser.add_argument('--top_solv', default=None)
    parser.add_argument('--num_polymer', default=1)
    parser.add_argument('output')
    args = parser.parse_args()

    s = parmed.load_file(args.struct)
    polymer = parmed.load_file(args.top_polymer)

    num_polymer = int(args.num_polymer)

    if args.top_solv:
        solv = parmed.load_file(args.top_solv)

        num_solv = 0
        for r in s.residues:
            if r.name == solv.residues[0].name:
                num_solv += 1

        res = num_polymer * polymer + num_solv * solv
    else:
        res = num_polymer * polymer

    res.save(args.output)

if __name__ == '__main__':
    main()
