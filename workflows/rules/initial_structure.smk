import common

localrules: initial_conformer
rule initial_conformer:
    input:
        "data/molecules/{which}/{molname}.smi"
    output:
        "tmpresults/molecules/{which}/{molname}.mol"
    wildcard_constraints:
        which=common.re_which,
        molname=common.re_molname
    params:
        script=workflow.source_path("../../scripts/embed_smiles.py"),
        N_confs=200,
        seed=0,
        shell="""
        python3 {params.script:q} {input[0]:q} {output[0]:q} \
            --N_confs {params.N_confs} \
            --N_threads {resources.threads} \
            --seed {params.seed}
        """,
        guix="rdkit"
    script:
        "../run_smk_rule.py"

localrules: convert_file_format
rule convert_file_format:
    input:
        "{path}.{suffix}",
    output:
        "{path}_{suffix}_conv.{newsuffix}"
    wildcard_constraints:
        suffix="[a-z0-9]+",
        newsuffix="[a-z0-9]+"
    params:
        shell="""
        obabel {input[0]:q} -O {output[0]:q}
        """,
        guix="openbabel"
    script:
        "../run_smk_rule.py"

localrules: copy_initial_molecule
rule copy_initial_molecule:
    input:
        "data/molecules/{which}/{molname}.mol"
    output:
        "results/molecules/{which}/{molname}/initial.mol"
    wildcard_constraints:
        which=common.re_which,
        molname=common.re_molname
    shell:
        """
        cp {input[0]:q} {output[0]:q}
        """

localrules: create_capped_spec
rule create_capped_spec:
    input:
        "results/molecules/PBLG/BLG_cap/initial_mol_conv.pdb",
        "data/molecules/PBLG/capped_spec_template.json"
    output:
        "results/molecules/PBLG/capped_spec.json"
    params:
        script=workflow.source_path("../../scripts/PBLG/create_capped_spec.py"),
        shell="""
        python3 {params.script:q} {input[0]:q} {input[1]:q} {output[0]:q}
        """,
        guix="rdkit"
    script:
        "../run_smk_rule.py"

localrules: prepare_monomer
rule prepare_monomer:
    input:
        "results/molecules/PBLG/BLG_cap/opt_xyz_conv.pdb",
        "results/molecules/PBLG/capped_spec.json"
    output:
        "results/molecules/PBLG/adjusted.pdb",
        "results/molecules/PBLG/buildspec.json"
    params:
        script=workflow.source_path("../../scripts/PBLG/build_polymer.py"),
        shell="""
        python3 {params.script:q} prepare {input[0]:q} {input[1]:q} {output[0]:q} {output[1]:q}
        """,
        guix="python"
    script:
        "../run_smk_rule.py"

localrules: three_capped_monomers
rule three_capped_monomers:
    input:
        "results/molecules/PBLG/adjusted.pdb",
        "results/molecules/PBLG/buildspec.json"
    output:
        "results/molecules/PBLG/three_capped.pdb",
        "results/molecules/PBLG/three_capped_numbers.json"
    params:
        script=workflow.source_path("../../scripts/PBLG/build_polymer.py"),
        shell="""
        python3 {params.script:q} build {input[0]:q} {input[1]:q} 3 {output[0]:q} {output[1]:q}
        """,
        guix="python"
    script:
        "../run_smk_rule.py"

rule pyscf_optimize:
    input:
        "results/molecules/{which}/{name}/initial_mol_conv.xyz"
    output:
        "results/molecules/{which}/{name}/opt.xyz"
    wildcard_constraints:
        which=common.re_which,
        name="[\w-]+"
    params:
        script=workflow.source_path("../../scripts/pyscf_optimize.py"),
        maxsteps=1000,
        max_memory=lambda _, resources: int(float(resources.mem_mb) * 0.9),
        shell="""
        export OMP_NUM_THREADS={resources.threads}
        export PYSCF_MAX_MEMORY={params.max_memory}
        python3 {params.script:q} {input[0]:q} {output[0]:q} \
        --maxsteps {params.maxsteps}
        """,
        guix="pyscf"
    script:
        "../run_smk_rule.py"

localrules: name_molecule
rule name_molecule:
    input:
        "results/molecules/{which}/{name}/initial_mol_conv.pdb"
    output:
        "results/molecules/{which}/{name}/named.gro"
    wildcard_constraints:
        which="(small|TCM)",
        name="[\w-]+"
    params:
        script=workflow.source_path("../../scripts/name_molecule.py"),
        shell="""
            which={wildcards.which:q}
            if [[ "$which" == small ]]; then
                name=LIG
            elif [[ "$which" == TCM ]]; then
                name=TCM
            fi
            python3 {params.script:q} {input[0]:q} "$name" {output[0]:q}
            """,
        guix="python"
    script:
        "../run_smk_rule.py"

localrules: center_molecule
rule center_molecule:
    input:
        "results/molecules/small/{name}/named.gro"
    output:
        "results/molecules/small/{name}/centered.gro"
    wildcard_constraints:
        name="[\w-]+"
    params:
        script=workflow.source_path("../../scripts/center_molecule.py"),
        shell="""
        python3 {params.script:q} {input[0]:q} {output[0]:q}
        """,
        guix="python"
    script:
        "../run_smk_rule.py"

localrules: chosen_structure
rule chosen_structure:
    input:
        lambda w: f"results/molecules/small/{common.choose_name(f'{w.enantiomer}-{w.molname}')}/centered.gro"
    output:
        "results/molecules/small/chosen/{enantiomer}-{molname}.gro"
    wildcard_constraints:
        molname=common.re_molname,
        enantiomer=common.re_enantiomer
    params:
        mol_exists=lambda w: common.mol_exists(f'{w.enantiomer}-{w.molname}'),
        script=workflow.source_path("../../scripts/small/invert_molecule.py"),
        shell="""
        if [[ {params.mol_exists:q} == True ]]; then
            cp {input[0]:q} {output[0]:q}
        else
            python3 {params.script:q} {input[0]:q} {output[0]:q}
        fi
        """,
        guix="python"
    script:
        "../run_smk_rule.py"

localrules: chosen_opt_structure
rule chosen_opt_structure:
    input:
        lambda w: f"results/molecules/small/{common.choose_name(f'{w.enantiomer}-{w.molname}')}/opt_xyz_conv.gro"
    output:
        "results/molecules/small/chosen_opt/{enantiomer}-{molname}.gro"
    wildcard_constraints:
        enantiomer=common.re_enantiomer,
        molname=common.re_molname
    params:
        mol_exists=lambda w: common.mol_exists(f'{w.enantiomer}-{w.molname}'),
        script=workflow.source_path("../../scripts/small/invert_molecule.py"),
        shell="""
        if [[ {params.mol_exists:q} == True ]]; then
            cp {input[0]:q} {output[0]:q}
        else
            python3 {params.script:q} {input[0]:q} {output[0]:q}
        fi
        """,
        guix="python"
    script:
        "../run_smk_rule.py"
