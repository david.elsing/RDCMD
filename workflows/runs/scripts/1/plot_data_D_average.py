import RDC_tools
from RDC_tools.file_io import parse_coupling_spec

import parmed

import h5py
import numpy as np

import argparse
import json

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('average')
    parser.add_argument('coupling_spec')
    parser.add_argument('ligand')
    parser.add_argument('top_ligand')
    parser.add_argument('output_file')
    args = parser.parse_args()

    with open(args.coupling_spec) as file:
        coupling_spec = json.load(file)
        coupling_spec = parse_coupling_spec(coupling_spec)

    top_ligand = parmed.load_file(args.top_ligand)
    masses = np.array([a.mass for a in top_ligand.atoms])

    ligand = parmed.load_file(args.ligand)

    pos = ligand.coordinates
    com = np.sum(pos * masses[:,None]) / np.sum(masses)
    pos = pos - com

    prefactors = RDC_tools.RDCs.calc_prefactors(coupling_spec)
    dist = RDC_tools.RDCs.calc_distances(coupling_spec, pos)
    dirs = RDC_tools.RDCs.calc_directions(coupling_spec, pos)
    M = RDC_tools.alignment_tensor.angle_parameter_matrix(prefactors, dirs, dist)

    with h5py.File(args.average) as file:
        mode = file.attrs['mode']
        mu = np.array(file['mu'])
        Cov = np.array(file['Cov'])

    if mode == 'A':
        D = M.dot(mu)
        DCov = M.dot(Cov.dot(M.T))
    else:
        D = mu
        DCov = Cov

    with h5py.File(args.output_file, 'w') as file:
        file.attrs['mode'] = mode
        file.create_dataset('D', data=D)
        file.create_dataset('Cov', data=DCov)

if __name__ == '__main__':
    main()
