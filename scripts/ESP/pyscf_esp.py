from pyscf import gto, dft, scf, df
from pyscf.geomopt.geometric_solver import optimize
import numpy as np

import argparse
import json

bohr_radius = 0.529177210903

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('xyz_file')
    parser.add_argument('grid')
    parser.add_argument('method', choices=['HF', 'DFT'])
    parser.add_argument('output_file')
    args = parser.parse_args()

    mol = gto.Mole(atom=args.xyz_file)
    mol.cart = True

    if args.method == 'HF':
        mol.basis = '6-31G*'
    else:
        mol.basis = 'def2-TZVP'

    mol.build()

    mol.verbose = 9

    if args.method == 'HF':
        mf = scf.RHF(mol)
    else:
        mf = dft.RKS(mol)
        mf.xc = 'b3lyp'
        mf = mf.density_fit()

    mf.kernel()

    grid = np.loadtxt(args.grid) / bohr_radius

    # From examples/1-advanced/031-MEP.py

    Vn = 0
    for i in range(mol.natm):
        r = grid - mol.atom_coord(i)
        Z = mol.atom_charge(i)
        Vn += Z / np.linalg.norm(r, axis=1)

    fakemol = gto.fakemol_for_charges(grid)
    Ve = np.einsum('ijk,ij->k', df.incore.aux_e2(mol, fakemol), mf.make_rdm1())

    esp = Vn - Ve
    np.savetxt(args.output_file, esp)


if __name__ == '__main__':
    main()
