import parmed

import argparse
import subprocess
import shutil
import tempfile
import os

parser = argparse.ArgumentParser()
parser.add_argument('box')
parser.add_argument('solvent')
parser.add_argument('reference')
parser.add_argument('output')
args = parser.parse_args()

def determine_number(path):
    mol = parmed.load_file(path)
    num = 0
    for r in mol.residues:
        if r.name == 'TCM':
            num += 1

    return num

ref_num = determine_number(args.reference)

Nsteps = 30
scale_min = 0
scale_max = 1
scale = 0.57
with tempfile.TemporaryDirectory() as tmpdir:
    tmpfile = os.path.join(tmpdir, 'tmp.gro')
    def try_scale(scale, maxnum=False):
        cmd = ['gmx', 'solvate',
               '-cp', args.box,
               '-cs', args.solvent,
               '-scale', str(scale),
               '-o', tmpfile]

        if maxnum:
            cmd += ['-maxsol', str(ref_num)]
        res = subprocess.run(cmd,
                             text=True,
                             stdout=subprocess.PIPE,
                             stderr=subprocess.STDOUT)

        if res.returncode != 0:
            raise RuntimeError(f'GROMACS error: {res.stdout}')

        num = determine_number(tmpfile)
        print(f'scale: {scale:.3f}, num: {num}, ref: {ref_num}', flush=True)
        if num == ref_num:
            return 'done'

        os.remove(tmpfile)
        return num


    for _ in range(Nsteps):
        num = try_scale(scale, maxnum=False)
        if num == 'done':
            break;

        if num > ref_num:
            scale_min = scale
            scale = 0.5 * (scale + scale_max)
        else:
            scale_max = scale
            scale = 0.5 * (scale + scale_min)
    else:
        num = try_scale(scale, maxnum=True)
        assert num == 'done'

    shutil.copy(tmpfile, args.output)
