from pathlib import Path

def mol_exists(name):
    p = Path(f'data/molecules/small/{name}.mol')
    return p.is_file()

_enantiomer_trans = str.maketrans({'R': 'S', 'S': 'R', 'm': 'p', 'p': 'm'})

def switch_enantiomer(enantiomer):
    return enantiomer.translate(_enantiomer_trans)

def choose_name(name):
    if mol_exists(name):
        return name
    else:
        enantiomer, molname = name.split('-', maxsplit=1)
        enantiomer = switch_enantiomer(enantiomer)
        return f'{enantiomer}-{molname}'

re_float = r"[+-]?(0|[1-9]\d*|\d+\.\d*|\.\d+)([eE][+-]?\d+)?"
re_which = "(small|TCM|PBLG)"
re_name = r"[\w-]+"
re_molname = r"[\w-]+"
re_enantiomer = r"[RSmp]+"
re_am = r"(PBLG|2PBLG)"
re_lnum = r"\d+"
re_index = r"\d+"

def am_path(w):
    if w.am == 'PBLG':
        return 'PBLG/periodic'
    elif w.am == '2PBLG':
        return 'PBLG/2periodic'

def small_ligand_top_input(w):
    name = choose_name(f'{w.enantiomer}-{w.molname}')
    return f"results/FF/small/{name}/AMBER.top"
