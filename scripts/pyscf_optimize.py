from pyscf import gto, dft
from pyscf.geomopt.geometric_solver import optimize

import argparse

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('xyz_file')
    parser.add_argument('output_file')
    parser.add_argument('--maxsteps', default=1000)
    args = parser.parse_args()

    maxsteps = int(args.maxsteps)

    mol = gto.Mole(atom=args.xyz_file)
    mol.cart = True
    mol.basis = 'def2-SVP'
    mol.build()

    mol.charge = 0
    mol.spin = 0
    mol.symmetry = 1
    mol.unit = 'Ang'    # (New in version 1.1)

    #mol.output = 'test.txt'
    mol.verbose = 9
    #mol.max_memory = 121600 # in MB

    mf = dft.RKS(mol)
    mf.xc = 'b3lyp'
    mf = mf.density_fit()
    mf.kernel()
    mf.analyze()

    mol_opt = optimize(mf, maxsteps=maxsteps)

    mol_opt.tofile(args.output_file, format='xyz')

if __name__ == '__main__':
    main()
