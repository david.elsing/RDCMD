(list
 (channel
  (name 'guix)
  (url "https://git.savannah.gnu.org/git/guix.git")
  (commit "1750d68309e26293c2da5aad953a061867f2cb14")
  (introduction
   (make-channel-introduction
	"9edb3f66fd807b096b48283debdcddccfea34bad"
	(openpgp-fingerprint
	 "BBB0 2DDF 2CEA F6A8 0D1D  E643 A2A0 6DF2 A33A 54FA"))))
 (channel
  (name 'chemistry-channel)
  (url "https://codeberg.org/dtelsing/chemistry-channel")
  (branch "RDCs1")
  (commit "322b3e87e2e32e08f8bd26cd9538c20b6ccd6b37")
  (introduction
   (make-channel-introduction
	"141d7d61b4001b6123f5e1ecf52774e072c2bfd0"
	(openpgp-fingerprint
	 "C40D 2F72 991A 2A60 0EE1  920E 4874 4C7F 3771 3D80")))))
