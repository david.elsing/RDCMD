from rdkit import Chem

import argparse
import json


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('monomer_capped')
    parser.add_argument('template')
    parser.add_argument('capped_spec')
    args = parser.parse_args()

    mol = Chem.MolFromPDBFile(args.monomer_capped, removeHs=False)

    with open(args.template) as file:
        template = json.load(file)

    pattern = Chem.MolFromSmarts(template['pattern'])

    indices = mol.GetSubstructMatch(pattern)

    def new_index(i):
        return indices[i-1] + 1

    def new_seq(seq):
        parts = seq.split('-')

        new_parts = []
        for index in parts:
            if 'n' in index:
                if 'p' not in index:
                    i, suffix = index.split('n', 1)
                    i = int(i)
                    new_parts.append(f'{new_index(i)}n{suffix}')
                else:
                    raise ValueError(f'Invalid sequence: {seq_str}')
            else:
                if 'p' in index:
                    i, suffix = index.split('p', 1)
                    i = int(i)
                    new_parts.append(f'{new_index(i)}p{suffix}')
                else:
                    i = int(index)
                    new_parts.append(f'{new_index(i)}')

        return '-'.join(new_parts)

    spec = {}
    spec['mainchain'] = [new_index(i) for i in template['mainchain']]
    spec['caps'] = [new_index(i) for i in template['caps']]
    spec['lengths'] = {new_seq(seq): val for seq, val in template['lengths'].items()}
    spec['angles'] = {new_seq(seq): val for seq, val in template['angles'].items()}
    spec['dihedrals'] = {new_seq(seq): val for seq, val in template['dihedrals'].items()}
    spec['chiral_orders'] = {str(new_index(int(i))): new_seq(seq) for i, seq in template['chiral_orders'].items()}

    with open(args.capped_spec, 'w') as file:
        json.dump(spec, file, indent=4)
        file.write('\n')



if __name__ == '__main__':
    main()
