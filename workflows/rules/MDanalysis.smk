import common

re_mode = "(full|angles|distances|A)"

rule MD_analysis:
    input:
        gro="results/MD/free/{am}/{lnum}/{enantiomer}-{molname}/chains/{index}/md/1_whole/whole.gro",
        xtc=lambda w: expand("results/MD/free/{am}/{lnum}/{enantiomer}-{molname}/chains/{index}/md/{stage}_whole/whole.xtc",
                             am=w.am,
                             lnum=w.lnum,
                             enantiomer=w.enantiomer,
                             molname=w.molname,
                             index=w.index,
                             stage=range(1, int(w.num_stages) + 1)),
        ligand="results/molecules/small/chosen/{enantiomer}-{molname}.gro",
        couplings_spec="data/couplings/{molname}.json"
    output:
        "results/MD/free/{am}/{lnum}/analysis/{enantiomer}-{molname}/chains/{index}/{num_stages}/RDCs_{mode}.hdf5"
    wildcard_constraints:
        am=common.re_am,
        lnum=common.re_lnum,
        enantiomer=common.re_enantiomer,
        molname=common.re_molname,
        index=common.re_index,
        num_stages="\d+",
        mode=re_mode
    group:
        "MD_analysis"
    params:
        script=workflow.source_path("../../scripts/MD/analysis/analysis.py"),
        shell="""
        python3 {params.script:q} {input.gro:q} {input.ligand:q} {input.couplings_spec:q} {output[0]:q} \
            {input.xtc:q} --mode={wildcards.mode:q} --Nmol {wildcards.lnum:q}
        """,
        guix="analysis"
    script:
        "../run_smk_rule.py"

localrules: MD_average
rule MD_average:
    input:
        lambda w: expand("results/MD/free/{am}/{lnum}/analysis/{enantiomer}-{molname}/chains/{index}/{num_stages}/RDCs_{mode}.hdf5",
                         am=w.am,
                         lnum=w.lnum,
                         enantiomer=w.enantiomer,
                         molname=w.molname,
                         index=range(int(w.first_index), int(w.last_index)+1),
                         num_stages=w.num_stages,
                         mode=w.mode)
    output:
        "results/MD/free/{am}/{lnum}/analysis/{enantiomer}-{molname}/average/{first_index}_{last_index}_{num_stages}_{mode}.hdf5"
    wildcard_constraints:
        am=common.re_am,
        lnum=common.re_lnum,
        enantiomer=common.re_enantiomer,
        molname=common.re_molname,
        first_index=common.re_index,
        last_index=common.re_index,
        num_stages="\d+",
        mode=re_mode
    params:
        script=workflow.source_path("../../scripts/MD/analysis/average.py"),
        shell="""
        python3 {params.script:q} {output[0]:q} {input:q}
        """,
        guix="analysis"
    script:
        "../run_smk_rule.py"

def Nchains(w):
    if w.am == 'PBLG':
        return 1
    elif w.am == '2PBLG':
        return 2

rule MD_free_hbond_analysis:
    input:
        gro="results/MD/free/{am}/{lnum}/{enantiomer}-{molname}/chains/{index}/md/1_whole/whole.gro",
        xtc=lambda w: expand("results/MD/free/{am}/{lnum}/{enantiomer}-{molname}/chains/{index}/md/{stage}_whole/whole.xtc",
                             am=w.am,
                             lnum=w.lnum,
                             enantiomer=w.enantiomer,
                             molname=w.molname,
                             index=w.index,
                             stage=range(1, int(w.num_stages) + 1)),
        buildspec="results/molecules/PBLG/buildspec.json",
        numbers="results/MD/PBLG/periodic/PBLG_periodic_numbers.json",
        hbonds_receptor="data/molecules/PBLG/hbond_analysis.json",
        hbond_ligand="data/molecules/small/hbonds/{molname}1.json"
    output:
        "results/MD/free/{am}/{lnum}/analysis/{enantiomer}-{molname}/chains/{index}/{num_stages}/hbonds.hdf5"
    wildcard_constraints:
        am=common.re_am,
        lnum=common.re_lnum,
        enantiomer=common.re_enantiomer,
        molname=common.re_molname,
        index=common.re_index,
        num_stages="\d+"
    group:
        "MD_free_hbond_analysis"
    params:
        script=workflow.source_path("../../scripts/MD/analysis/hbond_analysis.py"),
        max_distance=6,
        Nchains=Nchains,
        shell="""
            python3 {params.script:q} {input.gro:q} {input.buildspec:q} {input.numbers:q} \
                {input.hbonds_receptor:q} {input.hbond_ligand:q} {params.max_distance:q} {output[0]:q} \
                {input.xtc:q} --Nmol {wildcards.lnum:q} --Nchains {params.Nchains:q}
            """,
        guix="analysis"
    script:
        "../run_smk_rule.py"

rule MD_free_distance_analysis:
    input:
        gro="results/MD/free/{am}/{lnum}/{enantiomer}-{molname}/chains/{index}/md/1_whole/whole.gro",
        xtc=lambda w: expand("results/MD/free/{am}/{lnum}/{enantiomer}-{molname}/chains/{index}/md/{stage}_whole/whole.xtc",
                             am=w.am,
                             lnum=w.lnum,
                             enantiomer=w.enantiomer,
                             molname=w.molname,
                             index=w.index,
                             stage=range(1, int(w.num_stages) + 1)),
        top_receptor="results/MD/PBLG/periodic/box/box.top",
        top_ligand=common.small_ligand_top_input,
        buildspec="results/molecules/PBLG/buildspec.json",
        numbers="results/MD/PBLG/periodic/PBLG_periodic_numbers.json",
        groups="data/molecules/PBLG/distance_analysis.json",
    output:
        "results/MD/free/{am}/{lnum}/analysis/{enantiomer}-{molname}/chains/{index}/{num_stages}/distances.hdf5"
    group:
        "MD_free_distance_analysis"
    wildcard_constraints:
        am="PBLG",
        lnum=1,
        enantiomer="[RSmp]+",
        molname="[\w-]+",
        index="\d+",
        num_stages="\d+"
    params:
        script=workflow.source_path("../../scripts/MD/analysis/distance_analysis.py"),
        max_distance=4.0,
        shell="""
            python3 {params.script:q} {input.gro:q} {input.top_receptor:q} {input.top_ligand:q} {input.buildspec:q} {input.numbers:q} \
                {input.groups:q} {params.max_distance:q} {output[0]:q} \
                {input.xtc:q}
            """,
        guix="analysis"
    script:
        "../run_smk_rule.py"

rule MD_free_com_distance_analysis:
    input:
        gro="results/MD/free/{am}/{lnum}/{enantiomer}-{molname}/chains/{index}/md/1_whole/whole.gro",
        xtc=lambda w: expand("results/MD/free/{am}/{lnum}/{enantiomer}-{molname}/chains/{index}/md/{stage}_whole/whole.xtc",
                             am=w.am,
                             lnum=w.lnum,
                             enantiomer=w.enantiomer,
                             molname=w.molname,
                             index=w.index,
                             stage=range(1, int(w.num_stages) + 1)),
        top_am=lambda w: f"results/MD/{common.am_path(w)}/box/box.top",
        numbers="results/MD/PBLG/periodic/PBLG_periodic_numbers.json",
        top_ligand=common.small_ligand_top_input
    output:
        "results/MD/free/{am}/{lnum}/analysis/{enantiomer}-{molname}/chains/{index}/{num_stages}/com_distances.hdf5"
    group:
        "MD_free_com_distance_analysis"
    wildcard_constraints:
        am=common.re_am,
        lnum=common.re_lnum,
        enantiomer="[RSmp]+",
        molname="[\w-]+",
        index="\d+",
        num_stages="\d+"
    params:
        script=workflow.source_path("../../scripts/MD/analysis/com_distance_analysis.py"),
        Nchains=Nchains,
        shell="""
            python3 {params.script:q} {input.gro:q} {input.top_am:q} {input.top_ligand:q} {input.numbers:q} {output[0]:q} \
                {input.xtc:q} --Nmol {wildcards.lnum:q} --Nchains {params.Nchains:q}
            """,
        guix="analysis"
    script:
        "../run_smk_rule.py"
