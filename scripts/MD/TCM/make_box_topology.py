import parmed

import argparse

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('prmtop')
    parser.add_argument('struct')
    parser.add_argument('output')
    args = parser.parse_args()

    mol = parmed.load_file(args.prmtop)
    s = parmed.load_file(args.struct)

    num = len(s.atoms) // len(mol.atoms)
    box = num * mol

    box.save(args.output)

if __name__ == '__main__':
    main()
