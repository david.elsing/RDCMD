localrules: TCM_radius
rule TCM_radius:
    output:
        "results/MD/TCM/radius.txt"
    params:
        script=workflow.source_path("../../scripts/TCM/radius.py"),
        shell="""
        r="$(python3 {params.script:q})"
        echo "$r" > {output[0]:q}
        """,
        guix="python"
    script:
        "../run_smk_rule.py"

localrules: TCM_initial_box
rule TCM_initial_box:
    input:
        "results/molecules/TCM/TCM/named.gro",
        "results/MD/TCM/radius.txt"
    output:
        dir=directory("results/MD/TCM/box/initial"),
        file="results/MD/TCM/box/initial/box.gro"
    params:
        shell="""
        r="$(cat {input[1]:q})"
        cp {input[0]:q} {output.dir:q}/1.gro
        cd {output.dir:q}
        gmx editconf -f 1.gro -bt cubic -box "$r" "$r" "$r" -o 2.gro
        gmx editconf -f 2.gro -translate 0.05 0.05 0.05 -o 3.gro
        gmx genconf -f 3.gro -o box.gro -rot -nbox 10 10 10
        """,
        guix="gromacs"
    script:
        "../run_smk_rule.py"

localrules: TCM_box_topology
rule TCM_box_topology:
    input:
        gro="results/MD/TCM/box/initial/box.gro",
        top="results/FF/TCM/TCM/AMBER.top"
    output:
        "results/MD/TCM/box/box.top"
    params:
        script=workflow.source_path("../../scripts/MD/TCM/make_box_topology.py"),
        shell="""
        python3 {params.script:q} {input.top:q} {input.gro:q} {output[0]:q}
        """,
        guix="python"
    script:
        "../run_smk_rule.py"

rule TCM_box_MD_em:
    input:
        gro="results/MD/TCM/box/initial/box.gro",
        top="results/MD/TCM/box/box.top",
        mdp="scripts/MD/TCM/em.mdp"
    output:
        directory("results/MD/TCM/box/em"),
        "results/MD/TCM/box/em/em.gro",
        "results/MD/TCM/box/em/finished"
    params:
        shell="""
        gmx grompp -f {input.mdp:q} -c {input.gro:q} -p {input.top:q} -o {output[0]:q}/em.tpr -po {output[0]:q}/mdout.mdp
        cd {output[0]:q}
        gmx mdrun -g -v -deffnm em -ntmpi 1
        touch finished
        """,
        guix="gromacs"
    script:
        "../run_smk_rule.py"

rule TCM_box_MD_nvt:
    input:
        gro="results/MD/TCM/box/em/em.gro",
        top="results/MD/TCM/box/box.top",
        mdp="scripts/MD/TCM/nvt.mdp"
    output:
        directory("results/MD/TCM/box/nvt"),
        "results/MD/TCM/box/nvt/nvt.gro",
        "results/MD/TCM/box/nvt/nvt.cpt",
        "results/MD/TCM/box/nvt/finished"
    params:
        seed=config['seed'],
        shell="""
        export OMP_NUM_THREADS={resources.GMX_OMP}
        sed 's/$SEED/{params.seed}/g' {input.mdp:q} > {output[0]:q}/nvt.mdp
        gmx grompp -f {output[0]:q}/nvt.mdp -c {input.gro:q} -p {input.top:q} -o {output[0]:q}/nvt.tpr -po {output[0]:q}/mdout.mdp
        cd {output[0]:q}
        gmx mdrun -g -v -deffnm nvt -ntmpi {resources.GMX_MPI}
        touch finished
        """,
        guix="gromacs"
    script:
        "../run_smk_rule.py"

rule TCM_box_MD_npt1:
    input:
        gro="results/MD/TCM/box/nvt/nvt.gro",
        cpt="results/MD/TCM/box/nvt/nvt.cpt",
        top="results/MD/TCM/box/box.top",
        mdp="scripts/MD/TCM/npt1.mdp"
    output:
        directory("results/MD/TCM/box/npt1"),
        "results/MD/TCM/box/npt1/npt1.gro",
        "results/MD/TCM/box/npt1/npt1.cpt",
        "results/MD/TCM/box/npt1/finished"
    params:
        seed=config['seed'],
        shell="""
        export OMP_NUM_THREADS={resources.GMX_OMP}
        sed 's/$SEED/{params.seed}/g' {input.mdp:q} > {output[0]:q}/npt1.mdp
        gmx grompp -f {output[0]:q}/npt1.mdp -c {input.gro:q} -t {input.cpt:q} -p {input.top:q} -o {output[0]:q}/npt1.tpr -po {output[0]:q}/mdout.mdp
        cd {output[0]:q}
        gmx mdrun -g -v -deffnm npt1 -ntmpi {resources.GMX_MPI}
        touch finished
        """,
        guix="gromacs"
    script:
        "../run_smk_rule.py"

rule TCM_box_MD_npt2:
    input:
        gro="results/MD/TCM/box/npt1/npt1.gro",
        cpt="results/MD/TCM/box/npt1/npt1.cpt",
        top="results/MD/TCM/box/box.top",
        mdp="scripts/MD/TCM/npt2.mdp"
    output:
        directory("results/MD/TCM/box/npt2"),
        "results/MD/TCM/box/npt2/npt2.gro",
        "results/MD/TCM/box/npt2/npt2.cpt",
        "results/MD/TCM/box/npt2/finished"
    params:
        seed=config['seed'],
        shell="""
        export OMP_NUM_THREADS={resources.GMX_OMP}
        sed 's/$SEED/{params.seed}/g' {input.mdp:q} > {output[0]:q}/npt2.mdp
        gmx grompp -f {output[0]:q}/npt2.mdp -c {input.gro:q} -t {input.cpt:q} -p {input.top:q} -o {output[0]:q}/npt2.tpr -po {output[0]:q}/mdout.mdp
        cd {output[0]:q}
        gmx mdrun -g -v -deffnm npt2 -ntmpi {resources.GMX_MPI}
        touch finished
        """,
        guix="gromacs"
    script:
        "../run_smk_rule.py"

rule TCM_box_MD_md:
    input:
        gro="results/MD/TCM/box/npt2/npt2.gro",
        cpt="results/MD/TCM/box/npt2/npt2.cpt",
        top="results/MD/TCM/box/box.top",
        mdp="scripts/MD/TCM/md.mdp"
    output:
        directory("results/MD/TCM/box/md"),
        "results/MD/TCM/box/md/md.gro",
        "results/MD/TCM/box/md/md.tpr",
        "results/MD/TCM/box/md/md.xtc",
        "results/MD/TCM/box/md/finished"
    params:
        seed=config['seed'],
        shell="""
        export OMP_NUM_THREADS={resources.GMX_OMP}
        sed 's/$SEED/{params.seed}/g' {input.mdp:q} > {output[0]:q}/md.mdp
        gmx grompp -f {output[0]:q}/md.mdp -c {input.gro:q} -t {input.cpt:q} -p {input.top:q} -o {output[0]:q}/md.tpr -po {output[0]:q}/mdout.mdp
        cd {output[0]:q}
        gmx mdrun -g -v -deffnm md -ntmpi {resources.GMX_MPI}
        touch finished
        """,
        guix="gromacs"
    script:
        "../run_smk_rule.py"

localrules: TCM_box_msd
rule TCM_box_msd:
    input:
        tpr="results/MD/TCM/box/md/md.tpr",
        xtc="results/MD/TCM/box/md/md.xtc"
    output:
        directory("results/MD/TCM/box/ana/msd"),
        msd="results/MD/TCM/box/ana/msd/msd.xvg",
        diff="results/MD/TCM/box/ana/msd/diff.xvg"
    wildcard_constraints:
        which="\w+"
    params:
        seed=0,
        shell="""
        echo 0 | gmx msd -f {input.xtc:q} -s {input.tpr:q} -o {output.msd:q} -mol {output.diff:q}
        """,
        guix="gromacs"
    script:
        "../run_smk_rule.py"

localrules: TCM_box_rotacf_index
rule TCM_box_rotacf_index:
    input:
        gro="results/MD/TCM/box/initial/box.gro",
        top="results/MD/TCM/box/box.top"
    output:
        "results/MD/TCM/box/ana/rotacf/index.ndx"
    wildcard_constraints:
        which="\w+"
    params:
        script=workflow.source_path("../../scripts/MD/TCM/rotacf_index.py"),
        shell="""
        python3 {params.script:q} {input.top:q} {output[0]:q}
        """,
        guix="python"
    script:
        "../run_smk_rule.py"

localrules: TCM_box_rotacf
rule TCM_box_rotacf:
    input:
        tpr="results/MD/TCM/box/md/md.tpr",
        xtc="results/MD/TCM/box/md/md.xtc",
        index="results/MD/TCM/box/ana/rotacf/index.ndx"
    output:
        xvg="results/MD/TCM/box/ana/rotacf/rotacf.xvg",
        log="results/MD/TCM/box/ana/rotacf/log.txt"
    wildcard_constraints:
        which="\w+"
    params:
        shell="""
        gmx rotacf -n {input.index:q} -f {input.xtc:q} -s {input.tpr:q} -o {output.xvg:q} -fitfn aexp -P 1 | tee {output.log:q}
        """,
        guix="gromacs"
    script:
        "../run_smk_rule.py"

localrules: TCM_box_density
rule TCM_box_density:
    input:
        edr="results/MD/TCM/box/md/md.edr"
    output:
        directory("results/MD/TCM/box/ana/dens"),
        dens="results/MD/TCM/box/ana/dens/dens.xvg",
        log="results/MD/TCM/box/ana/dens/log.txt"
    wildcard_constraints:
        which="\w+"
    params:
        seed=0,
        shell="""
        echo Density | gmx energy -f {input.edr:q} -o {output.dens:q} > {output.log:q}
        """,
        guix="gromacs"
    script:
        "../run_smk_rule.py"

localrules: TCM_box_dielectric
rule TCM_box_dielectric:
    input:
        tpr="results/MD/TCM/box/md/md.tpr",
        xtc="results/MD/TCM/box/md/md.xtc"
    output:
        directory("results/MD/TCM/box/ana/diel"),
        mtot="results/MD/TCM/box/ana/diel/mtot.xvg",
        eps="results/MD/TCM/box/ana/diel/epsilon.xvg",
        aver="results/MD/TCM/box/ana/diel/aver.xvg",
        dipdist="results/MD/TCM/box/ana/diel/dipdist.xvg",
        dipcorr="results/MD/TCM/box/ana/diel/dipcorr.xvg",
        log="results/MD/TCM/box/ana/diel/log.txt"
    params:
        shell="""
        echo 0 | gmx dipoles -f {input.xtc:q} -s {input.tpr:q} -corr mol -P 1 \
            -o {output.mtot:q} -eps {output.eps:q} -a {output.aver:q} -d {output.dipdist:q} -c {output.dipcorr:q} -fitfn exp \
            | tee {output.log:q}
        """,
        guix="gromacs"
    script:
        "../run_smk_rule.py"

localrules: PBLG_long_gro
rule PBLG_long_gro:
    input:
        "results/molecules/PBLG/adjusted.pdb",
        "results/molecules/PBLG/buildspec.json"
    output:
        "results/MD/PBLG/long/PBLG_long.gro",
        "results/MD/PBLG/long/PBLG_long_numbers.json"
    params:
        script=workflow.source_path("../../scripts/PBLG/build_polymer.py"),
        N_monomers=140,
        shell="""
        python3 {params.script:q} build {input[0]:q} {input[1]:q} {params.N_monomers} {output[0]:q} {output[1]:q}
        """,
        guix="python"
    script:
        "../run_smk_rule.py"

localrules: PBLG_align_z_axis
rule PBLG_align_z_axis:
    input:
        "results/MD/PBLG/long/PBLG_long.gro",
        "results/MD/PBLG/long/PBLG_long_numbers.json"
    output:
        "results/MD/PBLG/long/PBLG_long_aligned.gro"
    params:
        script=workflow.source_path("../../scripts/PBLG/align_z_axis.py"),
        shell="""
        python3 {params.script:q} {input[0]:q} {input[1]:q} {output[0]:q}
        """,
        guix="python"
    script:
        "../run_smk_rule.py"

localrules: PBLG_periodic_gro
rule PBLG_periodic_gro:
    input:
        struct="results/MD/PBLG/long/PBLG_long_aligned.gro",
        numbers="results/MD/PBLG/long/PBLG_long_numbers.json",
        bondi_radii="data/bondi_radii.json"
    output:
        "results/MD/PBLG/periodic/box/box.gro",
        "results/MD/PBLG/periodic/PBLG_periodic_numbers.json"
    params:
        script=workflow.source_path("../../scripts/MD/PBLG/periodic/extract_periodic.py"),
        bondi_radius_scale=0.5,
        Nmatch=3,
        Ntest_collision=3,
        dimage=1.1,
        num_monomers=18,
        sz=2.7,
        shell="""
        python3 {params.script:q} {input.struct:q} {input.numbers:q} {params.num_monomers} \
            {input.bondi_radii:q} {params.bondi_radius_scale} {params.Nmatch} \
            {params.Ntest_collision} {params.dimage} \
            --sz {params.sz:q} \
            {output[0]:q} {output[1]:q}
        """,
        guix="python"
    script:
        "../run_smk_rule.py"

localrules: PBLG_periodic_topology
rule PBLG_periodic_topology:
    input:
        polymer="results/MD/PBLG/periodic/box/box.gro",
        prmtop="results/FF/PBLG/three_capped/AMBER/mol.prmtop",
        charges="results/FF/PBLG/adjusted/charges/RESP/HF/charges_adjusted",
        three_capped_numbers="results/FF/PBLG/three_capped/numbers.json",
        polymer_numbers="results/MD/PBLG/periodic/PBLG_periodic_numbers.json"
    output:
        "results/MD/PBLG/periodic/box/box.top"
    params:
        script=workflow.source_path("../../scripts/PBLG/make_topology.py"),
        shell="""
        python3 {params.script:q} {input.polymer:q} {input.prmtop:q} {input.charges:q} \
            {input.three_capped_numbers:q} {input.polymer_numbers:q} --periodic \
            {output[0]:q}
        """,
        guix="python"
    script:
        "../run_smk_rule.py"

localrules: PBLG_periodic_solv
rule PBLG_periodic_solv:
    input:
        PBLG="results/MD/PBLG/periodic/box/box.gro",
        TCM_box="results/MD/TCM/box/npt2/npt2.gro"
    output:
        "results/MD/PBLG/periodic/box/solv.gro"
    params:
        scale=0.3,
        shell="""
        gmx solvate -cp {input.PBLG:q} -cs {input.TCM_box:q} -o {output[0]:q} -scale {params.scale:q}
        """,
        guix="gromacs"
    script:
        "../run_smk_rule.py"

localrules: PBLG_periodic_solv_topology
rule PBLG_periodic_solv_topology:
    input:
        gro="results/MD/PBLG/periodic/box/solv.gro",
        PBLGtop="results/MD/PBLG/periodic/box/box.top",
        TCMtop="results/FF/TCM/TCM/AMBER.top"
    output:
        "results/MD/PBLG/periodic/box/solv.top"
    params:
        script=workflow.source_path("../../scripts/MD/PBLG/make_box_topology.py"),
        shell="""
        python3 {params.script:q} {input.gro:q} {input.PBLGtop:q} --top_solv {input.TCMtop:q} {output[0]:q}
        """,
        guix="python"
    script:
        "../run_smk_rule.py"

localrules: PBLG_periodic_box_index
rule PBLG_periodic_box_index:
    input:
        "results/MD/PBLG/periodic/box/solv.gro"
    output:
        "results/MD/PBLG/periodic/index.ndx"
    params:
        shell="""
        tmpfile="$(mktemp --suffix=".ndx")"
        trap 'rm "$tmpfile"' EXIT
        echo -e "r PMO\\nr TCM\\nname 0 polymer\\nname 1 solvent\\nq" \
            | gmx make_ndx -f {input[0]:q} -n "$tmpfile" -o {output[0]:q}
        """,
        guix="gromacs"
    script:
        "../run_smk_rule.py"

rule PBLG_periodic_MD_em:
    input:
        gro="results/MD/PBLG/periodic/box/solv.gro",
        top="results/MD/PBLG/periodic/box/solv.top",
        mdp="scripts/MD/PBLG/periodic/em.mdp"
    output:
        directory("results/MD/PBLG/periodic/em"),
        "results/MD/PBLG/periodic/em/em.gro",
        "results/MD/PBLG/periodic/em/finished"
    params:
        shell="""
        export OMP_NUM_THREADS={resources.GMX_OMP}
        gmx grompp -f {input.mdp:q} -c {input.gro:q} -p {input.top:q} -r {input.gro:q} -o {output[0]:q}/em.tpr -po {output[0]:q}/mdout.mdp
        cd {output[0]:q}
        gmx mdrun -g -v -deffnm em -ntmpi {resources.GMX_MPI}
        touch finished
        """,
        guix="gromacs"
    script:
        "../run_smk_rule.py"

rule PBLG_periodic_MD_nvt:
    input:
        gro="results/MD/PBLG/periodic/em/em.gro",
        top="results/MD/PBLG/periodic/box/solv.top",
        index="results/MD/PBLG/periodic/index.ndx",
        mdp="scripts/MD/PBLG/periodic/nvt.mdp"
    output:
        directory("results/MD/PBLG/periodic/nvt"),
        "results/MD/PBLG/periodic/nvt/nvt.gro",
        "results/MD/PBLG/periodic/nvt/nvt.cpt",
        "results/MD/PBLG/periodic/nvt/finished"
    params:
        seed=config['seed'],
        shell="""
        export OMP_NUM_THREADS={resources.GMX_OMP}
        sed 's/$SEED/{params.seed}/g' {input.mdp:q} > {output[0]:q}/nvt.mdp
        gmx grompp -f {output[0]:q}/nvt.mdp -c {input.gro:q} -p {input.top:q} -n {input.index:q} -o {output[0]:q}/nvt.tpr -po {output[0]:q}/mdout.mdp
        cd {output[0]:q}
        gmx mdrun -g -v -deffnm nvt -ntmpi {resources.GMX_MPI}
        touch finished
        """,
        guix="gromacs"
    script:
        "../run_smk_rule.py"

rule PBLG_periodic_MD_npt:
    input:
        gro="results/MD/PBLG/periodic/nvt/nvt.gro",
        cpt="results/MD/PBLG/periodic/nvt/nvt.cpt",
        top="results/MD/PBLG/periodic/box/solv.top",
        index="results/MD/PBLG/periodic/index.ndx",
        mdp="scripts/MD/PBLG/periodic/npt.mdp"
    output:
        directory("results/MD/PBLG/periodic/npt"),
        "results/MD/PBLG/periodic/npt/npt.gro",
        "results/MD/PBLG/periodic/npt/npt.cpt",
        "results/MD/PBLG/periodic/npt/npt.tpr",
        "results/MD/PBLG/periodic/npt/finished"
    params:
        seed=config['seed'],
        shell="""
        export OMP_NUM_THREADS={resources.GMX_OMP}
        sed 's/$SEED/{params.seed}/g' {input.mdp:q} > {output[0]:q}/npt.mdp
        gmx grompp -f {output[0]:q}/npt.mdp -c {input.gro:q} -t {input.cpt:q} -p {input.top:q} -n {input.index:q} -o {output[0]:q}/npt.tpr -po {output[0]:q}/mdout.mdp
        cd {output[0]:q}
        gmx mdrun -g -v -deffnm npt -ntmpi {resources.GMX_MPI}
        touch finished
        """,
        guix="gromacs"
    script:
        "../run_smk_rule.py"

localrules: PBLG_periodic_solv_whole
rule PBLG_periodic_solv_whole:
    input:
        gro="results/MD/PBLG/periodic/npt/npt.gro",
        tpr="results/MD/PBLG/periodic/npt/npt.tpr",
        index="results/MD/PBLG/periodic/index.ndx"
    output:
        gro="results/MD/PBLG/periodic/PBLG_periodic_solv.gro"
    params:
        shell="""
        echo 0 | gmx trjconv -f {input.gro:q} -s {input.tpr:q} -pbc whole -o {output.gro:q}
        """,
        guix="gromacs"
    script:
        "../run_smk_rule.py"

localrules: PBLG_periodic_final
rule PBLG_periodic_final:
    input:
        gro="results/MD/PBLG/periodic/npt/npt.gro",
        tpr="results/MD/PBLG/periodic/npt/npt.tpr",
        index="results/MD/PBLG/periodic/index.ndx"
    output:
        gro="results/MD/PBLG/periodic/final.gro"
    params:
        shell="""
        echo 0 | gmx trjconv -f {input.gro:q} -s {input.tpr:q} -n {input.index:q} -pbc whole -o {output.gro:q}
        """,
        guix="gromacs"
    script:
        "../run_smk_rule.py"

localrules: PBLG_2periodic_gro
rule PBLG_2periodic_gro:
    input:
        single="results/MD/PBLG/periodic/final.gro"
    output:
        "results/MD/PBLG/2periodic/PBLG_2periodic.gro"
    params:
        sxy=4.2,
        script=workflow.source_path("../../scripts/MD/PBLG/2periodic/make_box.py"),
        shell="""
        python3 {params.script:q} {input.single:q} {output[0]:q} --sxy {params.sxy:q}
        """,
        guix="python"
    script:
        "../run_smk_rule.py"

localrules: PBLG_2periodic_solv
rule PBLG_2periodic_solv:
    input:
        PBLG="results/MD/PBLG/2periodic/PBLG_2periodic.gro",
        TCM_box="results/MD/TCM/box/npt2/npt2.gro"
    output:
        "results/MD/PBLG/2periodic/box/solv.gro"
    params:
        scale=0.3,
        shell="""
        gmx solvate -cp {input.PBLG:q} -cs {input.TCM_box:q} -o {output[0]:q} -scale {params.scale:q}
        """,
        guix="gromacs"
    script:
        "../run_smk_rule.py"

localrules: PBLG_2periodic_box_topology
rule PBLG_2periodic_box_topology:
    input:
        gro="results/MD/PBLG/2periodic/box/solv.gro",
        PBLGtop="results/MD/PBLG/periodic/box/box.top",
        TCMtop="results/FF/TCM/TCM/AMBER.top"
    output:
        "results/MD/PBLG/2periodic/box/box.top"
    params:
        script=workflow.source_path("../../scripts/MD/PBLG/make_box_topology.py"),
        shell="""
        python3 {params.script:q} {input.gro:q} {input.PBLGtop:q} {output[0]:q} --num_polymer 2
        """,
        guix="python"
    script:
        "../run_smk_rule.py"

localrules: PBLG_2periodic_solv_topology
rule PBLG_2periodic_solv_topology:
    input:
        gro="results/MD/PBLG/2periodic/box/solv.gro",
        PBLGtop="results/MD/PBLG/periodic/box/box.top",
        TCMtop="results/FF/TCM/TCM/AMBER.top"
    output:
        "results/MD/PBLG/2periodic/box/solv.top"
    params:
        script=workflow.source_path("../../scripts/MD/PBLG/make_box_topology.py"),
        shell="""
        python3 {params.script:q} {input.gro:q} {input.PBLGtop:q} --top_solv {input.TCMtop:q} {output[0]:q} --num_polymer 2
        """,
        guix="python"
    script:
        "../run_smk_rule.py"

localrules: PBLG_2periodic_box_index
rule PBLG_2periodic_box_index:
    input:
        "results/MD/PBLG/2periodic/box/solv.gro"
    output:
        "results/MD/PBLG/2periodic/index.ndx"
    params:
        shell="""
        tmpfile="$(mktemp --suffix=".ndx")"
        trap 'rm "$tmpfile"' EXIT
        echo -e "r PMO\\nr TCM\\nname 0 polymer\\nname 1 solvent\\nq" \
            | gmx make_ndx -f {input[0]:q} -n "$tmpfile" -o {output[0]:q}
        """,
        guix="gromacs"
    script:
        "../run_smk_rule.py"

rule PBLG_2periodic_MD_em:
    input:
        gro="results/MD/PBLG/2periodic/box/solv.gro",
        top="results/MD/PBLG/2periodic/box/solv.top",
        mdp="scripts/MD/PBLG/2periodic/em.mdp"
    output:
        directory("results/MD/PBLG/2periodic/em"),
        "results/MD/PBLG/2periodic/em/em.gro",
        "results/MD/PBLG/2periodic/em/finished"
    params:
        shell="""
        export OMP_NUM_THREADS={resources.GMX_OMP}
        gmx grompp -f {input.mdp:q} -c {input.gro:q} -p {input.top:q} -r {input.gro:q} -o {output[0]:q}/em.tpr -po {output[0]:q}/mdout.mdp
        cd {output[0]:q}
        gmx mdrun -g -v -deffnm em -ntmpi {resources.GMX_MPI}
        touch finished
        """,
        guix="gromacs"
    script:
        "../run_smk_rule.py"

rule PBLG_2periodic_MD_nvt:
    input:
        gro="results/MD/PBLG/2periodic/em/em.gro",
        top="results/MD/PBLG/2periodic/box/solv.top",
        index="results/MD/PBLG/2periodic/index.ndx",
        mdp="scripts/MD/PBLG/2periodic/nvt.mdp"
    output:
        directory("results/MD/PBLG/2periodic/nvt"),
        "results/MD/PBLG/2periodic/nvt/nvt.gro",
        "results/MD/PBLG/2periodic/nvt/nvt.cpt",
        "results/MD/PBLG/2periodic/nvt/finished"
    params:
        seed=config['seed'],
        shell="""
        export OMP_NUM_THREADS={resources.GMX_OMP}
        sed 's/$SEED/{params.seed}/g' {input.mdp:q} > {output[0]:q}/nvt.mdp
        gmx grompp -f {output[0]:q}/nvt.mdp -c {input.gro:q} -p {input.top:q} -n {input.index:q} -o {output[0]:q}/nvt.tpr -po {output[0]:q}/mdout.mdp
        cd {output[0]:q}
        gmx mdrun -g -v -deffnm nvt -ntmpi {resources.GMX_MPI}
        touch finished
        """,
        guix="gromacs"
    script:
        "../run_smk_rule.py"

rule PBLG_2periodic_MD_npt:
    input:
        gro="results/MD/PBLG/2periodic/nvt/nvt.gro",
        cpt="results/MD/PBLG/2periodic/nvt/nvt.cpt",
        top="results/MD/PBLG/2periodic/box/solv.top",
        index="results/MD/PBLG/2periodic/index.ndx",
        mdp="scripts/MD/PBLG/2periodic/npt.mdp"
    output:
        directory("results/MD/PBLG/2periodic/npt"),
        "results/MD/PBLG/2periodic/npt/npt.gro",
        "results/MD/PBLG/2periodic/npt/npt.cpt",
        "results/MD/PBLG/2periodic/npt/npt.tpr",
        "results/MD/PBLG/2periodic/npt/finished"
    params:
        seed=config['seed'],
        shell="""
        export OMP_NUM_THREADS={resources.GMX_OMP}
        sed 's/$SEED/{params.seed}/g' {input.mdp:q} > {output[0]:q}/npt.mdp
        gmx grompp -f {output[0]:q}/npt.mdp -c {input.gro:q} -t {input.cpt:q} -p {input.top:q} -n {input.index:q} -o {output[0]:q}/npt.tpr -po {output[0]:q}/mdout.mdp
        cd {output[0]:q}
        gmx mdrun -g -v -deffnm npt -ntmpi {resources.GMX_MPI}
        touch finished
        """,
        guix="gromacs"
    script:
        "../run_smk_rule.py"

localrules: PBLG_2periodic_final
rule PBLG_2periodic_final:
    input:
        gro="results/MD/PBLG/2periodic/npt/npt.gro",
        tpr="results/MD/PBLG/2periodic/npt/npt.tpr",
        index="results/MD/PBLG/2periodic/index.ndx"
    output:
        gro="results/MD/PBLG/2periodic/final.gro"
    params:
        shell="""
        echo 0 | gmx trjconv -f {input.gro:q} -s {input.tpr:q} -n {input.index:q} -pbc whole -o {output.gro:q}
        """,
        guix="gromacs"
    script:
        "../run_smk_rule.py"
