import h5py
import numpy as np
from correrr import inseq

import argparse
from pathlib import Path
import json

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('path')
    parser.add_argument('mode')
    parser.add_argument('hbonds_receptor')
    parser.add_argument('max_HA_distance')
    parser.add_argument('min_angle')
    parser.add_argument('first_index')
    parser.add_argument('last_index')
    parser.add_argument('num_stages')
    parser.add_argument('output')
    parser.add_argument('--Nmol', default=1)
    args = parser.parse_args()

    path = Path(args.path)

    mode = args.mode

    Nmol = int(args.Nmol)

    with open(args.hbonds_receptor) as file:
        hbonds_receptor = json.load(file)

    max_HA_distance = float(args.max_HA_distance)
    min_angle = float(args.min_angle)

    first_index = int(args.first_index)
    last_index = int(args.last_index)
    num_stages = int(args.num_stages)

    results = {f'acc_index{i}': {'Xs': [], 'Nframes': 0}
               for i in hbonds_receptor['acceptors']}

    Nframes = 0
    Nchains = None

    for index in range(first_index, last_index+1):
        filename = path / f"{index}/{num_stages}/RDCs_{mode}.hdf5"
        with h5py.File(filename, 'r') as file:
            if file.attrs['mode'] != mode:
                raise RuntimeError('Different modes!')

            if mode != 'A':
                X = np.array(file['D'])
            else:
                X = np.array(file['A'])

        Nframes += X.shape[1]

        filename = path / f"{index}/{num_stages}/hbonds.hdf5"
        print('Reading', filename, flush=True)
        with h5py.File(filename) as file:
            if Nchains is None:
                Nchains = file.attrs['Nchains']
            else:
                assert Nchains == file.attrs['Nchains']

            for k in range(Nchains):
                c_group = file[f'chain{k}']
                for groupname, group_results in results.items():
                    group = c_group[groupname]
                    HA_dist = np.array(group['HA_distance'])
                    hbond_angle = np.array(group['angle']) * 180 / np.pi

                    mask1 = (HA_dist <= max_HA_distance) & (hbond_angle >= min_angle)

                    all_indices = np.array(group['frame_index'])

                    indices = all_indices[mask1]

                    all_mol_indices = np.array(group['mol_index'])
                    mol_indices = all_mol_indices[mask1]

                    sums = np.zeros(X.size, dtype='float64')

                    sum_indices = ((mol_indices * X.shape[1] * X.shape[2] + indices * X.shape[2])[:, None] + np.arange(X.shape[2])).ravel()
                    np.add.at(sums, sum_indices, X.ravel()[sum_indices])
                    sums = sums.reshape(X.shape)

                    if k == 0:
                        group_results['Xs'].append(sums)
                    else:
                        group_results['Xs'][-1] += sums

                    group_results['Nframes'] += len(indices)

    for groupname, group_results in results.items():
        for i, X in enumerate(group_results['Xs']):
            group_results['Xs'][i] = np.mean(X, axis=0)

    with h5py.File(args.output, 'w') as file:
        file.attrs['Nframes'] = Nframes
        file.attrs['Nmol'] = Nmol
        file.attrs['mode'] = mode
        for groupname, group_results in results.items():
            group = file.create_group(groupname)
            group.attrs['Nframes'] = group_results['Nframes']

            Ntot = sum(len(X) for X in group_results['Xs'])
            mean = sum(np.sum(X, axis=0) for X in group_results['Xs']) / Ntot

            group.create_dataset('mean', data=mean)
            group.attrs['Ntot'] = Ntot

if __name__ == '__main__':
    main()
