import common

from pathlib import Path

def MD_free_box_input(w):
    p = common.am_path(w)
    return {
        'box': f"results/MD/{p}/final.gro",
        'old_index': f"results/MD/{p}/index.ndx",
        'ligand': f"results/molecules/small/chosen/{w.enantiomer}-{w.molname}.gro"
    }

localrules: MD_free_box_ref
rule MD_free_box_ref:
    input:
        unpack(MD_free_box_input)
    output:
        "results/MD/free/{am}/{lnum}/{enantiomer}-{molname}/ref_box.gro"
    wildcard_constraints:
        am=common.re_am,
        lnum=common.re_lnum,
        enantiomer=common.re_enantiomer,
        molname=common.re_molname,
    params:
        scale=0.3,
        shell="""
        gmx insert-molecules -f {input.box:q} -n {input.old_index:q} -ci {input.ligand:q} \
            -seed 0 -nmol {wildcards.lnum} -try 100000 -scale {params.scale} \
            -o {output[0]:q}
            """,
        guix="gromacs"
    script:
        "../run_smk_rule.py"

localrules: MD_free_solv_ref
rule MD_free_solv_ref:
    input:
        box="results/MD/free/{am}/{lnum}/{enantiomer}-{molname}/ref_box.gro",
        solvbox="results/MD/TCM/box/npt2/npt2.gro"
    output:
        "results/MD/free/{am}/{lnum}/{enantiomer}-{molname}/ref_solv.gro"
    wildcard_constraints:
        am=common.re_am,
        lnum=common.re_lnum,
        enantiomer=common.re_enantiomer,
        molname=common.re_molname,
    params:
        scale=0.4,
        shell="""
        gmx solvate -cp {input.box:q} -cs {input.solvbox:q} -scale {params.scale} -o {output[0]:q}
        """,
        guix="gromacs"
    script:
        "../run_smk_rule.py"

localrules: MD_free_box
rule MD_free_box:
    input:
        unpack(MD_free_box_input)
    output:
        "results/MD/free/{am}/{lnum}/{enantiomer}-{molname}/chains/{index}/box.gro"
    wildcard_constraints:
        am=common.re_am,
        lnum=common.re_lnum,
        enantiomer=common.re_enantiomer,
        molname=common.re_molname,
        index=common.re_index
    params:
        shell="""
        gmx insert-molecules -f {input.box:q} -n {input.old_index:q} -ci {input.ligand:q} \
            -seed {wildcards.index} -nmol {wildcards.lnum} -try 100000 \
            -o {output[0]:q}
            """,
        guix="gromacs"
    script:
        "../run_smk_rule.py"

localrules: MD_free_solv
rule MD_free_solv:
    input:
        box="results/MD/free/{am}/{lnum}/{enantiomer}-{molname}/chains/{index}/box.gro",
        solvbox="results/MD/TCM/box/npt2/npt2.gro",
        ref="results/MD/free/{am}/{lnum}/{enantiomer}-{molname}/ref_solv.gro"
    output:
        "results/MD/free/{am}/{lnum}/{enantiomer}-{molname}/chains/{index}/solv.gro"
    wildcard_constraints:
        am=common.re_am,
        lnum=common.re_lnum,
        enantiomer=common.re_enantiomer,
        molname=common.re_molname,
        index=common.re_index
    params:
        script=workflow.source_path("../../scripts/MD/solvate_same_number.py"),
        shell="""
        python3 {params.script:q} {input.box:q} {input.solvbox:q} {input.ref:q} {output[0]:q}
        """,
        guix=["gromacs", "python"]
    script:
        "../run_smk_rule.py"

def small_ligand_top_input(w):
    name = common.choose_name(f'{w.enantiomer}-{w.molname}')
    return f"results/FF/small/{name}/AMBER.top"

def am_top_input(w):
    p = common.am_path(w)
    return f"results/MD/{p}/box/box.top"

localrules: MD_free_solv_topology
rule MD_free_solv_topology:
    input:
        gro="results/MD/free/{am}/{lnum}/{enantiomer}-{molname}/ref_solv.gro",
        amtop=am_top_input,
        TCMtop="results/FF/TCM/TCM/AMBER.top",
        ligtop=small_ligand_top_input
    output:
        "results/MD/free/{am}/{lnum}/{enantiomer}-{molname}/solv.top"
    wildcard_constraints:
        am=common.re_am,
        lnum=common.re_lnum,
        enantiomer="[RSmp]+",
        molname="[\w-]+"
    params:
        script=workflow.source_path("../../scripts/MD/free/make_box_topology.py"),
        shell="""
        python3 {params.script:q} {input.gro:q} {input.amtop:q} {input.TCMtop:q} {input.ligtop:q} \
            {output[0]:q} --num_ligand {wildcards.lnum}
        """,
        guix="python"
    script:
        "../run_smk_rule.py"

localrules: MD_free_index
rule MD_free_index:
    input:
        "results/MD/free/{am}/{lnum}/{enantiomer}-{molname}/ref_solv.gro"
    output:
        "results/MD/free/{am}/{lnum}/{enantiomer}-{molname}/index.ndx"
    wildcard_constraints:
        am=common.re_am,
        lnum=common.re_lnum,
        enantiomer="[RSmp]+",
        molname="[\w-]+"
    params:
        shell="""
        tmpfile="$(mktemp --suffix=".ndx")"
        trap 'rm "$tmpfile"' EXIT
        echo -e "r PMO \\nr TCM\\nr LIG\\nr PMO | r LIG\\nname 0 polymer\\nname 1 solvent\\nname 2 ligand\\nname 3 solutes\\nq" \
            | gmx make_ndx -f {input[0]:q} -n "$tmpfile" -o {output[0]:q}
        """,
        guix="gromacs"
    script:
        "../run_smk_rule.py"

rule MD_free_em:
    input:
        gro="results/MD/free/{am}/{lnum}/{enantiomer}-{molname}/chains/{index}/solv.gro",
        top="results/MD/free/{am}/{lnum}/{enantiomer}-{molname}/solv.top",
        mdp="scripts/MD/free/em.mdp"
    output:
        directory("results/MD/free/{am}/{lnum}/{enantiomer}-{molname}/chains/{index}/em"),
        "results/MD/free/{am}/{lnum}/{enantiomer}-{molname}/chains/{index}/em/em.gro",
        "results/MD/free/{am}/{lnum}/{enantiomer}-{molname}/chains/{index}/em/finished"
    wildcard_constraints:
        am=common.re_am,
        lnum=common.re_lnum,
        enantiomer="[RSmp]+",
        molname="[\w-]+",
        index="\d+"
    group:
        "freeMDem"
    params:
        shell="""
        export OMP_NUM_THREADS=1
        gmx grompp -f {input.mdp:q} -c {input.gro:q} -p {input.top:q} -o {output[0]:q}/em.tpr -po {output[0]:q}/mdout.mdp
        cd {output[0]:q}
        gmx mdrun -g -v -deffnm em -ntmpi 1
        touch finished
        """,
        guix="gromacs"
    script:
        "../run_smk_rule.py"

rule MD_free_nvt:
    input:
        gro="results/MD/free/{am}/{lnum}/{enantiomer}-{molname}/chains/{index}/em/em.gro",
        top="results/MD/free/{am}/{lnum}/{enantiomer}-{molname}/solv.top",
        index="results/MD/free/{am}/{lnum}/{enantiomer}-{molname}/index.ndx",
        mdp="scripts/MD/free/nvt.mdp"
    output:
        directory("results/MD/free/{am}/{lnum}/{enantiomer}-{molname}/chains/{index}/nvt"),
        "results/MD/free/{am}/{lnum}/{enantiomer}-{molname}/chains/{index}/nvt/nvt.gro",
        "results/MD/free/{am}/{lnum}/{enantiomer}-{molname}/chains/{index}/nvt/nvt.cpt",
        "results/MD/free/{am}/{lnum}/{enantiomer}-{molname}/chains/{index}/nvt/finished"
    wildcard_constraints:
        am=common.re_am,
        lnum=common.re_lnum,
        enantiomer="[RSmp]+",
        molname="[\w-]+",
        index="\d+"
    group:
        "freeMDnvt"
    params:
        seed=lambda w: config['seed'] + int(w.index),
        shell="""
        export OMP_NUM_THREADS=1
        sed 's/$SEED/{params.seed}/g' {input.mdp:q} > {output[0]:q}/nvt.mdp
        gmx grompp -f {output[0]:q}/nvt.mdp -c {input.gro:q} -p {input.top:q} -n {input.index:q} -o {output[0]:q}/nvt.tpr -po {output[0]:q}/mdout.mdp
        cd {output[0]:q}
        gmx mdrun -g -v -deffnm nvt -ntmpi 1
        touch finished
        """,
        guix="gromacs"
    script:
        "../run_smk_rule.py"

rule MD_free_npt:
    input:
        gro="results/MD/free/{am}/{lnum}/{enantiomer}-{molname}/chains/{index}/nvt/nvt.gro",
        cpt="results/MD/free/{am}/{lnum}/{enantiomer}-{molname}/chains/{index}/nvt/nvt.cpt",
        top="results/MD/free/{am}/{lnum}/{enantiomer}-{molname}/solv.top",
        index="results/MD/free/{am}/{lnum}/{enantiomer}-{molname}/index.ndx",
        mdp="scripts/MD/free/npt.mdp"
    output:
        directory("results/MD/free/{am}/{lnum}/{enantiomer}-{molname}/chains/{index}/npt"),
        "results/MD/free/{am}/{lnum}/{enantiomer}-{molname}/chains/{index}/npt/npt.gro",
        "results/MD/free/{am}/{lnum}/{enantiomer}-{molname}/chains/{index}/npt/npt.cpt",
        "results/MD/free/{am}/{lnum}/{enantiomer}-{molname}/chains/{index}/npt/finished"
    wildcard_constraints:
        am=common.re_am,
        lnum=common.re_lnum,
        enantiomer="[RSmp]+",
        molname="[\w-]+",
        index="\d+"
    group:
        "freeMDnpt"
    params:
        seed=lambda w: config['seed'] + int(w.index),
        shell="""
        export OMP_NUM_THREADS=1
        sed 's/$SEED/{params.seed}/g' {input.mdp:q} > {output[0]:q}/npt.mdp
        gmx grompp -f {output[0]:q}/npt.mdp -c {input.gro:q} -t {input.cpt:q} -p {input.top:q} -n {input.index:q} -o {output[0]:q}/npt.tpr -po {output[0]:q}/mdout.mdp
        cd {output[0]:q}
        gmx mdrun -g -v -deffnm npt -ntmpi 1
        touch finished
        """,
        guix="gromacs"
    script:
        "../run_smk_rule.py"

def MD_free_md_inputs(w):
    stage=int(w.stage)
    if stage == 1:
        return {
            'gro': f"results/MD/free/{w.am}/{w.lnum}/{w.enantiomer}-{w.molname}/chains/{w.index}/npt/npt.gro",
            'cpt': f"results/MD/free/{w.am}/{w.lnum}/{w.enantiomer}-{w.molname}/chains/{w.index}/npt/npt.cpt"
        }
    else:
        return {
            'gro': f"results/MD/free/{w.am}/{w.lnum}/{w.enantiomer}-{w.molname}/chains/{w.index}/md/{stage-1}/md.gro",
            'cpt': f"results/MD/free/{w.am}/{w.lnum}/{w.enantiomer}-{w.molname}/chains/{w.index}/md/{stage-1}/md.cpt"
        }

rule MD_free_md:
    input:
        unpack(MD_free_md_inputs),
        top="results/MD/free/{am}/{lnum}/{enantiomer}-{molname}/solv.top",
        index="results/MD/free/{am}/{lnum}/{enantiomer}-{molname}/index.ndx",
        mdp="scripts/MD/free/md.mdp"
    output:
        directory("results/MD/free/{am}/{lnum}/{enantiomer}-{molname}/chains/{index}/md/{stage}"),
        "results/MD/free/{am}/{lnum}/{enantiomer}-{molname}/chains/{index}/md/{stage}/md.gro",
        "results/MD/free/{am}/{lnum}/{enantiomer}-{molname}/chains/{index}/md/{stage}/md.xtc",
        "results/MD/free/{am}/{lnum}/{enantiomer}-{molname}/chains/{index}/md/{stage}/md.cpt",
        "results/MD/free/{am}/{lnum}/{enantiomer}-{molname}/chains/{index}/md/{stage}/md.tpr",
        "results/MD/free/{am}/{lnum}/{enantiomer}-{molname}/chains/{index}/md/{stage}/finished"
    wildcard_constraints:
        am=common.re_am,
        lnum=common.re_lnum,
        enantiomer="[RSmp]+",
        molname="[\w-]+",
        index="\d+",
        stage="\d+"
    group:
        lambda w: f"freeMD{w.stage}"
    params:
        seed=lambda w: config['seed'] + 1000*int(w.index) + int(w.stage),
        nsteps_single=config['nsteps_single'],
        shell="""
        export OMP_NUM_THREADS=1
        sed 's/$NSTEPS/{params.nsteps_single}/g;s/$SEED/{params.seed}/g' {input.mdp:q} > {output[0]:q}/md.mdp
        gmx grompp -f {output[0]:q}/md.mdp -c {input.gro:q} -t {input.cpt:q} -p {input.top:q} -n {input.index:q} -o {output[0]:q}/md.tpr -po {output[0]:q}/mdout.mdp
        cd {output[0]:q}
        gmx mdrun -g -v -deffnm md -ntmpi 1
        touch finished
        """,
        guix="gromacs"
    script:
        "../run_smk_rule.py"

rule MD_free_trjconv:
    input:
        gro="results/MD/free/{am}/{lnum}/{enantiomer}-{molname}/chains/{index}/md/{stage}/md.gro",
        xtc="results/MD/free/{am}/{lnum}/{enantiomer}-{molname}/chains/{index}/md/{stage}/md.xtc",
        tpr="results/MD/free/{am}/{lnum}/{enantiomer}-{molname}/chains/{index}/md/{stage}/md.tpr",
        index="results/MD/free/{am}/{lnum}/{enantiomer}-{molname}/index.ndx"
    output:
        gro="results/MD/free/{am}/{lnum}/{enantiomer}-{molname}/chains/{index}/md/{stage}_whole/whole.gro",
        tpr="results/MD/free/{am}/{lnum}/{enantiomer}-{molname}/chains/{index}/md/{stage}_whole/whole.tpr",
        xtc="results/MD/free/{am}/{lnum}/{enantiomer}-{molname}/chains/{index}/md/{stage}_whole/whole.xtc"
    wildcard_constraints:
        am=common.re_am,
        lnum=common.re_lnum,
        enantiomer="[RSmp]+",
        molname="[\w-]+",
        index="\d+",
        stage="\d+"
    group:
        lambda w: f"freeMDtrjc{w.stage}"
    params:
        tend=config['nsteps_single'] * 0.002,
        shell="""
        echo 3 | gmx trjconv -f {input.gro:q} -s {input.tpr:q} -pbc whole -n {input.index:q} -o {output.gro:q}
        echo 3 | gmx convert-tpr -s {input.tpr:q} -n {input.index:q} -o {output.tpr:q}
        echo 0 | gmx trjconv -f {input.xtc:q} -s {output.tpr:q} -pbc whole -e {params.tend} -o {output.xtc:q}
        """,
        guix="gromacs"
    script:
        "../run_smk_rule.py"
