import parmed
from scipy.spatial.transform import Rotation
import numpy as np

import argparse
import json

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('gro_receptor_solv')
    parser.add_argument('gro_ligand')
    parser.add_argument('top_receptor')
    parser.add_argument('top_ligand')
    parser.add_argument('top_solv')
    parser.add_argument('buildspec')
    parser.add_argument('hbonds_receptor')
    parser.add_argument('hbond_ligand')
    parser.add_argument('output')
    parser.add_argument('--seed', default=1)
    parser.add_argument('--max_tries', default=10000000)
    args = parser.parse_args()

    seed = int(args.seed)
    max_tries = int(args.max_tries)

    receptor_solv_gro = parmed.load_file(args.gro_receptor_solv)

    receptor = parmed.load_file(args.top_receptor)
    receptor.coordinates = receptor_solv_gro[':PMO'].coordinates

    solv_gro = receptor_solv_gro[':TCM']
    single_solv = parmed.load_file(args.top_solv)
    solv = len(solv_gro.residues) * single_solv
    solv.coordinates = solv_gro.coordinates

    ligand_gro = parmed.load_file(args.gro_ligand)
    ligand = parmed.load_file(args.top_ligand)
    ligand.coordinates = ligand_gro.coordinates

    with open(args.buildspec) as file:
        buildspec = json.load(file)

    with open(args.hbonds_receptor) as file:
        hbonds_receptor = json.load(file)

    with open(args.hbond_ligand) as file:
        hbond_ligand = json.load(file)

    r_receptor = np.array([a.rmin for a in receptor.atoms])
    r_ligand = np.array([a.rmin for a in ligand.atoms])

    p_rec = receptor.coordinates

    ihydrogen = hbond_ligand['hydrogen'] - 1

    i_reference = buildspec['new_indices'][hbonds_receptor['reference'] - 1] - len(buildspec['fullcaps'][0])

    p_lig = ligand.coordinates
    pref_hydrogen = p_lig[ihydrogen]
    rref_hydrogen = r_ligand[ihydrogen]

    restraint_radius = hbonds_receptor['restraint_radius']

    receptor_box = receptor_solv_gro.box
    sigma_receptor = np.array([a.sigma for a in receptor.atoms])
    sigma_ligand = np.array([a.sigma for a in ligand.atoms])
    sigma_solv = np.array([a.sigma for a in single_solv.atoms])

    rsq_min_lig_rec = (0.5*(sigma_receptor[:, None] + sigma_ligand[None, :]))**2
    rsq_min_lig_solv = (0.5*(sigma_solv[:, None] + sigma_ligand[None, :]))**2

    def collision(p_other, rsq_min, p_ligand):
        for zoffset in [-receptor_box[2], 0, receptor_box[2]]:
            offset = np.array([0., 0, zoffset])
            rsq = np.sum((p_other[:, None, :] + offset - p_ligand[None, :, :])**2, axis=-1)
            diff = (rsq - rsq_min).flatten()
            i = np.argmin(diff)
            if np.any(diff < 0):
                return True
        return False

    g = np.random.default_rng(seed)

    num_monomers = len(receptor.residues)
    Nmonomer = len(receptor.residues[0].atoms)
    print('Nmonomer', Nmonomer)

    for Ntries in range(1, max_tries+1):
        print(f'Attempt {Ntries}')
        monomer = g.integers(0, num_monomers)

        i_receptor = i_reference + monomer * Nmonomer
        pref_rec = p_rec[i_receptor]
        rref_rec = r_receptor[i_receptor]

        R = Rotation.random(random_state=g)

        sphere_point = g.standard_normal(3)
        sphere_point /= np.linalg.norm(sphere_point)

        d = g.uniform(rref_rec + rref_hydrogen, restraint_radius)

        p = R.apply(p_lig)

        p += pref_rec - p[ihydrogen] + sphere_point * d

        if not collision(p_rec, rsq_min_lig_rec, p):
            ligand.coordinates = p
            break
    else:
        raise RuntimeError(f'Could not insert molecule after {max_tries} attempts')

    new_solv_indices = []
    p_solv = solv.coordinates

    for res in solv.residues:
        indices = [a.idx for a in res.atoms]
        p_single_solv = p_solv[indices, :]
        if not collision(p_single_solv, rsq_min_lig_solv, ligand.coordinates):
            new_solv_indices += indices

    box = receptor + solv[new_solv_indices] + ligand

    box.box = receptor_box

    box.save(args.output)


if __name__ == '__main__':
    main()
