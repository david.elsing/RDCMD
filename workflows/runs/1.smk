import sys
from pathlib import Path

def resolve_path(relpath):
    import inspect

    frame = inspect.currentframe().f_back
    calling_file = frame.f_code.co_filename

    return (Path(calling_file).parent / relpath).as_posix()


sys.path.insert(0, resolve_path("../rules"))
import common

module MDanalysis:
    snakefile:
        "../rules/MDanalysis.smk"
    config: config

use rule * from MDanalysis

module cylinder:
    snakefile:
        "../rules/cylinder.smk"
    config: config

use rule * from cylinder

ams = ['PBLG', '2PBLG']

def molnames(am):
    return ['IPC', 'quinuclidinol', 'borneol', 'camphor']

all_molnames = sorted(set(sum((molnames(am) for am in ams), [])))

def lnums(am, molname):
    if am == 'PBLG':
        return [1, 8]
    elif am == '2PBLG':
        return [8]

_hbond_molnames = set(['IPC', 'quinuclidinol', 'borneol'])
def hbond_molnames(am):
    return [n for n in molnames(am) if n in _hbond_molnames]
def nohbond_molnames(am):
    return [n for n in molnames(am) if n not in _hbond_molnames]

def enantiomers(molname):
    if molname == 'IPC':
        return ['m', 'p']
    elif molname in ['borneol', 'camphor']:
        return ['Sm', 'Rp']
    elif molname == 'quinuclidinol':
        return ['Rm', 'Sp']

first_index = 1
def last_index(w):
    if w.lnum == '1':
        return 1000
    elif w.lnum == '8':
        return 150

def num_stages(w):
    return 10

cylinder_radius = '10.0'

localrules: plot_data_hbonds_distances_angles
rule plot_data_hbonds_distances_angles:
    input:
        lambda w: expand("results/MD/free/{am}/{lnum}/analysis/{enantiomer}-{molname}/chains/{index}/{num_stages}/hbonds.hdf5",
                         am=w.am,
                         lnum=w.lnum,
                         enantiomer=w.enantiomer,
                         molname=w.molname,
                         index=range(first_index, last_index(w)+1),
                         num_stages=num_stages(w)),
        hbonds_receptor="data/molecules/PBLG/hbond_analysis.json"
    output:
        "results/runs/1/{am}/{lnum}/{enantiomer}-{molname}/hbonds_distances_angles.hdf5"
    wildcard_constraints:
        am=common.re_am,
        lnum=common.re_lnum,
        enantiomer=common.re_enantiomer,
        molname=common.re_molname
    params:
        script=workflow.source_path("scripts/1/plot_data_hbonds_distances_angles.py"),
        path="results/MD/free/{am}/{lnum}/analysis/{enantiomer}-{molname}/chains",
        first_index=first_index,
        last_index=last_index,
        num_stages=num_stages,
        shell="""
        python3 {params.script:q} {params.path:q} {input.hbonds_receptor:q} \
            {params.first_index:q} {params.last_index:q} {params.num_stages:q} \
            {output[0]:q} --Nmol {wildcards.lnum:q}
        """,
        guix="analysis"
    script:
        "../run_smk_rule.py"

rule all_plot_data_hbonds_distances_angles:
    input:
        [f"results/runs/1/{am}/{lnum}/{enantiomer}-{molname}/hbonds_distances_angles.hdf5"
         for am in ams
         for molname in hbond_molnames(am)
         for lnum in lnums(am, molname)
         for enantiomer in enantiomers(molname)]

localrules: plot_data_hbonds
rule plot_data_hbonds:
    input:
        lambda w: expand("results/MD/free/{am}/{lnum}/analysis/{enantiomer}-{molname}/chains/{index}/{num_stages}/hbonds.hdf5",
                         am=w.am,
                         lnum=w.lnum,
                         enantiomer=w.enantiomer,
                         molname=w.molname,
                         index=range(first_index, last_index(w)+1),
                         num_stages=num_stages(w)),
        hbonds_receptor="data/molecules/PBLG/hbond_analysis.json"
    output:
        "results/runs/1/{am}/{lnum}/{enantiomer}-{molname}/hbonds.json"
    wildcard_constraints:
        enantiomer="[RSmp]+",
        molname="[\w-]+",
    params:
        script=workflow.source_path("scripts/1/plot_data_hbonds.py"),
        path="results/MD/free/{am}/{lnum}/analysis/{enantiomer}-{molname}/chains",
        first_index=first_index,
        last_index=last_index,
        num_stages=num_stages,
        max_HA_distance=3.0,
        min_angle=150,
        shell="""
            python3 {params.script:q} {params.path:q} {input.hbonds_receptor:q} \
                {params.max_HA_distance:q} {params.min_angle:q} \
                {params.first_index:q} {params.last_index:q} {params.num_stages:q} {output[0]:q} --Nmol {wildcards.lnum:q}
        """,
        guix="analysis"
    script:
        "../run_smk_rule.py"

rule all_plot_data_hbonds:
    input:
        [f"results/runs/1/{am}/{lnum}/{enantiomer}-{molname}/hbonds.json"
         for am in ams
         for molname in hbond_molnames(am)
         for lnum in lnums(am, molname)
         for enantiomer in enantiomers(molname)]

localrules: plot_data_distances
rule plot_data_distances:
    input:
        lambda w: expand("results/MD/free/{am}/{lnum}/analysis/{enantiomer}-{molname}/chains/{index}/{num_stages}/distances.hdf5",
                         am=w.am,
                         lnum=w.lnum,
                         enantiomer=w.enantiomer,
                         molname=w.molname,
                         index=range(first_index, last_index(w)+1),
                         num_stages=num_stages(w)),
        groups="data/molecules/PBLG/distance_analysis.json"
    output:
        "results/runs/1/{am}/{lnum}/{enantiomer}-{molname}/distances.hdf5"
    wildcard_constraints:
        am="PBLG",
        lnum="1",
        enantiomer="[RSmp]+",
        molname="[\w-]+",
    params:
        script=workflow.source_path("scripts/1/plot_data_distances.py"),
        path="results/MD/free/{am}/{lnum}/analysis/{enantiomer}-{molname}/chains",
        first_index=first_index,
        last_index=last_index,
        num_stages=num_stages,
        shell="""
        python3 {params.script:q} {params.path:q} {input.groups:q} \
            {params.first_index:q} {params.last_index:q} {params.num_stages:q} {output[0]:q}
        """,
        guix="analysis"
    script:
        "../run_smk_rule.py"

rule all_plot_data_distances:
    input:
        [f"results/runs/1/{am}/{lnum}/{enantiomer}-{molname}/distances.hdf5"
         for am in ['PBLG']
         for molname in molnames(am)
         for lnum in [1]
         for enantiomer in enantiomers(molname)]

localrules: copy_average
rule copy_average:
    input:
        lambda w: f"results/MD/free/{w.am}/{w.lnum}/analysis/{{enantiomer}}-{{molname}}/average/{first_index}_{last_index(w)}_{num_stages(w)}_{{mode}}.hdf5"
    output:
        "results/runs/1/{am}/{lnum}/{enantiomer}-{molname}/average/{mode}.hdf5"
    wildcard_constraints:
        am=common.re_am,
        lnum=common.re_lnum,
        enantiomer="[RSmp]+",
        molname="[\w-]+",
        mode="[\w-]+"
    shell:
        """
        cp {input[0]:q} {output[0]:q}
        """

localrules: all_copy_average
rule all_copy_average:
    input:
        [f"results/runs/1/{am}/{lnum}/{enantiomer}-{molname}/average/{mode}.hdf5"
         for am in ams
         for molname in molnames(am)
         for lnum in lnums(am, molname)
         for enantiomer in enantiomers(molname)
         for mode in ['A']]

localrules: plot_data_hbonds_contact_averages
rule plot_data_hbonds_contact_averages:
    input:
        lambda w: [f"results/MD/free/{w.am}/{w.lnum}/analysis/{w.enantiomer}-{w.molname}/chains/{index}/{num_stages(w)}/{filename}.hdf5"
                   for index in range(first_index, last_index(w)+1)
                   for filename in [f'RDCs_{w.mode}', 'hbonds']],
        hbonds_receptor="data/molecules/PBLG/hbond_analysis.json"
    output:
        "results/runs/1/{am}/{lnum}/{enantiomer}-{molname}/hbonds_contact_averages/{mode}.hdf5"
    wildcard_constraints:
        am=common.re_am,
        lnum=common.re_lnum,
        enantiomer="[RSmp]+",
        molname="[\w-]+",
        mode="[\w-]+"
    params:
        script=workflow.source_path("scripts/1/plot_data_hbonds_contact_averages.py"),
        path="results/MD/free/{am}/{lnum}/analysis/{enantiomer}-{molname}/chains",
        first_index=first_index,
        last_index=last_index,
        num_stages=num_stages,
        max_HA_distance=3.0,
        min_angle=150,
        shell="""
        python3 {params.script:q} {params.path:q} {wildcards.mode:q} \
            {input.hbonds_receptor:q} {params.max_HA_distance:q} {params.min_angle:q} \
            {params.first_index:q} {params.last_index:q} {params.num_stages:q} {output[0]:q} \
            --Nmol {wildcards.lnum:q}
        """,
        guix="analysis"
    script:
        "../run_smk_rule.py"

rule all_plot_data_hbonds_contact_averages:
    input:
        [f"results/runs/1/{am}/{lnum}/{enantiomer}-{molname}/hbonds_contact_averages/{mode}.hdf5"
         for am in ams
         for molname in hbond_molnames(am)
         for lnum in lnums(am, molname)
         for enantiomer in enantiomers(molname)
         for mode in ['full', 'A']]

localrules: plot_data_distances_contact_averages
rule plot_data_distances_contact_averages:
    input:
        lambda w: [f"results/MD/free/{w.am}/{w.lnum}/analysis/{w.enantiomer}-{w.molname}/chains/{index}/{num_stages(w)}/{filename}.hdf5"
                   for index in range(first_index, last_index(w)+1)
                   for filename in [f'RDCs_{w.mode}', 'distances']],
        groups="data/molecules/PBLG/distance_analysis.json"
    output:
        "results/runs/1/{am}/{lnum}/{enantiomer}-{molname}/distances_contact_averages/{mode}.hdf5"
    wildcard_constraints:
        am="PBLG",
        lnum="1",
        enantiomer="[RSmp]+",
        molname="[\w-]+",
        mode="[\w-]+"
    params:
        script=workflow.source_path("scripts/1/plot_data_distances_contact_averages.py"),
        path="results/MD/free/{am}/{lnum}/analysis/{enantiomer}-{molname}/chains",
        first_index=first_index,
        last_index=last_index,
        num_stages=num_stages,
        max_LJ_distance=1.5,
        shell="""
        python3 {params.script:q} {params.path:q} {wildcards.mode:q} \
            {input.groups:q} {params.max_LJ_distance:q} \
            {params.first_index:q} {params.last_index:q} {params.num_stages:q} {output[0]:q}
        """,
        guix="analysis"
    script:
        "../run_smk_rule.py"

rule all_plot_data_distances_contact_averages:
    input:
        [f"results/runs/1/{am}/{lnum}/{enantiomer}-{molname}/distances_contact_averages/{mode}.hdf5"
         for am in ['PBLG']
         for molname in molnames(am)
         for lnum in [1]
         for enantiomer in enantiomers(molname)
         for mode in ['full', 'A']]

def l_num_indices(w):
    molname = w.molname
    if w.am == 'PBLG' and w.lnum == '1':
        return list(range(100, 1000+1, 100))
    else:
        return list(range(25, 150+1, 25))

localrules: plot_data_relative_errors
rule plot_data_relative_errors:
    input:
        values=lambda w: [f"results/MD/free/{w.am}/{w.lnum}/analysis/{{enantiomer}}-{{molname}}/average/{last_index(w) - n + 1}_{last_index(w)}_{num_stages(w)}_{{mode}}.hdf5"
                          for n in l_num_indices(w)],
        ref_values=lambda w: f"results/MD/free/{w.am}/{w.lnum}/analysis/{{enantiomer}}-{{molname}}/average/{first_index}_{last_index(w)}_{num_stages(w)}_{{mode}}.hdf5"
    output:
        "results/runs/1/{am}/{lnum}/{enantiomer}-{molname}/relative_errors/{mode}.json"
    wildcard_constraints:
        am=common.re_am,
        lnum=common.re_lnum,
        enantiomer=common.re_enantiomer,
        molname=common.re_molname,
        mode="[\w-]+"
    params:
        script=workflow.source_path("scripts/1/plot_data_relative_errors.py"),
        l_num_indices=l_num_indices,
        shell="""
        python3 {params.script:q} {input.ref_values:q} {output[0]:q} \
            --values {input.values:q} --l_num_indices {params.l_num_indices:q}
        """,
        guix="analysis"
    script:
        "../run_smk_rule.py"

rule all_plot_data_relative_errors:
    input:
        [f"results/runs/1/{am}/{lnum}/{enantiomer}-{molname}/relative_errors/full.json"
         for am in ams
         for molname in molnames(am)
         for lnum in lnums(am, molname)
         for enantiomer in enantiomers(molname)]

localrules: plot_data_com_distances
rule plot_data_com_distances:
    input:
        lambda w: expand("results/MD/free/{am}/{lnum}/analysis/{enantiomer}-{molname}/chains/{index}/{num_stages}/com_distances.hdf5",
                         am=w.am,
                         lnum=w.lnum,
                         enantiomer=w.enantiomer,
                         molname=w.molname,
                         index=range(first_index, last_index(w)+1),
                         num_stages=num_stages(w))
    output:
        "results/runs/1/{am}/{lnum}/{enantiomer}-{molname}/com_distances.hdf5"
    wildcard_constraints:
        am=common.re_am,
        lnum=common.re_lnum,
        enantiomer=common.re_enantiomer,
        molname=common.re_molname,
    params:
        script=workflow.source_path("scripts/1/plot_data_com_distances.py"),
        path="results/MD/free/{am}/{lnum}/analysis/{enantiomer}-{molname}/chains",
        first_index=first_index,
        last_index=lambda w: last_index(w),
        num_stages=num_stages,
        shell="""
        python3 {params.script:q} {params.path:q} \
            {params.first_index:q} {params.last_index:q} {params.num_stages:q} {output[0]:q}
        """,
        guix="analysis"
    script:
        "../run_smk_rule.py"

rule all_plot_data_com_distances:
    input:
        [f"results/runs/1/{am}/{lnum}/{enantiomer}-{molname}/com_distances.hdf5"
         for am in ams
         for molname in molnames(am)
         for lnum in lnums(am, molname)
         for enantiomer in enantiomers(molname)]

localrules: plot_data_com_distances_averages
rule plot_data_com_distances_averages:
    input:
        lambda w: [f"results/MD/free/{w.am}/{w.lnum}/analysis/{w.enantiomer}-{w.molname}/chains/{index}/{num_stages(w)}/{filename}.hdf5"
                   for index in range(first_index, last_index(w)+1)
                   for filename in [f'RDCs_{w.mode}', 'com_distances']]
    output:
        "results/runs/1/{am}/{lnum}/{enantiomer}-{molname}/com_distances_averages/{mode}.hdf5"
    wildcard_constraints:
        am=common.re_am,
        lnum=common.re_lnum,
        enantiomer=common.re_enantiomer,
        molname=common.re_molname,
        mode="[\w-]+"
    params:
        script=workflow.source_path("scripts/1/plot_data_com_distances_averages.py"),
        path="results/MD/free/{am}/{lnum}/analysis/{enantiomer}-{molname}/chains",
        first_index=first_index,
        last_index=last_index,
        num_stages=num_stages,
        shell="""
        python3 {params.script:q} {params.path:q} {wildcards.mode:q} \
            {params.first_index:q} {params.last_index:q} {params.num_stages:q} {output[0]:q}
        """,
        guix="analysis"
    script:
        "../run_smk_rule.py"

rule all_plot_data_com_distances_averages:
    input:
        [f"results/runs/1/{am}/{lnum}/{enantiomer}-{molname}/com_distances_averages/full.hdf5"
         for am in ams
         for molname in molnames(am)
         for lnum in lnums(am, molname)
         for enantiomer in enantiomers(molname)]

localrules: plot_data_D_average
rule plot_data_D_average:
    input:
        average=lambda w: f"results/MD/free/{w.am}/{w.lnum}/analysis/{w.enantiomer}-{w.molname}/average/{first_index}_{last_index(w)}_{num_stages(w)}_{{mode}}.hdf5",
        couplings_spec="data/couplings/{molname}.json",
        ligand="results/molecules/small/chosen/{enantiomer}-{molname}.gro",
        top_ligand=common.small_ligand_top_input
    output:
        "results/runs/1/{am}/{lnum}/{enantiomer}-{molname}/D_average/{mode}.hdf5"
    wildcard_constraints:
        am=common.re_am,
        lnum=common.re_lnum,
        enantiomer=common.re_enantiomer,
        molname=common.re_molname,
        mode="[\w-]+"
    params:
        script=workflow.source_path("scripts/1/plot_data_D_average.py"),
        shell="""
        python3 {params.script:q} {input.average:q} {input.couplings_spec:q} {input.ligand:q} \
            {input.top_ligand:q} {output[0]:q}
        """,
        guix="analysis"
    script:
        "../run_smk_rule.py"

rule all_plot_data_D_average:
    input:
        [f"results/runs/1/{am}/{lnum}/{enantiomer}-{molname}/D_average/{mode}.hdf5"
         for am in ams
         for molname in molnames(am)
         for lnum in lnums(am, molname)
         for enantiomer in enantiomers(molname)
         for mode in ['full', 'angles', 'distances', 'A']]

localrules: plot_data_A_cylinder
rule plot_data_A_cylinder:
    input:
        f"results/cylinder/{{enantiomer}}-{{molname}}/A_{cylinder_radius}.txt"
    output:
        "results/runs/1/cylinder/{enantiomer}-{molname}/A.txt"
    wildcard_constraints:
        enantiomer=common.re_enantiomer,
        molname=common.re_molname
    shell:
        """
        cp {input[0]:q} {output[0]:q}
        """

rule all_plot_data_A_cylinder:
    input:
        [f"results/runs/1/cylinder/{enantiomer}-{molname}/A.txt"
         for molname in all_molnames
         for enantiomer in enantiomers(molname)]

localrules: plot_data_D_cylinder
rule plot_data_D_cylinder:
    input:
        A="results/runs/1/cylinder/{enantiomer}-{molname}/A.txt",
        couplings_spec="data/couplings/{molname}.json",
        ligand="results/molecules/small/chosen/{enantiomer}-{molname}.gro",
        top_ligand=common.small_ligand_top_input
    output:
        "results/runs/1/cylinder/{enantiomer}-{molname}/D.txt"
    wildcard_constraints:
        enantiomer=common.re_enantiomer,
        molname=common.re_molname
    params:
        script=workflow.source_path("scripts/1/plot_data_D_cylinder.py"),
        shell="""
        python3 {params.script:q} {input.A:q} {input.couplings_spec:q} {input.ligand:q} \
            {input.top_ligand:q} {output[0]:q}
        """,
        guix="analysis"
    script:
        "../run_smk_rule.py"

rule all_plot_data_D_cylinder:
    input:
        [f"results/runs/1/cylinder/{enantiomer}-{molname}/D.txt"
         for molname in all_molnames
         for enantiomer in enantiomers(molname)]

localrules: copy_opt_molecule
rule copy_opt_molecule:
    input:
        "results/molecules/small/chosen_opt/{enantiomer}-{molname}.gro"
    output:
        "results/runs/1/opt_molecules/{enantiomer}-{molname}.gro"
    wildcard_constraints:
        enantiomer=common.re_enantiomer,
        molname=common.re_molname
    shell:
        """
        cp {input[0]:q} {output[0]:q}
        """

rule all_copy_opt_molecule:
    input:
        [f"results/runs/1/opt_molecules/{enantiomer}-{molname}.gro"
         for molname in all_molnames
         for enantiomer in enantiomers(molname)]

rule all:
    input:
        rules.all_plot_data_hbonds_distances_angles.input,
        rules.all_plot_data_hbonds.input,
        rules.all_plot_data_distances.input,
        rules.all_copy_average.input,
        rules.all_plot_data_hbonds_contact_averages.input,
        rules.all_plot_data_distances_contact_averages.input,
        rules.all_plot_data_relative_errors.input,
        rules.all_plot_data_com_distances.input,
        rules.all_plot_data_com_distances_averages.input,
        rules.all_plot_data_D_average.input,
        rules.all_plot_data_A_cylinder.input,
        rules.all_plot_data_D_cylinder.input,
        rules.all_copy_opt_molecule.input
