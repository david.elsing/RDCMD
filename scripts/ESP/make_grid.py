"""Script to calculate grid for RESP fit."""

import numpy as np

import argparse
import json

def read_xyz_file(filename):
    positions = []
    elements = []
    with open(filename) as file:
        for i, line in enumerate(file):
            parts = line.split()
            if i == 0:
                N = int(line)
            elif i == 1:
                pass
            elif i - 2 < N:
                positions.append([float(v) for v in parts[1:4]])
                el = parts[0].upper()
                if len(el) == 2:
                    el = el[0].upper() + el[1].lower()
                elements.append(el)
            else:
                break

    return positions, elements

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('xyz_file')
    parser.add_argument('bondi_radii')
    parser.add_argument('density')
    parser.add_argument('output_file')
    parser.add_argument('--scale_radii', nargs='+', default=[1.4, 1.6, 1.8, 2.0])
    args = parser.parse_args()

    density = float(args.density)
    scale_radii = [float(r) for r in args.scale_radii]

    with open(args.bondi_radii) as file:
        bondi_radii = json.load(file)

    positions = []
    radii = []
    positions, elements = read_xyz_file(args.xyz_file)
    radii = [bondi_radii[e] for e in elements]

    positions = np.array(positions)
    radii = np.array(radii)


    def sphere_positions(N_sphere):
        res = []
        N = int(np.sqrt(np.pi*N_sphere))
        for theta in np.linspace(0, np.pi, N//2 + 1):
            N2 = int(N*np.sin(theta))
            if N2 < 1:
                N2 = 1
            for phi in np.linspace(0, 2*np.pi, N2 + 1)[:-1]:
                res.append((np.sin(theta) * np.cos(phi),
                            np.sin(theta) * np.sin(phi),
                            np.cos(theta)))

        return np.array(res)


    final_pos = []
    for i, (pos, r) in enumerate(zip(positions, radii)):
        for t in scale_radii:
            N_sphere = int(4 * np.pi * (t*r)**2 * density)
            for sp in sphere_positions(N_sphere):
                newpos = t*r*sp + pos
                collision = False
                for j, (r2, pos2) in enumerate(zip(radii, positions)):
                    if i == j:
                        continue
                    if np.sum((pos2 - newpos)**2) < (t*r2)**2:
                        collision = True
                        break
                if not collision:
                    final_pos.append(newpos)

    np.savetxt(args.output_file, final_pos, fmt='%.4e')


if __name__ == '__main__':
    main()
