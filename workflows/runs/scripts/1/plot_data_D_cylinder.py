import RDC_tools
from RDC_tools.file_io import parse_coupling_spec

import parmed

import numpy as np

import argparse
import json

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('A')
    parser.add_argument('coupling_spec')
    parser.add_argument('ligand')
    parser.add_argument('top_ligand')
    parser.add_argument('output_file')
    args = parser.parse_args()

    with open(args.coupling_spec) as file:
        coupling_spec = json.load(file)
        coupling_spec = parse_coupling_spec(coupling_spec)

    top_ligand = parmed.load_file(args.top_ligand)
    masses = np.array([a.mass for a in top_ligand.atoms])

    ligand = parmed.load_file(args.ligand)

    pos = ligand.coordinates
    com = np.sum(pos * masses[:,None]) / np.sum(masses)
    pos = pos - com

    prefactors = RDC_tools.RDCs.calc_prefactors(coupling_spec)
    dist = RDC_tools.RDCs.calc_distances(coupling_spec, pos)
    dirs = RDC_tools.RDCs.calc_directions(coupling_spec, pos)
    M = RDC_tools.alignment_tensor.angle_parameter_matrix(prefactors, dirs, dist)

    A_matrix = np.loadtxt(args.A)
    A = RDC_tools.alignment_tensor.from_matrix(A_matrix)

    D = M.dot(A)

    np.savetxt(args.output_file, D)

if __name__ == '__main__':
    main()
