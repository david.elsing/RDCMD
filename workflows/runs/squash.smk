rule MD_free_squash:
    output:
        "results/MD/free/{enantiomer}-{molname}.squash"
    wildcard_constraints:
        enantiomer="[RSmp]+",
        molname="[\w-]+"
    group:
        lambda w: f"freeMDsquash"
    params:
        shell="""
            gensquashfs {output[0]:q} -c zstd -D results/MD/free/{wildcards.enantiomer:q}-{wildcards.molname:q} -j {resources.threads}
            """,
        guix="squashfs"
    script:
        "../run_smk_rule.py"

localrules: MD_small_squash
rule MD_small_squash:
    output:
        "results/MD/{name}.squash"
    wildcard_constraints:
        name="\w+"
    params:
        shell="""
            gensquashfs {output[0]:q} -c zstd -D results/MD/{wildcards.name:q}
            """,
        guix="squashfs"
    script:
        "../run_smk_rule.py"

localrules: other_results_squash
rule other_results_squash:
    output:
        "results/{name}.squash"
    wildcard_constraints:
        name="\w+"
    params:
        shell="""
            gensquashfs {output[0]:q} -c zstd -D results/{wildcards.name:q}
            """,
        guix="squashfs"
    script:
        "../run_smk_rule.py"



results = []

results += expand("results/MD/free/{mol}.squash",
                  mol=['m-IPC', 'p-IPC',
                       'Rm-quinuclidinol', 'Sp-quinuclidinol'
                       'Sm-borneol', 'Rp-borneol',
                       'Sm-camphor', 'Rp-camphor'])

results += expand("results/MD/{name}.squash",
                  name=['PBLG', 'TCM'])

results += expand("results/{name}.squash",
                  name=['FF', 'molecules'])

rule all:
    input:
        results
