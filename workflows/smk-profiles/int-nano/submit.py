from snakemake.utils import read_job_properties

import subprocess
from pathlib import Path
import sys
import shlex
import tempfile
import re
import os

jobscript = sys.argv[-1]
properties = read_job_properties(jobscript)

resources = properties['resources']
if 'rule' in properties:
    rule = properties['rule']
else:
    rule = 'GROUP'

time = shlex.split(resources['time'])
if len(time) != 1:
    raise RuntimeError(f'Invalid time: {resources["time"]}')
time = time[0]

Path(f'slurm-logs/{rule}').mkdir(parents=True, exist_ok=True)

cmd = ['sbatch',
       f'--parsable',
       f'--partition={resources["partition"]}',
       f'--qos={resources["qos"]}',
       f'--ntasks-per-node={resources["threads"]}',
       f'--mem={resources["mem_mb"]}M',
       f'--job-name={rule}',
       f'--output=slurm-logs/{rule}/%x-%j.log',
       f'--time={time}'
]

env = os.environ.copy()

if 'GUIX_UNS' in env:
    del env['GUIX_UNS']

if 'JOBSHELL' in env:
    shell = Path(env['JOBSHELL']).resolve().as_posix()
else:
    shell = '/bin/sh'

if 'SLURM_UID' in env:
    cmd += ['--uid', env['SLURM_UID']]

if 'SLURM_GID' in env:
    cmd += ['--gid', env['SLURM_GID']]

with tempfile.TemporaryDirectory() as tmpdir:
    tmpdir = Path(tmpdir)
    tmpfile = tmpdir / 'jobscript'
    with open(tmpfile, 'w') as file:
        with open(jobscript) as oldfile:
            file.write(oldfile.read().replace('$SHELL', shell))
    tmpfile.chmod(tmpfile.stat().st_mode | 0o111)
    cmd.append(tmpfile.as_posix())
    res = subprocess.run(cmd, check=True, capture_output=True, universal_newlines=True, env=env)

try:
    res.check_returncode()
except subprocess.CalledProcessError as exc:
    raise RuntimeError(f'sbatch failed with stdout:\n{res.stdout}\nstderr:\n{res.stderr}')

jobid = int(res.stdout.split(';')[0])
print(jobid)
