import h5py
import numpy as np

import argparse
from pathlib import Path
import json

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('path')
    parser.add_argument('groups')
    parser.add_argument('first_index')
    parser.add_argument('last_index')
    parser.add_argument('num_stages')
    parser.add_argument('output')
    args = parser.parse_args()

    path = Path(args.path)

    with open(args.groups) as file:
        groups_spec = json.load(file)

    first_index = int(args.first_index)
    last_index = int(args.last_index)
    num_stages = int(args.num_stages)

    results = np.zeros((len(groups_spec), 4), dtype='int64')

    Nframes = 0

    for index in range(first_index, last_index+1):
        filename = path / f"{index}/{num_stages}/distances.hdf5"

        with h5py.File(filename) as file:
            Nframes += file.attrs['Nframes']

            for i, gs in enumerate(groups_spec):
                group = file[f'group{i}']
                indices = np.array(group['monomer_indices'])
                mask = indices != -1
                distance = np.array(group['distance'])
                if gs['type'] == 'nearest':
                    sigma = np.array(group['sigma'])
                    for k in range(4):
                        results[i,k] += np.sum(distance[mask[:,k],k] <= 1.5*sigma[mask[:,k],k])
                else:
                    max_distance = gs['max_distance']
                    for k in range(4):
                        results[i,k] += np.sum(distance[mask[:,k],k] <= max_distance)

    with h5py.File(args.output, 'w') as file:
        file.attrs['Nframes'] = Nframes
        for i, gs in enumerate(groups_spec):
            file.create_dataset(f'group{i}', data=results[i])

if __name__ == '__main__':
    main()
