import RDC_tools
from RDC_tools import file_io, alignment_tensor, RDCs
import numpy as np

import argparse
import json
import itertools
import copy

parser = argparse.ArgumentParser()
parser.add_argument('averaged_couplings')
parser.add_argument('xyz')
parser.add_argument('averaged_RDCs')
parser.add_argument('molname')
parser.add_argument('enantiomer')
parser.add_argument('mode', choices=['full', 'step'])
parser.add_argument('svd_weighted', choices=['0', '1'])
parser.add_argument('quality_weighted', choices=['0', '1'])
parser.add_argument('output_couplings')
parser.add_argument('output_RDCs')
parser.add_argument('output_result')
parser.add_argument('--save_av', default=None)
parser.add_argument('--perm_RDCs', action='store_true')
parser.add_argument('--exclude', nargs='+', default=[])
parser.add_argument('--first_perms', default=None)
args = parser.parse_args()

if args.save_av is not None and args.perm_RDCs:
    raise RuntimeError('save_av is incompatible with perm_RDCs')

exclude = set(int(s) for s in args.exclude)

def parse_coupling(cs):
    ctype = cs['type']
    coupling_average = {
        'orientations': [],
        'scalings': [],
        'pairs': [],
        'pair_isotopes': []
    }
    if ctype == 'normal':
        file_io._parse_normal(cs, coupling_average)
    elif ctype == 'projected':
        file_io._parse_projected(cs, coupling_average)
    else:
        raise RuntimeError(f'Invalid coupling type "{ctype}"')

    return coupling_average

def split_spec_data(cs):
    res = []
    for cs2 in cs['couplings']:
        cs2 = copy.deepcopy(cs2)
        res.append(cs2)
    return res

def split_RDCs(D):
    return [np.array([v]) for v in D]

def apply_perm(perm, l):
    return [l[i] for i in perm]

def apply_perm_coupl(perm, l):
    res = []
    pairs = []
    orientations = []
    for c in l:
        if 'pair' in c:
            orientation = False
            pairs.append(c['pair'])
        else:
            orientation = True
            pairs.append(c['pairs'])
            orientations.append(c['orientation'])

    for i, c in enumerate(l):
        c2 = copy.deepcopy(c)
        if orientation:
            c2['pairs'] = pairs[perm[i]]
            c2['orientation'] = orientations[perm[i]]
        else:
            c2['pair'] = pairs[perm[i]]
        res.append(c2)
    return res

with open(args.xyz) as file:
    mols = file_io.read_xyz_file(file)

with open(args.averaged_RDCs) as file:
    _, _, D, Derr = file_io.read_RDC_file(file)


def fit(couplings, D, Derr, weight_svd=False, weight_res=False):
    M = 0
    for atoms in mols:
        prefactors = RDCs.calc_prefactors(couplings)
        directions = RDCs.calc_directions(couplings, [a['pos'] for a in atoms])
        distances = RDCs.calc_distances(couplings, [a['pos'] for a in atoms])
        M += alignment_tensor.angle_parameter_matrix(prefactors, directions, distances)
    M /= len(mols)

    mean = np.array([np.mean(v) for v in D])
    meanerr = np.array([np.mean(v) for v in Derr])
    if not weight_svd:
        A, _ = alignment_tensor.fit_alignment_tensor_averages(M, mean)
    else:
        A, _, _ = alignment_tensor.fit_alignment_tensor_averages(M, mean, meanerr)
    Dcalc = alignment_tensor.calc_RDCs(M, A)

    rmsd_D = np.sqrt(np.mean(mean**2))
    if not weight_res:
        rmsd = np.sqrt(np.mean((mean - Dcalc)**2))
        cq = rmsd / rmsd_D
    else:
        w = 1 / meanerr
        rmsd = np.sqrt(np.sum(w * (mean - Dcalc)**2) / np.sum(w))
        cq = rmsd / rmsd_D
    return A, rmsd, cq

with open(args.averaged_couplings) as file:
    spec = json.load(file)

data = spec['data']
av_couplings = file_io.parse_coupling_spec(spec)

splits = []
indices = []
orig_indices = {}
iactual = -1
for i, c in enumerate(av_couplings):
    if 'onlyenantiomer' in data[i] and args.enantiomer != data[i]['onlyenantiomer']:
        continue

    iactual += 1

    if (data[i]['type'] != 'average') or i+1 in exclude:
        continue

    cs_split = split_spec_data(data[i])
    D_split = split_RDCs(D[iactual])
    Derr_split = split_RDCs(Derr[iactual])

    orig_indices[i] = len(indices)
    indices.append(i)
    splits.append((cs_split, D_split, Derr_split))

perms = []

if args.mode == 'split':
    for i, (cs_split, D_split, Derr_split) in enumerate(splits):
        c_split = [parse_coupling(cs) for cs in cs_split]
        Dtest = D[:i] + D_split + D[i+1:]
        Derrtest = Derr[:i] + Derr_split + Derr[i+1:]

        min_rmsd = np.inf
        cs_av = data[indices[i]]
        if 'perms' in cs_av:
            perms = [[i-1 for i in p] for p in cs_av['perms']]
        else:
            perms = itertools.permutations(range(len(c_split)))
        for perm in perms:
            test_couplings = av_couplings[:i] + apply_perm(perm, c_split) + av_couplings[i+1:]
            A, rmsd, _ = fit(test_couplings, Dtest, Derrtest,
                             args.svd_weighted == '1',
                             args.quality_weighted == '1')
            if rmsd < min_rmsd:
                min_rmsd = rmsd
                perm_chosen = perm

        perms.append(perm_chosen)
else:
    all_perms = []
    for i, split in enumerate(splits):
        cs_av = data[indices[i]]
        if 'perms' in cs_av:
            perms = [[i-1 for i in p] for p in cs_av['perms']]
        else:
            perms = itertools.permutations(range(len(split[0])))

        all_perms.append(perms)

    min_rmsd = np.inf
    for perms in itertools.product(*all_perms):
        Dtest = []
        Derrtest = []
        test_couplings = []
        iactual = -1
        for i, c in enumerate(av_couplings):
            if 'onlyenantiomer' in data[i] and args.enantiomer != data[i]['onlyenantiomer']:
                continue

            iactual += 1

            if i not in orig_indices:
                Dtest.append(D[iactual])
                Derrtest.append(Derr[iactual])
                test_couplings.append(c)
                continue

            index = orig_indices[i]
            perm = perms[index]
            cs_split, D_split, Derr_split = splits[index]
            c_split = [parse_coupling(cs) for cs in cs_split]
            Dtest += D_split
            Derrtest += Derr_split
            test_couplings += apply_perm(perm, c_split)

        A, rmsd, _ = fit(test_couplings, Dtest, Derrtest,
                         args.svd_weighted == '1',
                         args.quality_weighted == '1')
        if rmsd < min_rmsd:
            min_rmsd = rmsd
            perms_chosen = perms

    perms = perms_chosen

res_spec = copy.deepcopy(spec)
res_data = res_spec['data'] = []
res_av_data = []
res_D = []
res_Derr = []

iactual = -1
for i, c in enumerate(av_couplings):
    correct_enantiomer = 'onlyenantiomer' not in data[i] or args.enantiomer == data[i]['onlyenantiomer']
    if not correct_enantiomer:
        res_av_data.append(data[i])
        continue

    iactual += 1

    if i not in orig_indices or not correct_enantiomer:
        res_data.append(data[i])
        res_av_data.append(data[i])
        res_D.append(D[iactual])
        res_Derr.append(Derr[iactual])
        continue

    index = orig_indices[i]
    cs_split, D_split, Derr_split = splits[index]
    perm = perms[index]

    if not args.perm_RDCs:
        res_D += D_split
        res_Derr += Derr_split
        res_data += apply_perm_coupl(perm, cs_split)
        data_av = copy.deepcopy(data[i])
        data_av['couplings'] = apply_perm_coupl(perm, data_av['couplings'])
        res_av_data.append(data_av)
    else:
        invperm = np.argsort(perm)
        res_D += apply_perm(invperm, D_split)
        res_Derr += apply_perm(invperm, Derr_split)
        res_data += cs_split

with open(args.output_couplings, 'w') as file:
    json.dump(res_spec, file, indent=2)
    file.write('\n')

with open(args.output_RDCs, 'w') as file:
    file.write(f'{len(res_D)}\n')
    for v, verr in zip(res_D, res_Derr):
        if len(v) == 1:
            file.write(f'{v[0]:.3f} {verr[0]:.3f}\n')
        else:
            file.write(f'a {len(v)}')
            for v2, v2err in zip(v, verr):
                file.write(f' {v2:.3f} {v2err:.3f}')
            file.write('\n')

res_couplings = file_io.parse_coupling_spec(res_spec)
_, rmsd, cq = fit(res_couplings, res_D, res_Derr,
                  args.svd_weighted == '1',
                  args.quality_weighted == '1')

if args.first_perms is not None:
    with open(args.first_perms) as file:
        first_perms = json.load(file)['perms']

    fp_index = 0
    new_first_perms = []
    for i, d in enumerate(data):
        if d['type'] != 'average':
            continue

        if 'onlyenantiomer' not in d:
            new_first_perms.append(first_perms[fp_index])
            fp_index += 1
        elif args.enantiomer == d['onlyenantiomer']:
            new_first_perms.append(list(range(len(perms[i]))))
        else:
            fp_index += 1

    perms = [[first_perm[i] for i in perm]
             for (first_perm, perm) in zip(new_first_perms, perms)]

result = {
    'rmsd': rmsd,
    'cornilescu': cq,
    'perms': perms
}

with open(args.output_result, 'w') as file:
    json.dump(result, file, indent=2)
    file.write('\n')

if args.save_av is not None:
    res_spec['data'] = res_av_data
    with open(args.save_av, 'w') as file:
        json.dump(res_spec, file, indent=2)
        file.write('\n')
