import parmed

import numpy as np

import argparse
import json

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('input_file')
    parser.add_argument('numbers')
    parser.add_argument('output_file')
    args = parser.parse_args()

    mol = parmed.load_file(args.input_file, structure=True)

    with open(args.numbers) as file:
        numbers = json.load(file)

    Ncap1 = numbers['Ncap1']
    Nmonomer = numbers['Nmonomer']
    num_monomers = numbers['num_monomers']
    first_mainchain = numbers['first_mainchain']

    mainchain = []
    for i in range(num_monomers):
        for idx in first_mainchain:
            mainchain.append(i * Nmonomer + idx)

    s_mainchain = mol[mainchain]
    r = s_mainchain.coordinates

    m = np.array([a.mass for a in s_mainchain])

    M = np.sum(m)
    r_com = np.sum(r*m[:,None], axis=0) / M
    r = r - r_com[None,:]
    rsq = np.sum(r**2, axis=1)
    xixj = np.einsum('...i,...j->...ij', r, r)
    I = np.sum(m[:,None,None] * (rsq[:,None,None] * np.eye(3)[None,:,:] - xixj), axis=0) / M
    lam, Q = np.linalg.eigh(I)
    Q = Q[:,::-1]
    if np.linalg.det(Q) < 0:
        Q *= -1

    new_coords = np.einsum('ji,...j', Q, mol.coordinates)
    if new_coords[mainchain[0]][2] > new_coords[mainchain[-1]][2]:
        new_coords[:,2] *= -1
        new_coords[:,0] *= -1

    mol.coordinates = new_coords

    mol.save(args.output_file)

if __name__ == '__main__':
        main()
