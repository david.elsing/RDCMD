from rdkit import Chem
from rdkit.Chem import AllChem
import numpy as np

import argparse

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('SMILES_file')
    parser.add_argument('output_file')
    parser.add_argument('--N_confs', default=1)
    parser.add_argument('--N_threads', default=1)
    parser.add_argument('--seed', default=0)
    args = parser.parse_args()

    N_confs = int(args.N_confs)
    N_threads = int(args.N_threads)
    seed = int(args.seed)

    with open(args.SMILES_file) as file:
        smi = file.read().strip()
    mol = Chem.MolFromSmiles(smi)
    mol = Chem.AddHs(mol)

    cids = AllChem.EmbedMultipleConfs(mol,
            numConfs = N_confs,
            maxAttempts = 100 * N_confs,
            randomSeed = seed,
            clearConfs = True,
            useRandomCoords = False,
            boxSizeMult = 2.0,
            randNegEig = True,
            numZeroFail = 1,
            pruneRmsThresh = -1.0,
            coordMap = {},
            forceTol = 0.001,
            ignoreSmoothingFailures = False,
            enforceChirality = True,
            numThreads = N_threads,
            useExpTorsionAnglePrefs = False,
            useBasicKnowledge = True,
            printExpTorsionAngles = False,
            useSmallRingTorsions = False,
            useMacrocycleTorsions = False)

    while True:
        opt_results = AllChem.MMFFOptimizeMoleculeConfs(mol, numThreads = N_threads, maxIters = 10000)
        print(opt_results, flush=True)
        if all(res[0] == 0 for res in opt_results):
            break

    energies = np.array([res[1] for res in opt_results])

    min_index = int(np.argmin(energies))
    best_cid = cids[min_index]

    Chem.MolToMolFile(mol, args.output_file, confId=best_cid)

if __name__ == '__main__':
    main()
