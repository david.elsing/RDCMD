import parmed
import numpy as np

import argparse

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('prmtop')
    parser.add_argument('struct')
    parser.add_argument('charges')
    parser.add_argument('output')
    args = parser.parse_args()

    mol = parmed.load_file(args.prmtop)
    s = parmed.load_file(args.struct)
    charges = np.loadtxt(args.charges)

    mol.residues = parmed.ResidueList()
    for a, a2, c in zip(mol.atoms, s.atoms, charges):
        a.charge = c
        a.name = a2.name
        if a.atomic_number == 1:
            a.mass = 2.0141
        mol.residues.add_atom(a, 'TCM', 1)

    mol.save(args.output)

if __name__ == '__main__':
    main()
