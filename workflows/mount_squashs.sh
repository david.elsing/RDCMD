export GUIX_PROFILE="$(readlink -f workflows/guix/profiles/squashfs)"
source "$GUIX_PROFILE"/etc/profile

for file in; do
	dirname="${file%.squash}"
	mkdir -p "$dirname"
	if [[ "p$1" == "p-u" ]]; then
		fusermount3 -u "$dirname"
	else
		squashfuse "$file" "$dirname"
	fi
done
