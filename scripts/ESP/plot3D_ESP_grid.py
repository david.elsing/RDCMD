"""Script to show grid for RESP fit."""

import pyvista as pv
import numpy as np

import argparse
import json
from collections import defaultdict

element_colors = defaultdict(lambda: (0.8, 0.8, 0.8),
                             {'H': (1.0, 1.0, 1.0),
                              'C': (0.2, 0.2, 0.2),
                              'N': (0.0, 0.0, 0.8),
                              'O': (0.8, 0.0, 0.0),
                              'F': (0.024, 0.933, 0.749)})


def read_xyz_file(filename):
    positions = []
    elements = []
    with open(filename) as file:
        for i, line in enumerate(file):
            parts = line.split()
            if i == 0:
                N = int(line)
            elif i == 1:
                pass
            elif i - 2 < N:
                positions.append([float(v) for v in parts[1:4]])
                el = parts[0].upper()
                if len(el) == 2:
                    el = el[0].upper() + el[1].lower()
                elements.append(el)
            else:
                break

    return positions, elements

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('grid_file')
    parser.add_argument('--esp', default=None)
    parser.add_argument('--xyz_file', default=None)
    parser.add_argument('--bondi_radii', default=None)
    parser.add_argument('--output_file', default=None)

    args = parser.parse_args()

    if args.xyz_file is not None:
        if args.bondi_radii is None:
            raise RuntimeError('xyz_file requires bondi_radii')

        with open(args.bondi_radii) as file:
            bondi_radii = json.load(file)

    pos = np.loadtxt(args.grid_file)

    plotter = pv.Plotter()

    points = pv.PolyData(pos)
    if args.esp is None:
        plotter.add_mesh(points.glyph(scale=False, factor=0.2, geom=pv.Sphere()))
    else:
        pot = np.loadtxt(args.esp)
        #potmax = np.max(np.abs(pot))
        potmax = 0.045
        points['scalars'] = pot
        plotter.add_mesh(points.glyph(scale=False, factor=0.2, geom=pv.Sphere()), clim=[-potmax,potmax], cmap='bwr', scalar_bar_args={'title': 'Electrostatic potential'})

    if args.xyz_file is not None:
        pos, elements = read_xyz_file(args.xyz_file)
        for p, el in zip(pos, elements):
            r = bondi_radii[el]
            color = element_colors[el]
            plotter.add_mesh(pv.Sphere(center=p, radius=r, theta_resolution=30, phi_resolution=30), color=color, style='surface')


    plotter.enable_anti_aliasing()

    if args.output_file is not None:
        def save():
            plotter.screenshot(args.output_file)

        plotter.add_key_event('s', save)

    plotter.show()

if __name__ == '__main__':
    main()
