import RDC_tools
from RDC_tools.file_io import parse_coupling_spec

import h5py
import MDAnalysis
from MDAnalysis.analysis import align
from MDAnalysis.analysis.hydrogenbonds.hbond_analysis import HydrogenBondAnalysis as HBA
import numpy as np

import argparse
import itertools
import json
from pathlib import Path


def _xdr_load_offsets(self):
    self._read_offsets()
def _xdr_read_offsets(self, store=False):
    self._xdr.offsets
MDAnalysis.coordinates.XDR.XDRBaseReader._load_offsets = _xdr_load_offsets
MDAnalysis.coordinates.XDR.XDRBaseReader._read_offsets = _xdr_read_offsets

def minimum_lattice_distance(basis_vectors, pos):
    basis_vectors = np.array(basis_vectors)
    pos = np.array(pos)
    lengths = np.linalg.norm(basis_vectors, axis=1)
    directions = basis_vectors / lengths[:,None]
    coords = np.remainder(np.linalg.solve(directions.T, pos), lengths)
    refpos = np.sum(directions * coords[:,None], axis=0)
    min_distance = np.inf
    for c in itertools.product([0,1], repeat=basis_vectors.shape[0]):
        p = np.sum(basis_vectors * np.array(c)[:,None], axis=0)
        d = np.linalg.norm(p - refpos)
        if d < min_distance:
            min_distance = d
    return min_distance

modes = ['full', 'angles', 'distances', 'A']

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('structure_file')
    parser.add_argument('ligand_file')
    parser.add_argument('coupling_spec_file')
    parser.add_argument('output_file')
    parser.add_argument('trajectory_files', nargs='+')
    parser.add_argument('--mode', choices=modes)
    parser.add_argument('--Nmol', default=1)
    args = parser.parse_args()

    with open(args.coupling_spec_file) as file:
        coupling_spec = json.load(file)
        coupling_spec = parse_coupling_spec(coupling_spec)

    def mass(n):
        if n.startswith('C'):
            return 12.011
        elif n.startswith('N'):
            return 14.007
        elif n.startswith('O'):
            return 15.999
        elif n.startswith('H'):
            return 1.008

    ref = MDAnalysis.Universe(args.ligand_file).atoms
    ref.masses = np.array([mass(n) for n in ref.names])
    pos_ref = ref.positions - ref.center_of_mass()[None,:]

    prefactors = RDC_tools.RDCs.calc_prefactors(coupling_spec)
    ap_ref = RDC_tools.RDCs.calc_angle_parameters(coupling_spec, pos_ref)
    dist_ref = RDC_tools.RDCs.calc_distances(coupling_spec, pos_ref)
    dir_ref = RDC_tools.RDCs.calc_directions(coupling_spec, pos_ref)

    def calc_A(pose):
        pos_pose = pose.positions - pose.center_of_mass()[None,:]
        R, rmqd = align.rotation_matrix(pos_ref, pos_pose, weights=pose.masses)
        return R.T.dot(1/3*np.diag([-1, -1, 2]).dot(R))

    def calc_RDCs_angles(pose):
        ap = RDC_tools.RDCs.calc_angle_parameters(coupling_spec, pose.positions)
        return RDC_tools.RDCs.calc_RDCs(prefactors, dist_ref, ap)

    def calc_RDCs_distances(pose, A):
        dist = RDC_tools.RDCs.calc_distances(coupling_spec, pose.positions)
        M = RDC_tools.alignment_tensor.angle_parameter_matrix(prefactors, dir_ref, dist)
        return RDC_tools.alignment_tensor.calc_RDCs(M, A)

    def calc_RDCs(pose):
        ap = RDC_tools.RDCs.calc_angle_parameters(coupling_spec, pose.positions)
        dist = RDC_tools.RDCs.calc_distances(coupling_spec, pose.positions)
        return RDC_tools.RDCs.calc_RDCs(prefactors, dist, ap)

    Nmol = int(args.Nmol)
    Natoms = len(ref)

    values = [[] for _ in range(Nmol)]

    if args.mode == 'A':
        M_ref = RDC_tools.alignment_tensor.angle_parameter_matrix(prefactors, dir_ref, dist_ref)
        A_values = [[] for _ in range(Nmol)]

    for trajectory_file in args.trajectory_files:
        u = MDAnalysis.Universe(args.structure_file, trajectory_file)
        all_atoms = u.atoms
        pose = u.select_atoms('resname LIG', updating=True)
        pose.masses = np.array([mass(n) for n in pose.names])

        assert len(pose.atoms) == Nmol * Natoms

        if args.mode == 'A':
            M_ref = RDC_tools.alignment_tensor.angle_parameter_matrix(prefactors, dir_ref, dist_ref)

        for ts in u.trajectory:
            for i, vs in enumerate(values):
                if args.mode == 'A':
                    Avs = A_values[i]
                p = pose[i * Natoms: (i + 1) * Natoms]
                if args.mode in ('distances', 'A'):
                    A_matrix = calc_A(p)

                if args.mode == 'full':
                    vs.append(calc_RDCs(p))
                elif args.mode == 'angles':
                    vs.append(calc_RDCs_angles(p))
                elif args.mode == 'distances':
                    vs.append(calc_RDCs_distances(p, A_matrix))
                else:
                    vs.append(RDC_tools.alignment_tensor.calc_RDCs(M_ref, A_matrix))
                    A = RDC_tools.alignment_tensor.from_matrix(A_matrix)
                    Avs.append(A)

    with h5py.File(args.output_file, 'w') as file:
        file.attrs['mode'] = args.mode
        if args.mode != 'A':
            file.create_dataset(f'D', data=np.array(values, dtype='float64'))
        else:
            file.create_dataset(f'A', data=np.array(A_values, dtype='float64'))

if __name__ == '__main__':
    main()
