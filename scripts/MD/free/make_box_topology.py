import parmed

import argparse

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('struct')
    parser.add_argument('top_am')
    parser.add_argument('top_solv')
    parser.add_argument('top_lig')
    parser.add_argument('--num_ligand', default=1)
    parser.add_argument('output')
    args = parser.parse_args()

    s = parmed.load_file(args.struct)
    am = parmed.load_file(args.top_am)
    solv = parmed.load_file(args.top_solv)
    lig = parmed.load_file(args.top_lig)

    num_solv = 0
    for r in s.residues:
        if r.name == solv.residues[0].name:
            num_solv += 1

    num_ligand = int(args.num_ligand)

    res = am + num_ligand * lig + num_solv * solv

    res.save(args.output)

if __name__ == '__main__':
    main()
