import parmed

import argparse

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('struct')
    parser.add_argument('output')
    args = parser.parse_args()

    mol = parmed.load_file(args.struct, structure=True)

    mol.coordinates = -mol.coordinates
    mol.box = None
    mol.save(args.output)

if __name__ == '__main__':
    main()
