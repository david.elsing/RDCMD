import parmed
import numpy as np

import argparse

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('struct')
    parser.add_argument('output')
    args = parser.parse_args()

    mol = parmed.load_file(args.struct, structure=True)

    m = np.array([a.mass for a in mol.atoms])

    com = np.sum(mol.coordinates * m[:,None], axis=0) / np.sum(m)

    mol.coordinates = mol.coordinates - com
    mol.save(args.output)

if __name__ == '__main__':
    main()
