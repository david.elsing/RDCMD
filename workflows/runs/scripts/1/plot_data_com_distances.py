import h5py
import numpy as np

import argparse
from pathlib import Path
import json

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('path')
    parser.add_argument('first_index')
    parser.add_argument('last_index')
    parser.add_argument('num_stages')
    parser.add_argument('output')
    args = parser.parse_args()

    path = Path(args.path)

    first_index = int(args.first_index)
    last_index = int(args.last_index)
    num_stages = int(args.num_stages)

    r = np.linspace(0, 20, 100)
    histogram = 0

    Nframes = 0
    Nmol = None
    Nams = None

    for index in range(first_index, last_index+1):
        filename = path / f"{index}/{num_stages}/com_distances.hdf5"

        with h5py.File(filename) as file:
            distance = np.array(file['distance'])
            if Nmol is None:
                Nmol = distance.shape[2]
            else:
                assert Nmol == distance.shape[2]

            if Nams is None:
                Nams = distance.shape[1]
            else:
                assert Nams == distance.shape[1]

            Nframes += distance.shape[0]

            distance = distance.ravel()

            new_hist, _ = np.histogram(distance, bins=r, density=False)
            histogram += new_hist

    with h5py.File(args.output, 'w') as file:
        file.attrs['Nframes'] = Nframes
        file.attrs['Nams'] = Nams
        file.attrs['Nmol'] = Nmol
        file.create_dataset('r', data=r)
        file.create_dataset('histogram', data=histogram)

if __name__ == '__main__':
    main()
