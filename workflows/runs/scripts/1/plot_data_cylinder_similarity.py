import RDC_tools
from RDC_tools.file_io import parse_coupling_spec, read_RDC_file

import numpy as np

import argparse
import json


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('enantiomer')
    parser.add_argument('molname')
    parser.add_argument('coupling_spec')
    parser.add_argument('Dexp')
    parser.add_argument('Dcylinder')
    parser.add_argument('output')
    args = parser.parse_args()

    with open(args.coupling_spec) as file:
        coupling_spec = json.load(file)
        coupling_spec = parse_coupling_spec(coupling_spec)

    with open(args.Dexp) as file:
        Dexp, _, _, _ = read_RDC_file(file)

    Dcylinder = np.loadtxt(args.Dcylinder)
    sim = RDC_tools.alignment_tensor.similarity(Dexp, Dcylinder)

    with open(args.output, 'w') as file:
        file.write(f'{sim}\n')


if __name__ == '__main__':
    main()
