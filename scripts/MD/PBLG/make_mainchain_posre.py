import argparse
import json

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('numbers')
    parser.add_argument('parameters')
    parser.add_argument('output')
    args = parser.parse_args()

    with open(args.numbers) as file:
        numbers = json.load(file)

    Nmonomer = numbers['Nmonomer']
    first_mainchain = numbers['first_mainchain']

    parameters = args.parameters

    with open(args.output, 'w') as file:
        file.write('[ position_restraints ]\n')
        for i in range(numbers['num_monomers']):
            for j in first_mainchain:
                index = 1 + j + i*Nmonomer
                file.write(f'{index} {parameters}\n')

if __name__ == '__main__':
    main()
