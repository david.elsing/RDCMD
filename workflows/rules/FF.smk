localrules: three_capped_monomers
rule three_capped_monomers:
    input:
        "results/molecules/PBLG/adjusted.pdb",
        "results/molecules/PBLG/buildspec.json"
    output:
        "results/FF/PBLG/three_capped/mol.pdb",
        "results/FF/PBLG/three_capped/numbers.json"
    params:
        script=workflow.source_path("../../scripts/PBLG/build_polymer.py"),
        shell="""
            python3 {params.script:q} build {input[0]:q} {input[1]:q} 3 {output[0]:q} {output[1]:q}
            """,
        guix="python"
    script:
        "../run_smk_rule.py"

localrules: PBLG_AMBER_parameters
rule PBLG_AMBER_parameters:
    input:
        "results/FF/PBLG/three_capped/mol.pdb"
    output:
        prmtop="results/FF/PBLG/three_capped/AMBER/mol.prmtop",
        dir=directory("results/FF/PBLG/three_capped/AMBER/rundir")
    params:
        leap_script=workflow.source_path("../../scripts/leap_ff14SBonlysc.in"),
        shell="""
            leap_script="$(readlink -f {params.leap_script:q})"
            mkdir {output.dir:q}
            cp {input[0]:q} {output.dir:q}/mol.pdb
            cd {output.dir:q}
            antechamber -fi pdb -i mol.pdb -fo mol2 -o mol_AC.mol2 -at amber
            parmchk2 -i mol_AC.mol2 -f mol2 -o mol.frcmod -s 1
            tleap -s -f "$leap_script"
            cp mol.prmtop ..
            """,
        guix="ambertools"
    script:
        "../run_smk_rule.py"

localrules: AMBER_parameters
rule AMBER_parameters:
    input:
        "results/molecules/{which}/{name}/initial_mol_conv.mol2"
    output:
        dir=directory("results/FF/{which}/{name}/AMBER"),
        prmtop="results/FF/{which}/{name}/AMBER/mol.prmtop"
    wildcard_constraints:
        which="(small|TCM)",
        name="[\w-]+"
    params:
        leap_script=workflow.source_path("../../scripts/leap_gaff.in"),
        shell="""
            leap_script="$(readlink -f {params.leap_script:q})"
            cp {input[0]:q} {output.dir:q}/mol.mol2
            cd {output.dir:q}
            antechamber -fi mol2 -i mol.mol2 -fo mol2 -o mol_AC.mol2 -at amber
            parmchk2 -i mol_AC.mol2 -f mol2 -o mol.frcmod -s 1
            tleap -s -f "$leap_script"
            """,
        guix="ambertools"
    script:
        "../run_smk_rule.py"

def small_topology_top(w):
    if w.which == 'TCM':
        return "scripts/MD/TCM/TCM.top"
    else:
        return f"results/FF/{w.which}/{w.name}/AMBER/mol.prmtop"

localrules: small_topology
rule small_topology:
    input:
        script=lambda w: f"scripts/{w.which}/make_topology.py",
        top=small_topology_top,
        struct="results/molecules/{which}/{name}/named.gro",
        charges="results/FF/{which}/{name}/charges/RESP/HF/charges_adjusted"
    output:
        "results/FF/{which}/{name}/AMBER.top"
    wildcard_constraints:
        which="(small|TCM)",
        name="[\w-]+"
    params:
        shell="""
        python3 {input.script:q} {input.top:q} {input.struct:q} {input.charges:q} {output[0]:q}
        """,
        guix="python"
    conda:
        "../conda/parmed.yaml"
    script:
        "../run_smk_rule.py"
