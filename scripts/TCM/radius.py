N_A = 6.02214076e23

rho = 1.48 # g/cm**3 https://www.cdc.gov/niosh/npg/npgd0127.html
M = 119.37 # g/mol

V = (M / rho)/100**3 / N_A * (1e10)**3
r = V**(1/3)
print(r/10)
