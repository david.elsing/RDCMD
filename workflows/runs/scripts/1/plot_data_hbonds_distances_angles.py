import h5py
import numpy as np

import argparse
from pathlib import Path
import json

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('path')
    parser.add_argument('hbonds_receptor')
    parser.add_argument('first_index')
    parser.add_argument('last_index')
    parser.add_argument('num_stages')
    parser.add_argument('output')
    parser.add_argument('--Nmol')
    args = parser.parse_args()

    path = Path(args.path)

    with open(args.hbonds_receptor) as file:
        hbonds_receptor = json.load(file)

    first_index = int(args.first_index)
    last_index = int(args.last_index)
    num_stages = int(args.num_stages)

    min_dist = 1.5
    max_dist = 4
    N_grid_dist = 500
    grid_dist = np.linspace(min_dist, max_dist, N_grid_dist)

    min_angle = 90
    N_grid_angle = 200
    grid_angle = np.linspace(min_angle, 180, N_grid_angle)

    results = {f'acc_index{i}': 0 for i in hbonds_receptor['acceptors']}

    Nmol = int(args.Nmol)
    Nframes = 0
    Nchains = None

    for index in range(first_index, last_index+1):
        filename = path / f"{index}/{num_stages}/hbonds.hdf5"

        with h5py.File(filename) as file:
            Nframes += file.attrs['Nframes']

            if Nchains is None:
                Nchains = file.attrs['Nchains']
            else:
                assert Nchains == file.attrs['Nchains']

            for k in range(Nchains):
                c_group = file[f'chain{k}']
                for groupname in results:
                    group = c_group[groupname]
                    HA_dist = np.array(group['HA_distance'])
                    hbond_angle = np.array(group['angle']) * 180 / np.pi

                    f, _, _ = np.histogram2d(HA_dist, hbond_angle, bins=(grid_dist, grid_angle))

                    results[groupname] += f

    with h5py.File(args.output, 'w') as file:
        file.create_dataset('grid_dist', data=grid_dist)
        file.create_dataset('grid_angle', data=grid_angle)
        group = file.create_group('histograms')
        for groupname, acc_results in results.items():
            group.create_dataset(groupname, data=acc_results / (Nframes * Nmol))


if __name__ == '__main__':
    main()
