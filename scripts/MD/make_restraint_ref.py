import argparse

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('struct')
    parser.add_argument('output')
    args = parser.parse_args()

    with open(args.output, 'w') as outfile:
        with open(args.struct) as file:
            for i, line in enumerate(file):
                if i == 0:
                    newline = line
                elif i == 1:
                    newline = line
                    num = int(line)
                elif i < 2 + num:
                    newline = line[:20] + 3*'   0.000' + '\n'
                else:
                    newline = line

                outfile.write(newline)

if __name__ == '__main__':
    main()
