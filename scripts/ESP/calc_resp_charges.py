import subprocess
import argparse
from pathlib import Path

def f(s):
    subprocess.run(s, shell=True, check=True)

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('mol2file')
    parser.add_argument('--respgen_a')
    args = parser.parse_args()

    name = Path(args.mol2file).stem
    mol2file = Path(args.mol2file)

    if args.respgen_a is not None:
        p_respgen_a = Path(args.respgen_a)

    f(f'antechamber -fi mol2 -i "{mol2file}" -fo ac -o "{name}".ac')
    s1 = f'respgen -i "{name}.ac" -o stage1.respin -f resp1'
    s2 = f'respgen -i "{name}.ac" -o stage2.respin -f resp2'
    if args.respgen_a is not None:
        s1 += f' -a "{p_respgen_a}"'
        s2 += f' -a "{p_respgen_a}"'
    f(s1)
    f(s2)

    f('resp -O -i stage1.respin -o stage1.respout -e esp.dat -t qout_stage1')
    f('resp -O -i stage2.respin -o stage2.respout -e esp.dat -q qout_stage1 -t qout_stage2')
    f('xargs -n 1 < qout_stage2 > charges')

if __name__ == '__main__':
    main()
