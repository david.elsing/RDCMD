from snakemake.utils import format

import subprocess
from pathlib import Path
import shlex
import os

input = snakemake.input
output = snakemake.output
params = snakemake.params
rule = snakemake.rule
wildcards = snakemake.wildcards
resources = snakemake.resources

cmd_prefix = ''

env = os.environ.copy()

if isinstance(params.guix, list):
    guix_profiles = params.guix
else:
    guix_profiles = [params.guix]

for guix_profile in guix_profiles:
    pguix = (Path('workflows/guix/profiles') / guix_profile).resolve(strict=True)
    cmd_prefix += 'export GUIX_PROFILE=' + shlex.quote(pguix.as_posix()) + '; '
    cmd_prefix += 'source "$GUIX_PROFILE"/etc/profile; '

cmd_prefix += 'set -euo pipefail; '

logfile = Path(format('logs/{rule}/log_{wildcards}.txt', stepout=1))
logfile.parent.mkdir(exist_ok=True, parents=True)

def run(cmd):
    with open(logfile, 'w') as file:
        subprocess.run(cmd, shell=True, check=True, stdout=file, stderr=subprocess.STDOUT, env=env)

if hasattr(params, 'shell'):
    # As in snakemake/shell.py
    cmd = format(params.shell, stepout=1)
    cmd = " ".join((cmd_prefix, cmd)).strip()
    run(cmd)
