import h5py
import MDAnalysis
import numpy as np

import argparse
import json

def _xdr_load_offsets(self):
    self._read_offsets()
def _xdr_read_offsets(self, store=False):
    self._xdr.offsets
MDAnalysis.coordinates.XDR.XDRBaseReader._load_offsets = _xdr_load_offsets
MDAnalysis.coordinates.XDR.XDRBaseReader._read_offsets = _xdr_read_offsets

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('structure_file')
    parser.add_argument('buildspec')
    parser.add_argument('numbers')
    parser.add_argument('hbonds_receptor')
    parser.add_argument('hbond_ligand')
    parser.add_argument('max_distance')
    parser.add_argument('output_file')
    parser.add_argument('trajectory_files', nargs='+')
    parser.add_argument('--Nmol', default=1)
    parser.add_argument('--Nchains', default=1)
    args = parser.parse_args()

    with open(args.buildspec) as file:
        buildspec = json.load(file)

    with open(args.numbers) as file:
        numbers = json.load(file)

    Nmonomer = numbers['Nmonomer']
    num_monomers = numbers['num_monomers']

    Nrecatoms = Nmonomer * num_monomers

    with open(args.hbonds_receptor) as file:
        hbonds_receptor = json.load(file)

    with open(args.hbond_ligand) as file:
        hbond_ligand = json.load(file)

    max_distance = float(args.max_distance)

    acceptor_indices = hbonds_receptor['acceptors']
    acceptor_selections = []

    Nmol = int(args.Nmol)
    Nchains = int(args.Nchains)

    results = [
        [
            {
                'mol_index': [],
                'frame_index': [],
                'monomer_index': [],
                'DA_distance': [],
                'HA_distance': [],
                'angle': []
            } for _ in acceptor_indices
        ] for _ in range(Nchains)
    ]


    frame_index = 0

    for trajectory_file in args.trajectory_files:
        u = MDAnalysis.Universe(args.structure_file, trajectory_file)

        ams = u.select_atoms('resname PMO')

        Nchain_atoms = len(ams.atoms) // Nchains

        acceptors = []
        for k in range(Nchains):
            chain_acceptors = []
            acceptors.append(chain_acceptors)
            first_index = min(a.ix for a in ams[Nchain_atoms * k: Nchain_atoms * (k + 1)] for k in range(Nchains))
            for i in acceptor_indices:
                index = first_index + buildspec['new_indices'][i-1] - len(buildspec['fullcaps'][0])
                sel = [f'index {index + k*Nmonomer}' for k in range(num_monomers)]
                chain_acceptors.append(sum(u.select_atoms(s, updating=True) for s in sel))

        ligands = u.select_atoms(f'resname LIG')

        Natoms = len(ligands.atoms) // Nmol

        first_indices = [min(a.ix for a in ligands[Natoms * k: Natoms * (k + 1)]) for k in range(Nmol)]

        donors = u.select_atoms(f'resname LIG and (' + ' or '.join(f'id {fi + hbond_ligand["donor"]}' for fi in first_indices) + ')', updating=True)
        hydrogens = u.select_atoms(f'resname LIG and (' + ' or '.join(f'id {fi + hbond_ligand["hydrogen"]}' for fi in first_indices) + ')', updating=True)

        for frame in u.trajectory:
            pdonors = donors.positions
            phydrogens = hydrogens.positions

            for chain_acceptors, chain_results in zip(acceptors, results):
                for acc, acc_results in zip(chain_acceptors, chain_results):
                    racceptor = acc.positions

                    for j in range(Nmol):
                        rdonor = pdonors[j]
                        rhydrogen = phydrogens[j]
                        DA_dists = MDAnalysis.lib.distances.distance_array(rdonor, racceptor, box=frame.dimensions)[0,:]
                        index = np.argmin(DA_dists)
                        HA_dist = MDAnalysis.lib.distances.distance_array(rhydrogen, racceptor[index], box=frame.dimensions).ravel()[0]
                        hbond_angle = MDAnalysis.lib.distances.calc_angles(racceptor[index], rhydrogen, rdonor, box=frame.dimensions)

                        if DA_dists[index] <= max_distance:
                            acc_results['mol_index'].append(j)
                            acc_results['frame_index'].append(frame_index)
                            acc_results['monomer_index'].append(index)
                            acc_results['DA_distance'].append(DA_dists[index])
                            acc_results['HA_distance'].append(HA_dist)
                            acc_results['angle'].append(hbond_angle)

            frame_index += 1

    with h5py.File(args.output_file, 'w') as file:
        file.attrs['Nframes'] = frame_index
        file.attrs['Nchains'] = Nchains
        for i, chain_results in enumerate(results):
            gc = file.create_group(f'chain{i}')
            for j, acc_results in zip(acceptor_indices, chain_results):
                ga = gc.create_group(f'acc_index{j}')
                ga.create_dataset('mol_index', data=np.atleast_1d(np.array(acc_results['mol_index'])))
                ga.create_dataset('frame_index', data=np.atleast_1d(np.array(acc_results['frame_index'])))
                ga.create_dataset('monomer_index', data=np.atleast_1d(np.array(acc_results['monomer_index'])))
                ga.create_dataset('DA_distance', data=np.atleast_1d(np.array(acc_results['DA_distance'])))
                ga.create_dataset('HA_distance', data=np.atleast_1d(np.array(acc_results['HA_distance'])))
                ga.create_dataset('angle', data=np.atleast_1d(np.array(acc_results['angle'])))

if __name__ == '__main__':
    main()
