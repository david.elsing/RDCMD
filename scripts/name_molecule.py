import parmed

import argparse

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('input')
    parser.add_argument('resname')
    parser.add_argument('output')
    args = parser.parse_args()

    mol = parmed.load_file(args.input, structure=True)

    mol.residues = parmed.ResidueList()

    element_indices = {}
    for a in mol.atoms:
        n = a.atomic_number
        if n not in element_indices:
            element_indices[n] = 1
        a.name = f'{parmed.periodic_table.Element[n]}{element_indices[n]}'
        element_indices[n] += 1

        mol.residues.add_atom(a, args.resname, 1)

    mol.save(args.output)

if __name__ == '__main__':
    main()
