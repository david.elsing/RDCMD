import parmed
import MDAnalysis
import MDAnalysis.analysis.distances
import h5py
import numpy as np

import argparse
import itertools
import json

def _xdr_load_offsets(self):
    self._read_offsets()
def _xdr_read_offsets(self, store=False):
    self._xdr.offsets
MDAnalysis.coordinates.XDR.XDRBaseReader._load_offsets = _xdr_load_offsets
MDAnalysis.coordinates.XDR.XDRBaseReader._read_offsets = _xdr_read_offsets

def minimum_lattice_distance(basis_vectors, pos):
    basis_vectors = np.array(basis_vectors)
    pos = np.array(pos)
    lengths = np.linalg.norm(basis_vectors, axis=1)
    directions = basis_vectors / lengths[:,None]
    coords = np.remainder(np.linalg.solve(directions.T, pos), lengths)
    refpos = np.sum(directions * coords[:,None], axis=0)
    min_distance = np.inf
    for c in itertools.product([0,1], repeat=basis_vectors.shape[0]):
        p = np.sum(basis_vectors * np.array(c)[:,None], axis=0)
        d = np.linalg.norm(p - refpos)
        if d < min_distance:
            min_distance = d
    return min_distance

def com(x, m):
    return np.sum(m[..., None] * x, axis=-2) / np.sum(m, axis=-1)[..., None]

def inertia_tensor(x, m):
    c = com(x, m)
    x = x - c[..., None, :]
    M = np.sum(m, axis=-1)
    xsq = np.sum(x**2, axis=-1)
    xixj = np.einsum('...i,...j->...ij', x, x)
    return np.sum(m[..., None, None] * (xsq[..., None, None] * np.eye(3) - xixj), axis=-3) / M[..., None, None]

def chain_axis(x, m):
    I = inertia_tensor(x, m)
    _, eigv = np.linalg.eigh(I)
    return eigv[..., 0]

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('structure_file')
    parser.add_argument('top_ams')
    parser.add_argument('top_ligand')
    parser.add_argument('numbers')
    parser.add_argument('output_file')
    parser.add_argument('trajectory_files', nargs='+')
    parser.add_argument('--Nmol', default=1)
    parser.add_argument('--Nchains', default=1)
    args = parser.parse_args()

    Nmol = int(args.Nmol)
    Nchains = int(args.Nchains)

    distances = []

    top_ligand = parmed.load_file(args.top_ligand)
    masses_ligs = np.tile(np.array([a.mass for a in top_ligand.atoms]), (Nmol, 1))
    top_ams = parmed.load_file(args.top_ams)
    masses_ams = np.array([a.mass for a in top_ams.atoms]).reshape((Nchains, -1))

    box_nums = np.array(list(itertools.product([-1, 0, 1], repeat=3)))

    with open(args.numbers) as file:
        numbers = json.load(file)

    Nmonomer = numbers['Nmonomer']
    first_mainchain = numbers['first_mainchain']
    single_backbone_indices = []
    for i in range(numbers['num_monomers']):
        for j in first_mainchain:
            single_backbone_indices.append(j + i * Nmonomer)

    single_backbone_indices = np.array(single_backbone_indices)

    masses_backbones = masses_ams[:, single_backbone_indices]

    for trajectory_file in args.trajectory_files:
        u = MDAnalysis.Universe(args.structure_file, trajectory_file)

        ams = u.select_atoms('resname PMO')

        Nchain_atoms = len(ams.atoms) // Nchains

        ligands = u.select_atoms(f'resname LIG', updating=True)

        Natoms = len(ligands.atoms) // Nmol

        for frame in u.trajectory:
            pos_ligs = ligands.positions.reshape((Nmol, Natoms, 3))
            pos_ams = ams.positions.reshape((Nchains, Nchain_atoms, 3))

            pos_backbones = pos_ams[:, single_backbone_indices, :]

            com_ligs = com(pos_ligs, masses_ligs)
            com_ams = com(pos_backbones, masses_backbones)

            axis = chain_axis(pos_backbones, masses_backbones)

            box_choices = np.sum(box_nums[..., None] * frame.triclinic_dimensions, axis=-2)

            com_ligs2 = com_ligs[..., None, :] + box_choices

            ref_ams = np.tile(com_ams[:, None, None, :], (1, Nmol, len(box_choices), 1))
            ref_ams += np.sum((com_ligs2 - ref_ams) * axis[..., None, None, :], axis=-1)[..., None] * axis[..., None, None, :]

            nearests_distsqs = np.sort(np.sum((com_ligs2 - ref_ams)**2, axis=-1), axis=-1)

            dist = np.sqrt(nearests_distsqs[..., :4])

            distances.append(dist)

    with h5py.File(args.output_file, 'w') as file:
        file.create_dataset(f'distance', data=np.array(distances))

if __name__ == '__main__':
    main()
