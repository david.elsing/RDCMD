import numpy as np
import sgsrmsd
import h5py

import argparse

parser = argparse.ArgumentParser()
parser.add_argument('couplings')
parser.add_argument('perm')
parser.add_argument('output')
parser.add_argument('--inverse', action='store_true')
args = parser.parse_args()

with h5py.File(args.perm, 'r') as file:
    iso_perm = np.array(file['iso_perms']['iso_0_0'])
    perm = np.array(file['perms']['0_0'])

perm = sgsrmsd.compose(iso_perm, perm)
if args.inverse:
    perm = sgsrmsd.inverse(perm)

invperm = sgsrmsd.inverse(perm)

pairs = []
Ds = []
Derrs = []

with open(args.couplings) as file:
    for line in file:
        line = line.strip()
        if len(line) == 0 or line.startswith('rdc_data') or line.startswith('#') or line.startswith('}'):
            continue
        parts = line.split()
        print(line)
        pairs.append((int(parts[0]), int(parts[1])))
        Ds.append(parts[2])
        Derrs.append(parts[3])

with open(args.output, 'w') as file:
    file.write('rdc_data {\n')
    for p, D, Derr in zip(pairs, Ds, Derrs):
        p2 = invperm[p[0] - 1] + 1, invperm[p[1] - 1] + 1
        s = ' '.join(str(v).rjust(6) for v in [p2[0], p2[1], D, Derr])
        file.write(s + '\n')
    file.write('}\n')
