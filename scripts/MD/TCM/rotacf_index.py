import parmed

import argparse
import subprocess

parser = argparse.ArgumentParser()
parser.add_argument('top')
parser.add_argument('output')
args = parser.parse_args()

mol = parmed.load_file(args.top)

Nmol = len(mol.residues)
Natoms = len(mol.residues[0].atoms)

indices = {}

atoms = ['H', 'C', 'Cl']
found = {el: False for el in atoms}

for i, a in enumerate(mol.residues[0].atoms):
    el = a.element_name
    if el in found and not found[el]:
        indices[el] = i
        found[el] = True

indices = [indices[el] for el in atoms]

all_indices = []
for n in range(Nmol):
    all_indices += [1 + n * Natoms + i for i in indices]

res = '[ ref ]'

c = 0
for n in range(Nmol):
    if c % 15 == 0:
        res += '\n'
    for i in indices:
        index = 1 + n * Natoms + i
        if c % 15 != 0:
            res += ' '
        res += str(index).rjust(4)
        c += 1

res += '\n'

with open(args.output, 'w') as file:
    file.write(res)
