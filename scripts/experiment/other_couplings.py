import RDC_tools
from RDC_tools import file_io, RDCs
import numpy as np

import argparse
import json
import itertools
import copy

parser = argparse.ArgumentParser()
parser.add_argument('averaged_couplings')
parser.add_argument('averaged_RDCs')
parser.add_argument('other')
parser.add_argument('molname')
parser.add_argument('enantiomer')
parser.add_argument('output_couplings', default=None)
parser.add_argument('output_RDCs', default=None)
parser.add_argument('--save_av', default=None)
parser.add_argument('--perm_RDCs', action='store_true')
parser.add_argument('--save_perms', default=None)
parser.add_argument('--first_perms', default=None)
args = parser.parse_args()

if args.save_av is not None and args.perm_RDCs:
    raise RuntimeError('save_av is incompatible with perm_RDCs')

def parse_coupling(cs):
    ctype = cs['type']
    coupling_average = {
        'orientations': [],
        'scalings': [],
        'pairs': [],
        'pair_isotopes': []
    }
    if ctype == 'normal':
        file_io._parse_normal(cs, coupling_average)
    elif ctype == 'projected':
        file_io._parse_projected(cs, coupling_average)
    else:
        raise RuntimeError(f'Invalid coupling type "{ctype}"')

    return coupling_average

def split_spec_data(cs):
    res = []
    for cs2 in cs['couplings']:
        cs2 = copy.deepcopy(cs2)
        res.append(cs2)
    return res

def split_RDCs(D):
    return [np.array([v]) for v in D]

def apply_perm(perm, l):
    return [l[i] for i in perm]

with open(args.averaged_RDCs) as file:
    _, _, D, Derr = file_io.read_RDC_file(file)

with open(args.averaged_couplings) as file:
    spec = json.load(file)

other_pairs = []
other_Ds = []
other_Derrs = []
with open(args.other) as file:
    for line in file:
        line = line.strip()
        if len(line) == 0 or line.startswith('rdc_data') or line.startswith('#') or line.startswith('}'):
            continue
        parts = line.split()
        other_pairs.append(sorted((int(parts[0])-1, int(parts[1])-1)))
        other_Ds.append(parts[2])
        other_Derrs.append(parts[3])

other_indices = {tuple(p): i for i, p in enumerate(other_pairs)}

data = spec['data']
av_couplings = file_io.parse_coupling_spec(spec)

res_couplings = []
res_spec = copy.deepcopy(spec)
res_data = res_spec['data'] = []
res_av_data = []
res_D = []
res_Derr = []
perms = []

iactual = -1
for i, c in enumerate(av_couplings):
    correct_enantiomer = 'onlyenantiomer' not in data[i] or args.enantiomer == data[i]['onlyenantiomer']
    if not correct_enantiomer:
        res_av_data.append(data[i])
        continue

    iactual += 1

    type = data[i]['type']
    if type == 'normal':
        pair = tuple(sorted(c['pairs'][0]))
        index = other_indices[pair]
        if np.abs(D[iactual][0] - float(other_Ds[index])) > 1e-2 or np.abs(Derr[iactual][0] - float(other_Derrs[index])) > 1e-2:
            raise RuntimeError(f'Differing RDCs: i={i}, index={index}')
        res_data.append(data[i])
        res_av_data.append(data[i])
        res_D.append(D[iactual][0])
        res_Derr.append(Derr[iactual][0])
    elif type == 'projected':
        pairs = [tuple(sorted(p)) for p in c['pairs']]
        indices = [other_indices[p] for p in pairs]
        if len(set(indices)) != 3:
            raise RuntimeError(f'Differing pairs: i={i}, indices={indices}')
        for index in indices:
            if np.abs(D[iactual][0] - float(other_Ds[index])) > 1e-2 or np.abs(Derr[iactual][0] - float(other_Derrs[index])) > 1e-2:
                raise RuntimeError(f'Differing RDCs: i={i}, index={index}')

        res_data.append(data[i])
        res_av_data.append(data[i])
        res_D.append(D[iactual][0])
        res_Derr.append(Derr[iactual][0])
    else:
        cs_split = split_spec_data(data[i])
        c_split = [parse_coupling(cs) for cs in cs_split]
        D_split = split_RDCs(D[iactual])
        Derr_split = split_RDCs(Derr[iactual])

        type = None
        for c2 in cs_split:
            if type is None:
                type = c2['type']
            elif c2['type'] != type:
                raise RuntimeError(f'Different types in average')

        if type not in ['normal', 'projected']:
            raise RuntimeError(f'Invalid type in average: {type}')

        indices = []
        orig_pairs = []
        orig_orientations = []

        for j, c2 in enumerate(cs_split):
            if type == 'normal':
                orig_pairs.append(c2['pair'])
                pair = tuple(sorted([v['index'] - 1 for v in c2['pair']]))
                index = other_indices[pair]
                indices.append(index)
            else:
                pairs = [tuple(sorted([v['index'] - 1 for v in p])) for p in c2['pairs']]
                orig_pairs.append(c2['pairs'])
                orig_orientations.append(c2['orientation'])
                indices.append([other_indices[p] for p in pairs])

        data_av = copy.deepcopy(data[i])
        cs_split_new = copy.deepcopy(cs_split)

        perm = []
        found_Dindices = set()
        for j, (c2, c2_new) in enumerate(zip(cs_split, cs_split_new)):
            if type == 'normal':
                for k, index in enumerate(indices):
                    if index in found_Dindices:
                        continue
                    if np.abs(D_split[j][0] - float(other_Ds[index])) <= 1e-2 and np.abs(Derr_split[j][0] - float(other_Derrs[index])) <= 1e-2:
                        perm.append(k)

                        if not args.perm_RDCs:
                            c2_new['pair'] = orig_pairs[k]
                            data_av['couplings'][j]['pair'] = orig_pairs[k]

                        found_Dindices.add(index)
                        break
                else:
                    raise RuntimeError(f'Not found: {c2}, {D_split[j]}, {Derr_split[j]}')
            else:
                for k, l_index in enumerate(indices):
                    l_index = tuple(sorted(l_index))
                    if l_index in found_Dindices:
                        continue

                    D_other, Derr_other = float(other_Ds[l_index[0]]), float(other_Derrs[l_index[0]])
                    for index in l_index[1:]:
                        if np.abs(float(other_Ds[index]) - float(D_other)) > 1e-2 or np.abs(float(other_Derrs[index]) - float(Derr_other)) > 1e-2:
                            raise RuntimeError('other Ds of projected not the same')

                    if np.abs(D_split[j][0] - D_other) <= 1e-2 and np.abs(Derr_split[j][0] - Derr_other) <= 1e-2:
                        perm.append(k)

                        if not args.perm_RDCs:
                            c2_new['pairs'] = orig_pairs[k]
                            c2_new['orientation'] = orig_orientations[k]
                            data_av['couplings'][j]['pairs'] = orig_pairs[k]
                            data_av['couplings'][j]['orientation'] = orig_orientations[k]

                        found_Dindices.add(l_index)
                        break
                else:
                    raise RuntimeError(f'Not found: {c2}, {D_split[j]}, {Derr_split[j]}')

        res_av_data.append(data_av)
        res_data += cs_split_new

        if not args.perm_RDCs:
            res_D += [v[0] for v in D_split]
            res_Derr += [v[0] for v in Derr_split]
        else:
            new_D = len(D_split) * [None]
            new_Derr = len(Derr_split) * [None]
            for l, k in enumerate(perm):
                new_D[k] = D_split[l][0]
                new_Derr[k] = Derr_split[l][0]
            res_D += new_D
            res_Derr += new_Derr
        perms.append(perm)

if args.output_couplings is not None:
    with open(args.output_couplings, 'w') as file:
        json.dump(res_spec, file, indent=2)
        file.write('\n')

with open(args.output_RDCs, 'w') as file:
    file.write(f'{len(res_D)}\n')
    for v, verr in zip(res_D, res_Derr):
        file.write(f'{v:.3f} {verr:.3f}\n')

if args.save_av is not None:
    res_spec['data'] = res_av_data
    with open(args.save_av, 'w') as file:
        json.dump(res_spec, file, indent=2)
        file.write('\n')

if args.save_perms is not None:
    if args.first_perms is not None:
        with open(args.first_perms) as file:
            first_perms = json.load(file)['perms']

        fp_index = 0
        new_first_perms = []
        for i, d in enumerate(data):
            if d['type'] != 'average':
                continue

            if 'onlyenantiomer' not in d:
                new_first_perms.append(first_perms[fp_index])
                fp_index += 1
            elif args.enantiomer == d['onlyenantiomer']:
                new_first_perms.append(list(range(len(perms[i]))))
            else:
                fp_index += 1

        perms = [[first_perm[i] for i in perm]
                 for (first_perm, perm) in zip(new_first_perms, perms)]

    with open(args.save_perms, 'w') as file:
        json.dump({'perms': perms}, file, indent=2)
        file.write('\n')
