import parmed

import argparse
import json

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('monomer_capped')
    parser.add_argument('buildspec')
    parser.add_argument('output_file')
    args = parser.parse_args()

    mol = parmed.load_file(args.monomer_capped)

    with open(args.buildspec) as file:
        buildspec = json.load(file)

    fullcap1, fullcap2 = buildspec['fullcaps']
    monomer = buildspec['monomer']

    with open(args.output_file, 'w') as file:
        for group in [fullcap1, monomer, fullcap2]:
            file.write(f'GROUP {len(group)} 0.0\n')
            for i in group:
                a = mol.atoms[i]
                file.write(f'ATOM {i+1} {a.name}\n')


if __name__ == '__main__':
    main()
