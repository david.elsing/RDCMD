import common

rule cylinder:
    input:
        gro="results/molecules/small/chosen/{enantiomer}-{molname}.gro",
        top=common.small_ligand_top_input
    output:
        "results/cylinder/chosen/{enantiomer}-{molname}/A_{radius}.txt"
    wildcard_constraints:
        radius="\d+\.\d+",
        enantiomer=common.re_enantiomer,
        molname=common.re_molname,
    params:
        rel_err=0.01,
        batch_size=10000,
        script=workflow.source_path("../../scripts/cylinder/calc_cylinder_RDCs.py"),
        shell="""
        python3 {params.script:q} {input.gro:q} {input.top:q} {output[0]:q} \
            --radius {wildcards.radius:q} --rel_err {params.rel_err:q} --batch_size {params.batch_size:q}
        """,
        guix="python"
    script:
        "../run_smk_rule.py"

def cylinder_copy_input(w):
    name = common.choose_name(f'{w.enantiomer}-{w.molname}')
    return f"results/cylinder/chosen/{name}/A_{w.radius}.txt"

localrules: cylinder_copy
rule cylinder_copy:
    input:
        cylinder_copy_input
    output:
        "results/cylinder/{enantiomer}-{molname}/A_{radius}.txt"
    wildcard_constraints:
        radius="\d+\.\d+",
        enantiomer=common.re_enantiomer,
        molname=common.re_molname
    shell:
        """
        cp {input[0]:q} {output[0]:q}
        """
