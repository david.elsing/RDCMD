(use-modules (gnu packages chemistry)
			 (guix packages)
			 (guix utils)
			 (guix gexp))

(define-public gromacs-HoreKa
  (package
   (inherit gromacs)
   (name "gromacs-HoreKa")
   (arguments
	(substitute-keyword-arguments
	 (package-arguments gromacs)
	 ((#:tests? tests? #f) #f)
	 ((#:build-type _ #f) "Release")
	 ((#:configure-flags flags ''())
	  #~(append #$flags
				'("-DGMX_SIMD=AVX_512"
				  "-DCMAKE_INTERPROCEDURAL_OPTIMIZATION=ON"
				  #$(string-append
					 "-DCMAKE_CXX_FLAGS_RELEASE="
					 "-Ofast"
					 ;; Adjust for CPU
					 " -march=icelake-server"
					 " -DNDEBUG"))))))))

(concatenate-manifests
 (list
  (specifications->manifest
   '("bash"
	 "coreutils"
	 "sed"
	 "gawk"))
  (packages->manifest (list gromacs-HoreKa))))

