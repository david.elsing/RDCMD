from pathlib import Path

molnames = ['IPC', 'quinuclidinol', 'borneol', 'camphor']
enantiomers = [['m', 'p'], ['Rm', 'Sp'], ['Sm', 'Rp'], ['Sm', 'Rp']]

def PBLG_long_input(w):
    input = {
        'same': []
    }
    for name in ['edr', 'gro', 'log', 'tpr']:
        input['same'].append(f"results/MD/PBLG/long/{w.which}/{w.which}.{name}")
    trajext = 'trr' if w.which == 'em' else 'xtc'
    input['same'].append(f"results/MD/PBLG/long/{w.which}/{w.which}.{trajext}")
    input['same'].append("results/MD/PBLG/long/{which}/mdout.mdp")

    topname = 'em' if w.which == 'em' else 'md'
    input['top'] = f"results/MD/PBLG/long/{topname}.top"
    input['same'].append(f"results/MD/PBLG/long/{topname}_posre.itp")

    return input

localrules: PBLG_long
rule PBLG_long:
    input:
        unpack(PBLG_long_input)
    output:
        directory("results/runs/1_NOMAD/MD/PBLG_long/{which}")
    shell:
        """
        mkdir -p {output[0]:q}
        cp {input.same:q} {output[0]:q}
        cp {input.top:q} {output[0]:q}/{wildcards.which}.top
        """

rule all_PBLG_long:
    input:
        [f"results/runs/1_NOMAD/MD/PBLG_long/{which}"
         for which in ['em', 'nvt', 'npt']]

def TCM_input(w):
    input = {
        'same': []
    }
    for name in ['edr', 'gro', 'log', 'tpr']:
        input['same'].append(f"results/MD/TCM/box/{w.which}/{w.which}.{name}")
    trajext = 'trr' if w.which == 'em' else 'xtc'
    input['same'].append(f"results/MD/TCM/box/{w.which}/{w.which}.{trajext}")
    input['same'].append("results/MD/TCM/box/{which}/mdout.mdp")

    input['top'] = f"results/MD/TCM/box/box.top"

    return input

localrules: TCM
rule TCM:
    input:
        unpack(TCM_input)
    output:
        directory("results/runs/1_NOMAD/MD/TCM/{which}")
    shell:
        """
        mkdir -p {output[0]:q}
        cp {input.same:q} {output[0]:q}
        cp {input.top:q} {output[0]:q}/{wildcards.which}.top
        """

rule all_TCM:
    input:
        [f"results/runs/1_NOMAD/MD/TCM/{which}"
         for which in ['em', 'nvt', 'npt1', 'npt2']]

def PBLG_periodic_input(w):
    input = {
        'same': []
    }
    for name in ['edr', 'gro', 'log', 'tpr']:
        input['same'].append(f"results/MD/PBLG/periodic/{w.which}/{w.which}.{name}")
    trajext = 'trr' if w.which == 'em' else 'xtc'
    input['same'].append(f"results/MD/PBLG/periodic/{w.which}/{w.which}.{trajext}")
    input['same'].append("results/MD/PBLG/periodic/{which}/mdout.mdp")
    input['same'].append("results/MD/PBLG/periodic/index.ndx")
    input['same'].append("results/MD/PBLG/periodic/restraint.gro")

    input['top'] = f"results/MD/PBLG/periodic/md.top"
    input['same'].append(f"results/MD/PBLG/periodic/md_posre.itp")

    return input

localrules: PBLG_periodic
rule PBLG_periodic:
    input:
        unpack(PBLG_periodic_input)
    output:
        directory("results/runs/1_NOMAD/MD/PBLG_periodic/{which}")
    shell:
        """
        mkdir -p {output[0]:q}
        cp {input.same:q} {output[0]:q}
        cp {input.top:q} {output[0]:q}/{wildcards.which}.top
        """

rule all_PBLG_periodic:
    input:
        [f"results/runs/1_NOMAD/MD/PBLG_periodic/{which}"
         for which in ['em', 'nvt', 'npt']]

def MD_free_input(w):
    input = {
        'same': []
    }

    which = w.which if w.which != 'nvt2' else 'npt'

    if which == 'md':
        prefix = "1/md/10"
    else:
        prefix = f"1/{which}"
    for name in ['edr', 'gro', 'log', 'tpr']:
        input['same'].append(f"results/MD/free/{w.enantiomer}-{w.molname}/{prefix}/{which}.{name}")
    trajext = 'trr' if which == 'em' else 'xtc'
    input['same'].append(f"results/MD/free/{w.enantiomer}-{w.molname}/{prefix}/{which}.{trajext}")
    input['same'].append(f"results/MD/free/{w.enantiomer}-{w.molname}/{prefix}/mdout.mdp")
    input['same'].append(f"results/MD/free/{w.enantiomer}-{w.molname}/1/index.ndx")
    input['same'].append(f"results/MD/free/{w.enantiomer}-{w.molname}/1/restraint.gro")

    topname = 'em' if which == 'em' else 'md'
    input['top'] = f"results/MD/free/{w.enantiomer}-{w.molname}/1/{topname}.top"
    input['same'].append(f"results/MD/free/{w.enantiomer}-{w.molname}/1/{topname}_posre.itp")

    return input

localrules: MD_free
rule MD_free:
    input:
        unpack(MD_free_input)
    output:
        directory("results/runs/1_NOMAD/MD/RDCs/{enantiomer}-{molname}/{which}")
    wildcard_constraints:
        enantiomer="[RSmp]+",
        molname="[\w-]+",
        which="(em|nvt|nvt2|md)"
    shell:
        """
        mkdir -p {output[0]:q}
        cp {input.same:q} {output[0]:q}
        cp {input.top:q} {output[0]:q}/{wildcards.which}.top
        if [[ {wildcards.which} == nvt2 ]]; then
            cd {output[0]:q}
            sed -i 's/npt/nvt2/g' *.log *.mdp
            for suffix in .edr .gro .log .tpr .xtc; do
                mv "npt${{suffix}}" "nvt2${{suffix}}"
            done
            cat > NOTE << EOM
This was replaced by an NPT equilibration (which was the original
intention) in a later version of the workflow. Here, a second NVT
equilibration was used, but the difference should be negligible.
EOM
        fi
        if [[ {wildcards.which} == md ]]; then
            cd {output[0]:q}
            cat > NOTE << EOM
This is the 10th stage (10 ns) of one 100 ns MD chain.
EOM
        fi
        """

rule all_MD_free:
    input:
        [f"results/runs/1_NOMAD/MD/RDCs/{enantiomer}-{molname}/{which}"
         for molname, l_enantiomer in zip(molnames, enantiomers)
         for enantiomer in l_enantiomer
         for which in ['em', 'nvt', 'nvt2', 'md']]

def small_ligand_top_input(enantiomer, molname):
    p = Path(f'data/molecules/small/{enantiomer}-{molname}.smi')
    if p.is_file():
        return f"results/FF/small/{enantiomer}-{molname}.top"
    else:
        other_enantiomer = enantiomer.translate(str.maketrans({'R': 'S', 'S': 'R', 'm': 'p', 'p': 'm'}))
        return f"results/FF/small/{other_enantiomer}-{molname}.top"

localrules: copy_top
rule copy_top:
    input:
        TCM="results/FF/TCM/TCM.top",
        PBLG_long="results/MD/PBLG/long/PBLG_long.top",
        PBLG_periodic="results/MD/PBLG/periodic/PBLG_periodic.top",
        small=[small_ligand_top_input(enantiomer, molname)
               for molname, l_enantiomer in zip(molnames, enantiomers)
               for enantiomer in l_enantiomer]
    output:
        TCM="results/runs/1_NOMAD/top/TCM.top",
        PBLG_long="results/runs/1_NOMAD/top/PBLG/long.top",
        PBLG_periodic="results/runs/1_NOMAD/top/PBLG/periodic.top",
        small=[f"results/runs/1_NOMAD/top/small/{enantiomer}-{molname}.top"
               for molname, l_enantiomer in zip(molnames, enantiomers)
               for enantiomer in l_enantiomer]
    run:
        import shutil
        c = shutil.copyfile
        c(input.TCM, output.TCM)
        c(input.PBLG_long, output.PBLG_long)
        c(input.PBLG_periodic, output.PBLG_periodic)
        for fin, fout in zip(input.small, output.small):
            c(fin, fout)

localrules: copy_gro
rule copy_gro:
    input:
        TCM="results/molecules/TCM/TCM.gro",
        PBLG_long="results/MD/PBLG/long/box/box.gro",
        PBLG_periodic="results/MD/PBLG/periodic/PBLG_periodic.gro",
        small=[f"results/molecules/small/chosen/{enantiomer}-{molname}.gro"
               for molname, l_enantiomer in zip(molnames, enantiomers)
               for enantiomer in l_enantiomer]
    output:
        TCM="results/runs/1_NOMAD/molecules/TCM.gro",
        PBLG_long="results/runs/1_NOMAD/molecules/PBLG/long.gro",
        PBLG_periodic="results/runs/1_NOMAD/molecules/PBLG/periodic.gro",
        small=[f"results/runs/1_NOMAD/molecules/small/{enantiomer}-{molname}.gro"
               for molname, l_enantiomer in zip(molnames, enantiomers)
               for enantiomer in l_enantiomer]
    run:
        import shutil
        c = shutil.copyfile
        c(input.TCM, output.TCM)
        c(input.PBLG_long, output.PBLG_long)
        c(input.PBLG_periodic, output.PBLG_periodic)
        for fin, fout in zip(input.small, output.small):
            c(fin, fout)

rule all:
    input:
        rules.all_PBLG_long.input,
        rules.all_TCM.input,
        rules.all_PBLG_periodic.input,
        rules.all_MD_free.input,
        rules.copy_top.output,
        rules.copy_gro.output
